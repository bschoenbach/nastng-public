FROM fedora:38

# Install base packages: Python, Apache, Timezone
RUN yum update -y
RUN yum install -y httpd mod_ssl tzdata python3-pip python3-mod_wsgi python3-dns \
	python3-pyOpenSSL
RUN pip install connexion[flask,swagger-ui] xmltodict flask-cors prometheus-client

# configure timezone to CET
RUN rm -rf /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# User and directories
RUN groupadd -g 16001 wwwuser && \
    useradd -u 16001 -g wwwuser wwwuser && \
    mkdir -p /run/httpd /home/wwwuser /var/www/nast /etc/nast && \
    chown -R wwwuser:wwwuser /home/wwwuser /var/log/httpd /run/httpd /var/www/nast /etc/nast && \
    rm -rf /etc/httpd/conf.d/ssl.conf

COPY conf/nast.wsgi /var/www/nast
COPY conf/api.yaml /etc/nast/api.yaml
COPY conf/index.html /var/www/nast
COPY conf/iana-ipv6-special-registry.xml /etc/nast
COPY conf/ipv6-unicast-address-assignments.xml /etc/nast

## Apache
COPY conf/fedora/httpd_nast.conf /etc/httpd/conf.d/nast.conf
#COPY conf/fedora/ssl.conf /etc/httpd/conf.d/ssl.conf
COPY conf/fedora/httpd.conf /etc/httpd/conf/httpd.conf

# Install NAST
WORKDIR /tmp
ADD nast /tmp/nast
COPY setup.py /tmp
COPY api.py /tmp/nast
RUN ./setup.py install
#RUN setcap cap_net_raw+ep /usr/bin/traceroute
#RUN setcap cap_net_raw+ep /usr/bin/traceroute
#RUN setcap cap_net_raw+ep /usr/bin/ping
#RUN setcap cap_net_raw+ep /usr/sbin/ping6
#RUN setcap cap_net_raw+ep /usr/sbin/mtr

# Set env var for prometheus multiprocessing mode
RUN mkdir -p /tmp/prometheus_multiproc_dir
RUN chown wwwuser:wwwuser -R /tmp/prometheus_multiproc_dir
ENV PROMETHEUS_MULTIPROC_DIR=/tmp/prometheus_multiproc_dir

USER 16001

EXPOSE 8816 8443

# https://httpd.apache.org/docs/2.4/stopping.html#gracefulstop
STOPSIGNAL SIGWINCH

CMD ["/usr/sbin/httpd", "-DFOREGROUND"]
#CMD /bin/sh
