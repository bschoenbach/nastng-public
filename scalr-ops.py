import sys
import json
import subprocess
import datetime

def get_servers_with_state(status, indices):
    scalr_cmd_list_servers = "scalr-ctl --config {} farm-roles list-servers --farmRoleId {}".format(cli_config_path, cli_farmrole_id)
    list_servers = subprocess.Popen(scalr_cmd_list_servers, shell=True, stdout=subprocess.PIPE)
    list_servers.wait()

    servers_stdout, err = list_servers.communicate()
    if list_servers.returncode != 0:
        raise ValueError("scalr-ctl list servers failed with: {}".format(str(err)))
    
    all_servers = json.loads(servers_stdout)
    timestamps = list()
    ids = dict()

    for server in all_servers['data']:
        if server['status'] == status and str(server['index']) in indices:
            timestamp = datetime.datetime.strptime(server['launched'], '%Y-%m-%dT%H:%M:%SZ')
            if timestamp not in timestamps:
                timestamps.append(timestamp)
                ids[timestamp] = [server['id']]
            else:
                ids[timestamp].append(server['id'])

    servers = list()
    for timestamp in sorted(timestamps):
        servers.extend(ids[timestamp])

    return servers

def terminate_servers(server_list):
    for server in server_list:
        scalr_cmd_terminate_server = "scalr-ctl --config {} servers terminate --serverId {}".format(cli_config_path, server)
        terminate = subprocess.Popen(scalr_cmd_terminate_server, shell=True, stdout=subprocess.PIPE)
        terminate.wait()

        servers_stdout, err = terminate.communicate()
        if terminate.returncode != 0:
            raise ValueError("scalr-ctl terminate server failed with: {}".format(str(err)))
        
        print(servers_stdout)
    return

def launch_servers(server_count):
    for idx in range(server_count):
        scalr_cmd_launch_server = "scalr-ctl --config {} farm-roles launch-server --farmRoleId {}".format(cli_config_path, cli_farmrole_id)
        launch = subprocess.Popen(scalr_cmd_launch_server, shell=True, stdout=subprocess.PIPE)
        launch.wait()

        servers_stdout, err = launch.communicate()
        if launch.returncode != 0:
            raise ValueError("scalr-ctl launch server failed with: {}".format(str(err)))
        
        print(servers_stdout)
    return

cli_config_path= sys.argv[1]
cli_farmrole_id = sys.argv[2]
cli_server_ops = sys.argv[3]
cli_server_count = sys.argv[4]
cli_server_count = int(cli_server_count)
cli_server_state = ''
cli_server_indices = ''

def print_params(): 
    print("CLI Config Path: {}".format(cli_config_path))
    print("Farmrole ID: {}".format(cli_farmrole_id))
    print("Server Operation: {}".format(cli_server_ops))
    print("Server Count: {}".format(cli_server_count))
    print("Server State: {}".format(cli_server_state))
    print("Server Indice Scope: {}".format(cli_server_indices))

if cli_server_ops == "launch":
    print("----- SCALR SERVER LAUNCH OPS -----")
    print_params()
    print("Launch {} server for farmrole: {} .......".format(cli_server_count,cli_farmrole_id))
    launch_servers(cli_server_count)
    print("----- SCALR SERVER LAUNCH SUCCESSFULL -----")
elif cli_server_ops == "terminate":
    print("----- SCALR SERVER TERMINATE OPS -----")
    cli_server_state = sys.argv[5]
    cli_server_indices = sys.argv[6]
    cli_server_indices = cli_server_indices.split(",")
    print_params()
    print("Retrieve server list where: state - {}, indices - {} .......".format(cli_server_state,cli_server_indices))
    servers = get_servers_with_state(cli_server_state, cli_server_indices)
    print(servers)
    print("Terminate old server: {} ....... ".format(servers[:cli_server_count]))
    terminate_servers(servers[:cli_server_count])
    print("----- SCALR SERVER TERMINATE SUCCESSFULL -----")
else:
    print("Unknown operation, choose one of [launch, terminate]")