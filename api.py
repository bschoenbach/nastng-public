#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""!NAST Webservice"""

from logging.config import dictConfig
import os
import logging
import logging.handlers
import re
import connexion
from flask_cors import CORS
import traceback
from flask import has_request_context, request

from flask.logging import default_handler

from nast.config import Config
from nast.metrics import METRICS
from nast.logging import JSON_LOGGER
from nast.queries import NastExecutorHTTPError


#########################################################################################################
# Main
#########################################################################################################
def init_app():
    config = Config()
    # initialize connexion
    nast_app = connexion.FlaskApp("nast_app", specification_dir='.',
                                  swagger_ui_options={"swagger_ui": False})
    nast_app.app.logger.handlers.clear()
    nast_app.app.logger.removeHandler(default_handler)
    nast_app.app.logger.addHandler(JSON_LOGGER.get_log_handler())
    if JSON_LOGGER.get_log_file_handler():
        nast_app.app.logger.addHandler(JSON_LOGGER.get_log_file_handler())


    nast_app.add_api(config.conf_path + "/api.yaml", strict_validation=True,
                     validate_responses=True,
                     swagger_ui_options={"serve_spec": True}) 
    # allow CORS origin http requests from nast-webui
    CORS(nast_app.app, origins=config.cors_allow_origins)

    return nast_app


app = init_app()  # pylint: disable=invalid-name
application = app.app   # pylint: disable=invalid-name


def main():
    config = Config()
    app.run(host=config.hostname, port=config.port,
            debug=True, use_reloader=False, threaded=True)


if __name__ == "__main__":
    main()

@application.errorhandler(Exception)
def handle_exception(e):
    #traceback.print_exception(e)
    respCode=500
    if isinstance(e, NastExecutorHTTPError):
        JSON_LOGGER.log_exception(logging.CRITICAL, __name__, 
                                    "Nast Executor Timeout Error: "+format_jsons_traceback(str(e)),
                                    format_jsons_traceback(str(traceback.format_exception(e))))
        METRICS.update_server_error_metrics('503')
        respCode=503
    else:
        JSON_LOGGER.log_exception(logging.CRITICAL, __name__, "Internal Server Error: "+format_jsons_traceback(str(e)),
                                    format_jsons_traceback(str(traceback.format_exception(e))))
        METRICS.update_server_error_metrics('500')
        
    return str(e), respCode

def format_jsons_traceback(trace):
    replaceTable = ['"', "'", "\\n", "\\"]
    for rep in replaceTable:
        trace=trace.replace(rep, '')
    return trace