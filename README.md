# NAST - NAme Server Tester
NAST is a tool to pre-check your nameservers against the rules of domain registration of the german registry DENIC eG (Predelegation Check).

During the check, the name servers of your zone (domain) will be subjected to various tests in order to verify that they are configured correctly and can be delegated easily and securely. This guarantees a high quality level for the domain.

# Contents
- [How to use](#how-to-use)
- [Architecture](#architecture)
- [Install your own NAST](#install-your-own-nast)
- [Checks](#checks-according-to-denic-23p)
- [Issue Codes](#issue-codes)
- [Testing](#testing)


# How to use
There are several ways to use this tool:
1. use the webinterface: https://nastv2.denic.de/
2. use the public API: https://nastv2.denic.de/docs/nast/api.html
3. install your own NAST ([see below](#install-your-own-nast))

# Architecture
NAST consists of three components:
1. nast-resolver: This is a simple non-caching DNS resolver based on unbound (see: https://www.nlnetlabs.nl/projects/unbound/about/)
2. nast-server: API and business logic (all checks) written in Python
    - Apache Webserver as Frontend
    - WSGI interface to Python
    - Connexion Framework (see https://connexion.readthedocs.io/en/latest/)
    - OpenAPI
3. nast-executor: a resolver backend written in C++ and optimized for performance

In the first phase of development, we only had nast-resolver and nast-server and all DNS queries were implemented in Python using the dnspython module (https://www.dnspython.org/). This worked quite well in our testing environment when doing single queries. But when we started with performance tests, we found out that the code was way to slow and doesn't scale. A bottleneck analysis showed that the dns queries take to long and consume a high amount of CPU even when only waiting for the answer packets. As we couldn't figure out a way around this in Python, we decided to seperate the query module from the business logic and rewrite it in C++.




# Install your own NAST
NAST uses containers and requires IPv4 and IPv6.

- Install and start docker
- Download the latest version from https://nastv2.denic.de/
- unzip the file
- build the containers
```bash
cd nast
docker build -t nast-server:latest .
docker build -t nast-executor:latest -f nastpp/Dockerfile nastpp
# optional, skip if you want to use your own resolver
docker build -t nast-resolver:latest resolver
```

## docker network
In the folowing examples we assume, that you create a docker network called "nast-network" with an IPv4 network 172.31.0.0/24 and an IPv6 network fd00:10:10::/120. Please adjust to your local requirements.

```bash
docker network create --driver bridge --ipv6 \
    --subnet=172.31.0.0/24 \
    --subnet=fd00:10:10::/120 --gateway=172.31.0.1 \
    --gateway=fd00:10:10::1 nast-network
```

We will use the following addresses:
- gateway: 172.31.0.1 / fd00:10:10::1
- nast-resolver: 172.31.0.2 / fd00:10:10::2
- nast-server: 172.31.0.3 / fd00:10:10::3
- nast-executor: 172.31.0.8 / fd00:10:10::8


## nast-resolver (optional)
```bash
docker run --ulimit nofile=10000:10000 --rm -d \
    --net=nast-network \
    --ip=172.31.0.2 --ip6=fd00:10:10::2 \
    --name nast-resolver nast-resolver:latest
```

## nast-server
```bash
docker run --ulimit nofile=10000:10000 -p 8816:8816 -e --rm -d \
    --net=nast-network \
    --ip=172.31.0.3 --ip6=fd00:10:10::3 \
    -e NAST_HOSTNAME=172.31.0.3 \
    -e NAST_PORT=8816 \
    -e NAST_RESOLVER=172.31.0.2 \
    -e LOG_LEVEL=DEBUG \
    --name ${CONTAINER} ${CONTAINER}:latest
```
If you use your own resolver, please adjust the parameter NAST_RESOLVER.

## nast-executor
```bash
docker run --ulimit nofile=10000:10000 -p 8080:8080 --rm -d \
    --net=nast-network \
    --ip=172.31.0.8 --ip6=fd00:10:10::8 \
    -e NAST_QDNS_SERVICE_PORT=8080 \
    -e NAST_QDNS_SERVICE_LOGLEVEL=ERROR \
    -e NAST_QDNS_SERVICE_THREADS=50 \
    --name nast-executor nast-executor:latest
```
## python client
In the source code, you will find a small client "cli.py", which we used for testing. Based on this and the API documentation, you can easyly write your own client.



# Checks according to DENIC-23p
In this section we describe what NAST is really doing to check the requirements
of the annotated documentation DENIC-23p.

See: https://nastv2.denic.de/docs/spec/spec_en.html

## Stages, errors and warnings
The NAST checkes are seperated in several stages. All checks in one stage are
executed completely and all errors and warnings are collected. If there is
one or more errors in a stage, NAST terminates at the end of the stage with
an error and reports all collected errors and warnings.

If there are no errors or only warnings, the next stage gets started. Warnings
are collected over all stages and reported at the end.

### Stage 0 - Parameter check
All parameters are checked. There must be at least 2 nameservers. If a
nameserver is inside the zone it must have Glue-Records (chapter 2.1.3).
If there are dnskeys, their parameters are checked as described in chapter 3.6.1.

### Stage 1 - Resolver
All IPv4 and IPv6 addresses of all nameservers without glue records
are resolved. Every nameserver must have at least one A- or one AAAA-
record.

### Stage 2 - Checks without DNSSEC
- All IP addresses are queried by UDP and TCP for the SOA of the zone.

All further queries are send by UDP only with fallback to TCP.
- All IP addresses are queried by UDP for the NS records of the zone
- All IP addresses of nameservers with glue records are queried for the
  A and AAAA addresses of their glue records

#### Chapter 2.1.1: general requirements
At this point only the UDP answer of the SOA query is checked.
There must be an answer and it must be authoritative.

### Stage 3 - Checks with DNSSEC
All DNSSEC related record data (DNSKEY and SOA) is queried by UDP using EDNS0.
In terms of UDP query exeptions (i.e. truncation, timeout, unknown) a fallback to
TCP is used.
- All IP addresses of all nameservers are queried by UDP towards the DNSKEY
and SOA RRSet of the zone
- All queries use EDNS0 with D0-Bit active to retrieve the signature of the
DNSKEY RRSet and SOA RR, too
In order to execute all DNSSEC related checks of the following sub-chapters
at least one dnskey needs to be handed over. Otherwise DNSSEC checks
cannot be performed and stage3 is completely bypassed.

#### Chapter 3.6.1: Parameter
All given dnskeys are checked towards the flags, algorithm and
public key field. Each dnskey is handed over as DNSKEY RR in wire-text-format.
#### Chapter 3.6.2: Visibility
All given dnskeys are checked towards visibility within the DNSKEY RRSet of all
nameservers. At least one given dnskey must be visible on all nameservers
and the DNSKEY RRSet must be identical.
#### Chapter 3.6.3: Validation - Proof of Possession
The signature of the DNSKEY RRSet is validated. At least one given, visible dnskey must
verify the signature of DNSKEY RRSet of all nameservers.
#### Chapter 3.6.4: Validation - Chain of Trust
The signature of the SOA RR is validated. At least one visible dnskey must verify the
signature of the SOA RR of all nameservers.
#### Chapter 3.6.5: Cross-Rules
All nameservers are checked towards proper EDNS0 support. According to prior-chapter 3.6.2,
availability of the DNSKEY RRSet for all nameservers is checked.

## Issue-Codes
Documentation of issues: https://nastv2.denic.de/docs/spec/spec_en.html#_issues

## Testing
NAST can be tested by setting up test environment with docker containers and a bridged
network between them for communication. No external communication is required.

	# build and start test environment
    make testenv

    # perform tests
    make tests

    # shutdown test environment
    make shutdown

The test environment sets up a bridged docker network with the name "nast-test-net".
It uses two networks: 172.31.0.2/16 and fd00:10:10::/48. The following conatiners
are build and started within these networks:
- nast-resolver: a non caching resolver based on unbound. It is configured with
  the IPs 172.31.0.2 and fd00:10:10::2
- nast-server: NAST server running on port 8080 (no encryption) and 8443 (ssl) with
  the IPs 172.31.0.3 and fd00:10:10::3
- nast-ns*: a nameserver based on ISC's bind with test zones. See below.
- nast-legacy: container running on 172.31.0.4 with legacy java NAST
  Interface: http://172.31.0.4:8816/nast/json/.... or  http://172.31.0.4:8816/nast/xml/....
  This container is not build by default when starting the testenvironment,
  because it requires DENIC internal install servers


The test setup uses its own root zone, so now external communication happens.

### Enable IPv6-Support in Container
To get IPv6 working with external addresses, you need to start docker-ipv6nat.
See: https://github.com/robbertkl/docker-ipv6nat

    docker run -d --name ipv6nat --privileged --network host --restart unless-stopped -v /var/run/docker.sock:/var/run/docker.sock:ro -v /lib/modules:/lib/modules:ro robbertkl/ipv6nat

## Testnameserver

* ns1: normal functional Nameserver with root-zone and others
  IPs: 172.31.1.1 fd00:10:10::1:1
* ns1a: normal functional Nameserver, IPs in same Network as ns1
  IPs: 172.31.1.2 fd00:10:10::1:2
* ns2: normal functional Nameserver with root-zone and others
  IPs: 172.31.2.2 fd00:10:10::2:2
* ns2a: normal functional Nameserver, IPs in same Network as ns2
  IPs: 172.31.2.3 fd00:10:10::2:3
* ns3: normal functional Nameserver with misc others
  IPs: 172.31.3.3 fd00:10:10::3:3
* ns4: normal functional Nameserver with misc others
  IPs: 172.31.4.4 fd00:10:10::4:4
* ns5: Nameserver does not answer on IPv4
  IPs: 172.31.5.5 fd00:10:10::5:5
* ns6: Nameserver does not answer on IPv6
  IPs: 172.31.6.6 fd00:10:10::6:6
* ns7: Nameserver does not answer on TCP
  IPs: 172.31.7.7 fd00:10:10::7:7
* ns8: Nameserver does not answer on UDP
  IPs: 172.31.8.8 fd00:10:10::8:8
* ns9: Nameserver does not answer on any IP or protocol, packets are rejected
  IPs: 172.31.9.9 fd00:10:10::9:9
* ns10: Nameserver does not answer on any IP or protocol, packets are dropped
  IPs: 172.31.10.10 fd00:10:10::10:10
* ns11: Nameserver which announces very small edns0 packet sizes
  IPs: 172.31.11.11 fd00:10:10::11:11
* ns12: Nameserver which allows recursive queries
  IPs: 172.31.12.12 fd00:10:10::12:12
* ns13: nonexisting nameserver
  IPs: 172.31.13.13 fd00:10:10::13:13
* ns14: nsd, which answers only one tcp query and then closes the connection
  IPs: 172.31.14.14 fd00:10:10::14:14
