####################################################################################################
# unfortunately apache crashes with segfault as soon
# as the "WSGIDaemonProcess" parameter is added to
# the config. seems to be a known problem since 5 years
# on alpine. source:
# https://modwsgi.narkive.com/WAUhcjDB/wsgidaemonprocess-segfault-inside-docker-alpine-linux
####################################################################################################

FROM alpine
# Install base packages: Python, Apache, Timezone
RUN apk update && apk add tzdata python3 py3-pip py3-dnspython \
	py3-libxml2 apache2 apache2-ssl apache2-mod-wsgi && apk upgrade

# Install additional python packages
RUN pip install connexion

# configure timezone to CET
RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime && echo "Europe/Berlin" > /etc/timezone


# User and directories
RUN addgroup -g 16001 wwwuser && \
    adduser -u 16001 -D -H -G wwwuser wwwuser && \
    mkdir -p /run/apache2 /home/wwwuser /var/www/nast /etc/nast && \
    chown -R wwwuser:wwwuser /home/wwwuser /etc/ssl/apache2 /run/apache2 /var/www/nast /etc/nast

COPY conf/nast.wsgi /var/www/nast
COPY conf/api.yaml /etc/nast/api.yaml
COPY conf/index.html /var/www/nast

## Apache
COPY conf/httpd_nast.conf /etc/apache2/conf.d/nast.conf
COPY conf/ssl.conf /etc/apache2/conf.d/ssl.conf
COPY conf/httpd.conf /etc/apache2/httpd.conf

# Install NAST
WORKDIR /tmp
ADD nast /tmp/nast
COPY setup.py /tmp
COPY api.py /tmp/nast
RUN ./setup.py install && rm -rf /etc/apache2/conf.d/ssl.conf

# Set env var for prometheus multiprocessing mode
RUN mkdir -p /tmp/multiproc-tmp
ENV PROMETHEUS_MULTIPROC_DIR=/tmp/multiproc-tmp

#USER 16001

EXPOSE 8080 8443

#CMD /usr/sbin/httpd -DFOREGROUND
CMD /bin/sh
