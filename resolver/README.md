The "resolver" provides an alpine based container with unbound as
recursive resolver (see https://www.nlnetlabs.nl/projects/unbound/about/).

It is configured to allow recursive requests from any IPv4 and IPv6 address and
low TTLs for caching of positive and negative answers.

It is bound to any IPv4- and IPv6-address the container has.

If you set the environment variable "TESTMODE=1" on startup, the container gets
startet with a special crafted root-hints file, which requires the test nameservers
to work.