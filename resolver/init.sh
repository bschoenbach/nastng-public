#!/bin/sh

if [ "$TESTMODE" -eq "1" ] ; then
	echo "TESTMODE enabled, starting with fake root.hints"
	rm -rf /etc/unbound/root.hints
	cp /etc/unbound/fakeroot.hints /etc/unbound/root.hints
else
	wget https://www.internic.net/domain/named.cache -O /etc/unbound/root.hints
fi