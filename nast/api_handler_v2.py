# -*- coding: utf-8 -*-
"""!NAST misc API handler"""

import copy
#from base64 import b64encode
#import dns.message
#import dns.rdatatype
#import dns.rdataclass
#import dns.name
#import dns.flags
#import dns.exception
import dns

from nast.config import Config
from nast.issues import contains_errors, aggregate_issues
from nast.resolver import Resolver
from nast.queries import is_in_zone
from nast.controller import pre_delegation_check
from nast.debug import get_parsed_query_results
from nast.metrics import METRICS
from nast.logging import JSON_LOGGER
from logging import INFO


def check_dns_domainname(domainname):
    config = Config()
    try:
        domainname = dns.name.from_text(domainname, idna_codec=config.idna_codec).to_text().strip(".")
    except BaseException as exp:
        return False, {"detail": str(exp),
                "status": 400,
                "title": "Bad Request",
                "type": "about:blank"}
    return True, domainname


def get_resolve(domainname):
    config = Config()
    domainame_ok, exp = check_dns_domainname(domainname)
    if not domainame_ok:
        return exp, 400
    domainname = exp
    resolver = Resolver(config.resolver, config.query_executor)
    ns_dnskey_dict = resolver.resolve_ns_dnskey(domainname)
    answer = {"domainname": domainname,
              "nameserver": {}}

    if not ns_dnskey_dict['NS']:
        JSON_LOGGER.log_resolve(INFO, __name__, {"domainname": domainname, "answer": answer})
        return {}, 404

    ns_dict = resolver.run(ns_dnskey_dict['NS'])
    answer['dnskey'] = ns_dnskey_dict['DNSKEY']
    for nameserver in ns_dict:
        answer['nameserver'][nameserver] = []
        if is_in_zone(nameserver, domainname):
            for rr_type in ['A', 'AAAA']:
                if isinstance(ns_dict[nameserver][rr_type], Exception):
                    return {}, 404
                for ip_addr in ns_dict[nameserver][rr_type]:
                    answer['nameserver'][nameserver].append(ip_addr)
    JSON_LOGGER.log_resolve(INFO, __name__, {"domainname": domainname, "answer": answer})
    return answer


def get_check(domainname, ns1, ns2, nsX=None, dnskey=None, debug=None):  # pylint: disable=invalid-name,too-many-arguments
    METRICS.update_request_total_metrics('v2')
    nameserver = []
    nameserver.append(ns1)
    nameserver.append(ns2)
    if nsX:
        nameserver.extend(nsX)

    domainame_ok, exp = check_dns_domainname(domainname)
    if not domainame_ok:
        return exp, 400

    params = {'domain': domainname,
              'nameserver': {},
              'dnskey': []}
    for item in nameserver:
        tokens = item.split(",")
        params['nameserver'][tokens[0]] = tokens[1:]
    if dnskey:
        for item in dnskey:
            # adoption to fit old dnsḱey delimeter style in v1 with , as delimeter
            item_str = item.replace(",", " ")
            tokens = item_str.split(" ")
            params['dnskey'].append({'flags': int(tokens[0]),
                                     'protocol': int(tokens[1]),
                                     'algorithm': int(tokens[2]),
                                     'key': ''.join(tokens[3:])})
    original_params = copy.deepcopy(params)  # Save unmodified params for later logging

    issue_list, nameserver_resolved, query_results = pre_delegation_check(params)
    aggregated_issue_list = aggregate_issues(issue_list)
    answer = prepare_answer_v2(aggregated_issue_list, nameserver_resolved, query_results, debug)
    JSON_LOGGER.log_check(INFO, __name__, {"params": original_params, "domainname": domainname, "answer": answer})
    METRICS.update_metrics('v2', aggregated_issue_list, query_results, nameserver_resolved, domainname)
    return answer


def prepare_answer_v2(issue_list, nameserver_resolved, query_results, debug):
    answer = {}
    answer["success"] = not contains_errors(issue_list)
    if issue_list:
        answer["issues"] = []
        for issue in issue_list:
            issue_dict = {'code': issue.get_code(),
                          'message': issue.get_name(),
                          'severity': issue.get_severity_name(),
                          'arguments': issue.get_params()}
            answer["issues"].append(issue_dict)
    if debug:
        answer['debug'] = {'nameserver': nameserver_resolved,
                           'queries': get_parsed_query_results(query_results)}
    return answer
