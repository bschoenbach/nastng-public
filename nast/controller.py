"""!NAST main entry point for pre-delegation check"""

from nast.config import Config
from nast.issues import contains_errors
from nast.stage0 import parameter_check
from nast.stage1 import resolve_and_check_ip_addresses
from nast.stage2 import run_stage2
from nast.stage3 import check_dnssec


def pre_delegation_check(params):
    config = Config()
    # Stage 0: parameter check
    issue_list = parameter_check(config, params)
    if contains_errors(issue_list):
        return issue_list, None, None

    # Stage 1: resolve and check IP addresses
    issue_list += resolve_and_check_ip_addresses(config, params)
    if contains_errors(issue_list):
        return issue_list, params['nameserver'], None

    # Stage 2: Query nameserver and check results without dnssec
    query_results, stage2_issues = run_stage2(config, params)
    issue_list += stage2_issues
    if contains_errors(issue_list):
        return issue_list, params['nameserver'], query_results

    # Stage 3: check dnssec specific results of nameserver
    issue_list += check_dnssec(config, params, query_results)
    return issue_list, params['nameserver'], query_results
