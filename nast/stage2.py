"""!stage 2 checks"""
from logging import INFO
import math
from ipaddress import ip_address
import dns.flags
import dns.exception
import dns.rdataclass
import dns.rdatatype
import dns.message
from nast.issues import NastIssue, IssueCodes, IssueSeverity, append_issues
from nast.queries import Queries, is_in_zone, HostUnreachable
from nast.iana import iana


def check_is_reachable(answer, issue_params, proto='udp'):

    severity = IssueSeverity.WARNING if proto == 'tcp' else IssueSeverity.ERROR

    if isinstance(answer, dns.exception.Timeout):
        return NastIssue(IssueCodes.TIMEOUT, severity, issue_params)

    if isinstance(answer, ConnectionRefusedError):
        return NastIssue(IssueCodes.CONNECTION_REFUSED, severity, issue_params)

    if isinstance(answer, HostUnreachable):
        return NastIssue(IssueCodes.HOST_UNREACHABLE, severity, issue_params)

    if isinstance(answer, BrokenPipeError):
        return NastIssue(IssueCodes.BROKEN_PIPE, severity, issue_params)

    if isinstance(answer, ConnectionAbortedError):
        return NastIssue(IssueCodes.CONNECTION_ABORTED, severity, issue_params)

    if isinstance(answer, Exception):
        unexpected_excp_issue = NastIssue(IssueCodes.UNEXPECTED_EXCEPTION, severity, issue_params)
        unexpected_excp_issue.log(INFO, answer, 2, "check_is_reachable",
                               {"params": issue_params, "proto": proto})
        return unexpected_excp_issue
    return None


def check_for_unexpected_rcode(answer, ip, entity):
    if not answer:
        return NastIssue(IssueCodes.UNEXPECTED_RCODE,
                         IssueSeverity.ERROR,
                         {"target": ip,
                          "entity": entity,
                          "result": "no answer"})
    if answer.rcode() != dns.rcode.NOERROR:
        return NastIssue(IssueCodes.UNEXPECTED_RCODE,
                         IssueSeverity.ERROR,
                         {"target": ip,
                          "entity": entity,
                          "rcode": dns.rcode.to_text(answer.rcode())})
    return None


def check_is_reachable_and_authoritative(result, domain):
    """
    2.1.1
    """
    issue_list = list()
    for nameserver in result:
        for ip in result[nameserver]:

            answer_udp = result[nameserver][ip]['SOA']['udp']
            new_issue = check_is_reachable(answer_udp, {'nameserver': nameserver.strip("."),
                                                        'ip': ip,
                                                        'protocol': 'udp'}, 'udp')
            if new_issue:
                issue_list.append(new_issue)
            else:
                new_issue = check_for_unexpected_rcode(answer_udp, ip, "SOA")
                if new_issue:
                    issue_list.append(new_issue)
                else:
                    if dns.flags.AA not in answer_udp.flags:
                        issue_params = {"nameserver": nameserver.strip("."), "ip": ip}
                        rrset = answer_udp.get_rrset(dns.message.ANSWER, dns.name.from_text(domain),
                                                     dns.rdataclass.IN, dns.rdatatype.SOA)
                        if rrset:
                            issue_params["answer"] = str(rrset[0])

                        issue_list.append(NastIssue(IssueCodes.SOA_RECORD_NOT_AUTHORITATIVE,
                                                    IssueSeverity.ERROR,
                                                    issue_params))
                    if dns.flags.RA in answer_udp.flags:
                        parameter_dict = {
                            'nameserver': nameserver,
                            'ip': ip
                        }
                        issue_list.append(
                            NastIssue(IssueCodes.RECURSIVE_QUERIES_SHOULD_NOT_BE_ALLOWED,
                                      IssueSeverity.WARNING, parameter_dict=parameter_dict))

            answer_tcp = result[nameserver][ip]['SOA']['tcp']
            new_issue = check_is_reachable(answer_tcp, {'nameserver': nameserver.strip("."),
                                                        'ip': ip,
                                                        'protocol': 'tcp'}, 'tcp')
            if new_issue:
                issue_list.append(new_issue)

    return issue_list


def is_answer_ok(answer):

    if not isinstance(answer, dns.message.Message):
        return False

    if answer.rcode() != dns.rcode.NOERROR:
        return False

    if dns.flags.AA not in answer.flags:
        return False

    return True


def add_issues_on_wrong_answer(answer, issue_list, nameserver, ip, record_type, proto):
    if not isinstance(answer, dns.message.Message):
        return
    if answer.rcode() != dns.rcode.NOERROR:
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE,
                                    IssueSeverity.ERROR,
                                    parameter_dict={
                                        'nameserver': nameserver,
                                        'ip': ip,
                                        'proto': proto,
                                        'record': record_type
                                    }))

    if dns.flags.AA not in answer.flags:
        issue_list.append(NastIssue(IssueCodes.ANSWER_NOT_AUTHORITATIVE,
                                    IssueSeverity.ERROR,
                                    parameter_dict={
                                        'nameserver': nameserver,
                                        'ip': ip,
                                        'proto': proto,
                                        'record': record_type
                                    }))


def check_a_aaaa_for_nameserver(query_result, domain):
    """
    2.1.3.2
    """

    issue_list = list()

    for nameserver in query_result:
        ips_in_task = set()
        for ip in query_result[nameserver].keys():
            ips_in_task.add(str(ip_address(ip)))

        if not is_in_zone(nameserver, domain):
            continue

        for ip in query_result[nameserver]:
            ips_from_answer = set()

            # If none of the answers is a valid Message, skip rest of the loop
            if not isinstance(
                query_result[nameserver][ip]["A"], dns.message.Message
            ) and not isinstance(
                query_result[nameserver][ip]["AAAA"], dns.message.Message
            ):
                continue

            for record_type in ['A', 'AAAA']:
                answer_to_check = query_result[nameserver][ip][record_type]

                if not is_answer_ok(answer_to_check):
                    continue

                rrset = answer_to_check.get_rrset(dns.message.ANSWER,
                                                  dns.name.from_text(nameserver),
                                                  dns.rdataclass.IN,
                                                  dns.rdatatype.from_text(record_type))
                if rrset:
                    for record in rrset:
                        ips_from_answer.add(str(ip_address(record.address)))

            difference = ips_in_task.symmetric_difference(ips_from_answer)
            if difference:
                issue_params = {
                    'ns': nameserver,
                    'provided_ips': str(sorted(list(ips_in_task))),
                    'determined_ips': str(sorted(list(ips_from_answer)))
                }
                issue_list.append(NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES,
                                            IssueSeverity.ERROR, issue_params))

    return issue_list


def check_soa(query_result, domain):
    """
    2.1.4
    """
    issue_list = list()

    mname_set = set()
    mname_list = list()

    for nameserver, ip_list in query_result.items():
        for ip in ip_list:
            answer_to_check = query_result[nameserver][ip]['SOA']['udp']
            if not is_answer_ok(answer_to_check):
                answer_to_check = query_result[nameserver][ip]['SOA']['tcp']
                if not is_answer_ok(answer_to_check):
                    continue

            rrset = answer_to_check.get_rrset(dns.message.ANSWER, dns.name.from_text(domain),
                                              dns.rdataclass.IN, dns.rdatatype.SOA)
            if rrset:
                record = rrset[0]

                mname = str(record.mname).strip('.')
                mname_set.add(mname)
                mname_list.append(mname + ".")

                if not (3600 <= record.refresh <= 86400):
                    issue_list.append(NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE,
                                                IssueSeverity.WARNING, parameter_dict={
                                                    'expected': '[3600..86400]',
                                                    'found': str(record.refresh),
                                                    'nameserver': nameserver,
                                                    'ip': ip
                                                }))

                if not (900 <= record.retry <= 28800):
                    issue_list.append(NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE,
                                                IssueSeverity.WARNING, parameter_dict={
                                                    'expected': '[900..28800]',
                                                    'found': str(record.retry),
                                                    'nameserver': nameserver,
                                                    'ip': ip
                                                }))

                refresh_1_8 = math.floor(1 / 8 * record.refresh)
                refresh_1_3 = math.ceil(1 / 3 * record.refresh)
                if not (refresh_1_8 <= record.retry <= refresh_1_3):
                    issue_list.append(NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE_REFRESH,
                                                IssueSeverity.WARNING, parameter_dict={
                                                    'expected': f'[{refresh_1_8}..{refresh_1_3}]',
                                                    'found': str(record.retry),
                                                    'nameserver': nameserver,
                                                    'ip': ip
                                                }))

                if not (604800 <= record.expire <= 3_600_000):
                    issue_list.append(NastIssue(IssueCodes.EXPIRE_VALUE_OUT_OF_RANGE,
                                                IssueSeverity.WARNING, parameter_dict={
                                                    'expected': '[604800..3600000]',
                                                    'found': str(record.expire),
                                                    'nameserver': nameserver,
                                                    'ip': ip
                                                }))

                if not (180 <= record.minimum <= 86400):
                    issue_list.append(NastIssue(IssueCodes.MINTTL_VALUE_OUT_OF_RANGE,
                                                IssueSeverity.WARNING, parameter_dict={
                                                    'expected': '[180..86400]',
                                                    'found': str(record.minimum),
                                                    'nameserver': nameserver,
                                                    'ip': ip
                                                }))
            else:
                issue_list.append(NastIssue(IssueCodes.SOA_RECORD_NOT_AUTHORITATIVE,
                                            IssueSeverity.ERROR,
                                            parameter_dict={
                                                'found': "No records found in answer section",
                                                'nameserver': nameserver,
                                                'ip': ip
                                            }))

    if len(mname_set) > 1:
        issue_list.append(NastIssue(IssueCodes.MNANE_INCONSISTENT_ACROSS_SOA_RECORDS,
                                    IssueSeverity.WARNING,
                                    {"master": "[" + ", ".join(mname_list) + "]"}))

    return issue_list


def check_ns_rrset(query_results, nameserver_to_check, domain):
    params_ns_set = {ns.strip('.').lower() for ns in nameserver_to_check}
    issue_list = list()

    for nameserver, ip_dict in query_results.items():
        for ip, queries in ip_dict.items():
            answer = queries['NS']
            if not is_answer_ok(answer):
                add_issues_on_wrong_answer(answer, issue_list, nameserver, ip, "NS", "udp")
                continue

            rrset = answer.get_rrset(dns.message.ANSWER, dns.name.from_text(domain),
                                     dns.rdataclass.IN, dns.rdatatype.NS)
            if rrset:
                answer_ns_set = set()
                if rrset:
                    for record in rrset:
                        answer_ns_set.add(record.target.to_text().strip('.').lower())

                difference = params_ns_set.symmetric_difference(answer_ns_set)
                if difference:
                    issue_list.append(NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_RRS,
                                                parameter_dict={
                                                    'NS': nameserver,
                                                    'IP': ip,
                                                    'NS host names': str(sorted(list(answer_ns_set)))
                                                }))
            else:
                issue_list.append(NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_RRS,
                                            IssueSeverity.ERROR,
                                            parameter_dict={
                                                'found': "No NS records found in answer section",
                                                'nameserver': nameserver,
                                                'ip': ip
                                            }))
    return issue_list


def check_cname(query_result, domain):
    issue_list = list()

    for nameserver in query_result:
        for ip in query_result[nameserver]:
            answer = query_result[nameserver][ip]['SOA']['udp']

            if not is_answer_ok(answer):
                continue

            rrset = answer.get_rrset(dns.message.ANSWER, dns.name.from_text(domain),
                                     dns.rdataclass.IN, dns.rdatatype.CNAME)

            if rrset:
                issue_list.append(NastIssue(IssueCodes.SOA_RECORD_RESPONSE_MUST_BE_DIRECT,
                                            parameter_dict={
                                                'nameserver': nameserver,
                                                'ip': ip,
                                                'recordset': str(rrset)
                                            }))
    return issue_list


def run_stage2(config, params):
    issue_list = list()
    # Prüfen
    # Brauchen wir diese Fehlermeldungen?
    # | 126 | Insufficient number of nameservers reachable via IPv4 (expected, found) | 2.1.2 |
    # | 127 | Insufficient number of nameservers reachable (expected, found) | 2.1.2 |

    queries = Queries(base_url=config.query_executor)
    dnssec = False
    if 'dnskey' in params:
        dnssec = True if len(params['dnskey']) > 0 else False

    query_result = queries.run(params['nameserver'], params['domain'], dnssec)

    append_issues(issue_list, check_is_reachable_and_authoritative(query_result, params['domain']))
    append_issues(issue_list, check_soa(query_result, params['domain']))
    append_issues(issue_list, check_a_aaaa_for_nameserver(query_result, params['domain']))
    append_issues(issue_list, check_ns_rrset(query_result, params['nameserver'], params['domain']))
    append_issues(issue_list, check_cname(query_result, params['domain']))

    # check IPv6 addresses from aaaa records
    append_issues(issue_list, iana.check_ipaddresses_from_aaaa_rr(query_result))

    return query_result, issue_list
