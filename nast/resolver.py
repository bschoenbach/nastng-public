"""!NAST resolver module"""

import base64
from concurrent.futures import ThreadPoolExecutor
import dns.query
import dns.message
import dns.rdatatype
import dns.rdataclass
import requests
from nast.queries import  map_error_to_answer, add_request_record, make_executor_request
from nast.metrics import METRICS


class Resolver:

    def __init__(self, resolver_ip, executor_url):
        self.resolver_ip = resolver_ip
        self.executor_url = executor_url

    def resolve_ns_dnskey(self, domainname):
        result = {'NS': [], 'DNSKEY': []}
        request = {'nameserver': [self.resolver_ip], 'records': []}
        add_request_record(request, domainname, 'NS', 'udp', recursion=True)
        add_request_record(request, domainname, 'DNSKEY', 'udp', recursion=True)
        queries = make_executor_request(request, self.executor_url)

        for answer in queries[self.resolver_ip]:
            qrtype = answer['type']
            qstatus = answer['success']

            if qstatus:
                try:
                    b64_decode = base64.b64decode(bytes(answer['message'], 'ascii'))
                    message = dns.message.from_wire(b64_decode)
                    rrset = message.get_rrset(message.answer, dns.name.from_text(domainname),
                                dns.rdataclass.IN, dns.rdatatype.from_text(qrtype))
                    for record in rrset:
                        if qrtype == 'DNSKEY':
                            if record.flags & 1:
                                result[qrtype].append({'flags': int(record.flags),
                                'protocol': int(record.protocol),
                                'algorithm': int(record.algorithm),
                                'key': str(base64.b64encode(record.key), "us-ascii")})
                        else:
                            result[qrtype].append(str(record.target))
                except Exception as exp:
                    result[qrtype] = []
        
        return result

    def create_ns_dict(self, ns_list):
        ns_dict = dict()
        request = {'nameserver': [self.resolver_ip], 'records': []}

        for nameserver in ns_list:
            ns_dict[nameserver] = dict()
            for rtype in ['A', 'AAAA']:
                ns_dict[nameserver][str(rtype)] = list()
                add_request_record(request, nameserver, rtype, 'udp', recursion=True)
                
        queries = make_executor_request(request, self.executor_url)
        
        for nameserver in ns_list:
            for answer in queries[self.resolver_ip]:
                qname = answer['name']
                if qname == nameserver:
                    qrtype = answer['type']
                    qstatus = answer['success']

                    if qstatus:
                        try:
                            b64_decode = base64.b64decode(bytes(answer['message'], 'ascii'))
                            message = dns.message.from_wire(b64_decode)
                            rrset = message.get_rrset(message.answer, dns.name.from_text(nameserver),
                                        dns.rdataclass.IN, dns.rdatatype.from_text(qrtype))
                            if rrset:
                                ns_dict[nameserver][qrtype] += [rr.address.lower() for rr in rrset]
                        except Exception as exp:
                            ns_dict[nameserver][qrtype] = exp

                    if not qstatus:
                        qerror = answer['error']
                        qerror_message = answer['error_message']
                        message = map_error_to_answer(qerror, qerror_message)
                        ns_dict[nameserver][qrtype] = message

        return ns_dict

    def run(self, ns_list):
        ns_dict = self.create_ns_dict(ns_list)
        METRICS.update_resolver_metrics(ns_dict)
        return ns_dict