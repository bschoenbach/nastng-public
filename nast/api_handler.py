# -*- coding: utf-8 -*-
"""!NAST misc API handler"""

import json
import copy
import logging
from flask import Response
from nast.api_handler_v2 import check_dns_domainname
from nast.controller import pre_delegation_check
from nast.issues import contains_errors, aggregate_issues
from nast.debug import get_parsed_query_results
from nast.metrics import METRICS
from nast.logging import JSON_LOGGER
from logging import INFO


def get_homepage():
    return Response(response="", status=200, content_type='text/plain; charset=utf-8')


def get_alive():
    return Response(response="alive", status=200, content_type='text/plain; charset=utf-8')


def get_ready():
    return Response(response="ready", status=200, content_type='text/plain; charset=utf-8')


def get_query(result_format, domainname, ns1, ns2, policy=None, nsX=None, dnskey=None, debug=None):  # pylint: disable=invalid-name,too-many-arguments
    # we don't use policy
    METRICS.update_request_total_metrics('v1')
    del policy
    nameserver = []
    nameserver.append(ns1)
    nameserver.append(ns2)
    if nsX:
        nameserver.extend(nsX)

    domainame_ok, exp = check_dns_domainname(domainname)
    if not domainame_ok:
        return exp, 400

    params = {'domain': domainname,
              'nameserver': {},
              'dnskey': []}
    for item in nameserver:
        tokens = item.split(",")
        params['nameserver'][tokens[0]] = tokens[1:]
    if dnskey:
        for item in dnskey:
            # adoption to fit old dnsḱey delimeter style in v1 with , as delimeter
            item_str = item.replace(",", " ")
            tokens = item_str.split(" ")
            params['dnskey'].append({'flags': int(tokens[0]),
                                     'protocol': int(tokens[1]),
                                     'algorithm': int(tokens[2]),
                                     'key': ''.join(tokens[3:])})
    original_params = copy.deepcopy(params)  # Save unmodified params for later logging

    issue_list, nameserver_resolved, query_results = pre_delegation_check(params)
    aggregated_issue_list = aggregate_issues(issue_list)
    answer = prepare_answer(aggregated_issue_list)
    JSON_LOGGER.log_check(INFO, __name__, {"params": original_params, "domainname": domainname, "answer": answer, "format": result_format})
    METRICS.update_metrics('v1', aggregated_issue_list, query_results, nameserver_resolved, domainname)
    if result_format == "json":
        if debug:
            answer['debug'] = {'nameserver': nameserver_resolved,
                               'queries': get_parsed_query_results(query_results)}
        return answer_json(answer)
    return answer_xml(answer)


def prepare_answer(issue_list):
    answer = {}
    answer["success"] = not contains_errors(issue_list)
    if issue_list:
        answer["issues"] = []
        for issue in issue_list:
            issue_params = issue.get_params()
            issue_dict = {'code': issue.get_code(),
                          'message': issue.get_name(),
                          'severity': issue.get_severity_name()}
            if issue_params:
                append = " (" + ", ".join(issue_params.keys()) + ")"
                issue_dict['message'] += append

                issue_dict['arguments'] = list(issue_params.values())
                # for key in issue_params:
                #    issue_dict['arguments'].append(issue_params[key])
            answer["issues"].append(issue_dict)

    return answer


def answer_json(answer):
    return answer


def answer_xml(answer):
    xml = '<?xml-stylesheet type="text/xsl" href="../meta/nast_answer.xsl"?>\n' + \
        '<predelegation-check success="' + str(answer['success']).lower() + '" xsi:schemaLocation="../meta/nast_answer.xsd" ' +\
        'xmlns="http://schema.denic.de/rng/nast" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"'

    if "issues" in answer and answer['issues']:
        xml += ">\n"
        xml += "  <issues>\n"
        for issue in answer['issues']:
            xml += "    <issue code=\"%d\" severity=\"%s\">\n" % (issue['code'], issue['severity'])
            xml += "      <message>%s</message>\n" % (issue['message'])
            if "arguments" in issue and issue['arguments']:
                xml += "      <parameters>\n"
                for item in issue['arguments']:
                    xml += "        <parameter>%s</parameter>\n" % (item)
                xml += "      </parameters>\n"
            xml += "    </issue>\n"

        xml += "  </issues>\n"
        xml += "</predelegation-check>"
    else:
        xml += "/>\n"
    return Response(response=xml, status=200, content_type='application/xml;charset=UTF-8')
