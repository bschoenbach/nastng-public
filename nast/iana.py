# -*- coding: utf-8 -*-
"""!IANA IPv6 address check"""

import json
import ipaddress
import xmltodict
import dns.rcode
import dns.message
import dns.rdtypes
import dns.rdataclass
import dns.name
from nast.config import Config
from nast.issues import IssueSeverity, IssueCodes, NastIssue, aggregate_issues


class InvalidIanaFileError(BaseException):
    def __init__(self, filename):
        super().__init__()
        self.filename = filename

    def __str__(self):
        return "InvalidIanaFileError[%s]" % (self.filename)


def special_registr_is_true(value):
    str_value = None
    if isinstance(value, dict) and "#text" in value:
        str_value = str(value["#text"]).strip().lower()
    else:
        str_value = str(value).strip().lower()
    if str_value == "true":
        return True
    return False


def parse_special_registry(filename):
    try:
        with open(filename, "r") as handle:
            data = xmltodict.parse(handle.read())
            address_list = []
            if "registry" not in data or "registry" not in data["registry"] or "record" not in data["registry"]["registry"]:
                raise InvalidIanaFileError(filename)
            for record in data["registry"]["registry"]["record"]:
                network = None
                if isinstance(record["address"], dict):
                    network = ipaddress.ip_network(record["address"]["#text"])
                else:
                    network = ipaddress.ip_network(record["address"])
                # old NAST only checks the global value, which must be True
                is_global = special_registr_is_true(record["global"])
                address_list.append({"network": network,
                                     "name": record["name"],
                                     "global": is_global
                                     })
            return address_list
    except BaseException as exp:
        raise InvalidIanaFileError(str(exp) + " [filename]") from exp


def parse_address_assignments(filename):
    try:
        with open(filename, "r") as handle:
            data = xmltodict.parse(handle.read())
            address_list = []
            if "registry" not in data or "record" not in data["registry"]:
                raise InvalidIanaFileError(filename)
            for record in data["registry"]["record"]:
                if record["status"].upper() == "ALLOCATED":
                    address_list.append(ipaddress.ip_network(record["prefix"]))
            return address_list
    except BaseException as exp:
        raise InvalidIanaFileError(str(exp) + " [filename]") from exp


class IanaIPv6Addresses():
    def __init__(self):
        self.config = Config()
        self.assigned_addresses = []
        self.special_purpose = []
        if self.config.testmode:
            self.assigned_addresses.append(ipaddress.ip_network("fd00:10:10::/48"))
            self.special_purpose.append({"network": ipaddress.ip_network("fd00:10:10::/48"),
                                         "name": "NAST Testing",
                                         "global": True})
        self.special_purpose.extend(parse_special_registry(self.config.conf_path + "/iana-ipv6-special-registry.xml"))
        self.assigned_addresses.extend(parse_address_assignments(self.config.conf_path + "/ipv6-unicast-address-assignments.xml"))

    def check_assigned(self, addr):
        # if addr is in one of the assigned_addresses list, everything is ok
        for network in self.assigned_addresses:
            if addr in network:
                return True
        return False

    def check_special_purpose(self, addr):
        # we have to find the most specific network for this address
        scope = 0
        result = True
        for item in self.special_purpose:
            if addr in item["network"]:
                if item["network"].prefixlen > scope:
                    result = item["global"]
                    scope = item["network"].prefixlen
        return result

    def check(self, issue_list, nameserver, addr):
        if addr.version == 6:
            if not self.check_assigned(addr):
                issue_list.append(NastIssue(IssueCodes.IPV6_ADDRESS_NOT_ALLOCATED,
                                            IssueSeverity.ERROR,
                                            {"nameserver": nameserver,
                                             "ip": str(addr)}))
            if not self.check_special_purpose(addr):
                issue_list.append(NastIssue(IssueCodes.IPV6_ADDRESS_NOT_ROUTABLE,
                                            IssueSeverity.ERROR,
                                            {"nameserver": nameserver,
                                             "ip": str(addr)}))

    def check_ipaddresses_from_params(self, params):
        issue_list = list()
        if 'nameserver' not in params:
            return issue_list
        for nameserver in params['nameserver']:
            for str_addr in params['nameserver'][nameserver]:
                try:
                    addr = ipaddress.ip_address(str_addr)
                    self.check(issue_list, nameserver, addr)
                except BaseException:
                    issue_list.append(NastIssue(IssueCodes.INVALID_IP_ADDRESS,
                                                IssueSeverity.ERROR,
                                                {"nameserver": nameserver,
                                                 "ip": str_addr}))

        return issue_list

    def check_ipaddresses_from_aaaa_rr(self, query_result):
        issue_list = list()
        for nameserver_name in query_result:
            for nameserver_ip in query_result[nameserver_name]:
                if 'AAAA' in query_result[nameserver_name][nameserver_ip]:
                    answer = query_result[nameserver_name][nameserver_ip]['AAAA']
                    if isinstance(answer, dns.message.Message) and answer.rcode() == dns.rcode.NOERROR:
                        rr_set = answer.get_rrset(dns.message.ANSWER,
                                                  dns.name.from_text(nameserver_name),
                                                  dns.rdataclass.IN, dns.rdatatype.from_text('AAAA'))
                        if rr_set:
                            for entry in rr_set:
                                str_addr = entry.address.lower()
                                try:
                                    addr = ipaddress.ip_address(str_addr)
                                    self.check(issue_list, nameserver_name, addr)
                                except BaseException:
                                    issue_list.append(NastIssue(IssueCodes.INVALID_IP_ADDRESS,
                                                                IssueSeverity.ERROR,
                                                                {"nameserver": nameserver_name,
                                                                 "ip": str_addr}))

        return aggregate_issues(issue_list)


iana = IanaIPv6Addresses()
