"""!NAST query module"""

import base64
import json
import dns.exception
from concurrent.futures import ThreadPoolExecutor
import dns.query
import dns.message
import dns.rdatatype
import dns.rdataclass
import dns.name
import dns.flags
import requests


class HostUnreachable(ConnectionError):
    pass


class NastExecutorHTTPError(dns.exception.Timeout):
    """Raised when nast executor is not reachable via http or does not respond correctly"""
    pass


def is_in_zone(nameserver, domain):
    level_domain = domain.strip('.').lower().split('.')
    level_nameserver_hostname = nameserver.strip('.').lower().split('.')
    if len(level_nameserver_hostname) < len(level_domain):
        return False
    for i in range(1, len(level_domain) + 1):
        if level_domain[-i] != level_nameserver_hostname[-i]:
            return False
    return True


def make_executor_request(request, executor):
    headers = {'Content-type': 'application/json'}
    try:
        response = requests.post(executor, json=request, headers=headers)
    except Exception as exp:
        raise NastExecutorHTTPError('Query executor http request failed with:' + str(exp))

    if response.status_code != 200:
        raise NastExecutorHTTPError('Query executor http request failed with:', response.status_code, response.json())

    queries = response.json()

    if not queries:
        raise NastExecutorHTTPError('Query executor http request failed with:', response.status_code, "Empty response body")

    return queries


def map_error_to_answer(error, error_message):
    if not error:
        return None
    if error == 'TruncatedError':
        return dns.message.Truncated()
    elif error == 'TimeoutError':
        return dns.exception.Timeout(error_message)
    elif error == 'ConnectionRefusedError':
        return ConnectionRefusedError(error_message)
    elif error == 'HostUnreachable':
        return HostUnreachable(error_message)
    elif error == 'BrokenPipe':
        return BrokenPipeError(error_message)
    elif error == 'ConnectionClosedByRemoteHostError':
        if not error_message:
            error_message = "connection closed by remote host"
        return ConnectionAbortedError(error_message)
    else:
        return dns.exception.DNSException(f"{error} [{error_message}]")


def add_request_record(request, name, rtype, proto, dnssec=False, recursion=False):
    request['records'].append({'name': name, 'type': rtype, 'proto': proto, 'dnssec': dnssec, 'recursion': recursion})


class Queries:

    def __init__(self, base_url):
        self.base_url = base_url

    def create_and_fill_result_dict(self, nameserver_dict, domain, dnssec=False):
        result = dict()
        request = {'nameserver': [], 'records': []}

        if not nameserver_dict or not domain:
            return result

        for nameserver, ip_list in nameserver_dict.items():
            # prepare result dict
            result[nameserver] = dict()
            for ip_addr in ip_list:
                result[nameserver][ip_addr] = {'SOA': {'tcp': None, 'udp': None},
                                               'NS': None
                                               }
                if dnssec:
                    result[nameserver][ip_addr]['DNSKEY'] = {'udp': None}
                    result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}

            # A, AAAA
            if is_in_zone(nameserver, domain):
                for rtype in ['A', 'AAAA']:
                    for ip_addr in ip_list:
                        result[nameserver][ip_addr][rtype] = None
                    add_request_record(request, nameserver, rtype, 'udp')

            # IPs
            for ip_addr in ip_list:
                if ip_addr not in request['nameserver']:
                    request['nameserver'].append(ip_addr)

        # SOA
        add_request_record(request, domain, 'SOA', 'udp')
        add_request_record(request, domain, 'SOA', 'tcp')

        # DNSSEC (SOA, DNSKEY)
        if dnssec:
            add_request_record(request, domain, 'DNSKEY', 'udp', True)
            add_request_record(request, domain, 'SOA', 'udp', True)

        # NS
        add_request_record(request, domain, 'NS', 'udp')

        queries = make_executor_request(request, self.base_url)

        #print(json.dumps(request, indent=4, sort_keys=True))
        #print(json.dumps(queries, indent=4, sort_keys=True))

        for nameserver, ip_list in nameserver_dict.items():
            for ip_addr in ip_list:
                for answer in queries[ip_addr]:
                    qname = answer['name']
                    qrtype = answer['type'] + '+DNSSEC' if answer['type'] == 'SOA' and answer['dnssec'] else answer['type']
                    if (qname == domain or qname == nameserver) and (qrtype in result[nameserver][ip_addr]):
                        qproto = answer['proto']
                        qstatus = answer['success']
                        qfallback = answer['fallback']
                        qdnssec = answer['dnssec']
                        #qrecursion = answer['recursion']
                        #print('-----Parse nameserver query response------')
                        #print('nameserver:',nameserver)
                        #print('ip_addr:',ip_addr)
                        #print('domain:',qname)
                        #print('type:',qrtype)
                        #print('proto:',qproto)
                        #print('----------------')

                        if qstatus:
                            try:
                                b64_decode = base64.b64decode(bytes(answer['message'], 'ascii'))
                                message = dns.message.from_wire(b64_decode)
                                if isinstance(result[nameserver][ip_addr][qrtype], dict):
                                    result[nameserver][ip_addr][qrtype][qproto] = message
                                else:
                                    result[nameserver][ip_addr][qrtype] = message
                            except Exception as exp:
                                if not result[nameserver][ip_addr][qrtype]:
                                    result[nameserver][ip_addr][qrtype] = exp
                                else:
                                    result[nameserver][ip_addr][qrtype][qproto] = exp

                        if not qstatus:
                            qerror = answer['error']
                            qerror_message = answer['error_message']
                            message = map_error_to_answer(qerror, qerror_message)
                            if isinstance(result[nameserver][ip_addr][qrtype], dict):
                                result[nameserver][ip_addr][qrtype][qproto] = message
                            else:
                                result[nameserver][ip_addr][qrtype] = message

                        if qfallback and qdnssec:
                            qerror = answer['fallback_error']
                            qerror_message = answer['fallback_error_message']
                            message = map_error_to_answer(qerror, qerror_message)
                            result[nameserver][ip_addr][qrtype]['udp'] = message

        #print(result)
        return result

    def run(self, nameserver_dict, domain, dnssec=False):
        return self.create_and_fill_result_dict(nameserver_dict, domain, dnssec)
