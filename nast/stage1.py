"""!stage 1 checks"""
import ipaddress
from logging import INFO
import dns.exception
from nast.resolver import Resolver
from nast.issues import NastIssue, IssueCodes, IssueSeverity
from nast.issues import contains_errors, append_issues
from nast.iana import iana


def check_resolver_result(ns_dict, config):
    issue_list = list()
    for nameserver in ns_dict:
        for record_type in ['A', 'AAAA']:
            if isinstance(ns_dict[nameserver][record_type], dns.exception.Timeout):
                issue_list.append(NastIssue(IssueCodes.TIMEOUT_WITH_LOCAL_RESOLVER,
                                            IssueSeverity.ERROR,
                                            {
                                                'target': config.resolver,
                                                'entity': nameserver,
                                                'rtype': record_type
                                            }))
            elif isinstance(ns_dict[nameserver][record_type], ConnectionRefusedError):
                issue_list.append(NastIssue(IssueCodes.CONNECTION_REFUSED_WITH_LOCAL_RESOLVER,
                                            IssueSeverity.ERROR,
                                            {
                                                'target': config.resolver,
                                                'entity': nameserver,
                                                'rtype': record_type
                                            }))
            elif isinstance(ns_dict[nameserver][record_type], dns.exception.DNSException):
                unexpected_excp_issue = NastIssue(IssueCodes.UNEXPECTED_EXCEPTION,
                                            IssueSeverity.ERROR,
                                            {
                                                'target': config.resolver,
                                                'entity': nameserver,
                                                'rtype': record_type,
                                                'error': type(ns_dict[nameserver][record_type]).__name__
                                            })
                unexpected_excp_issue.log(INFO, ns_dict[nameserver][record_type],
                                          1, "check_resolver_result", 
                                          {"params": { "target": config.resolver,
                                                       "rtype": record_type,
                                                       "nameserver": {nameserver: []}}})

                issue_list.append(unexpected_excp_issue)
        if not ns_dict[nameserver]['A'] and not ns_dict[nameserver]['AAAA']:
            issue_list.append(NastIssue(IssueCodes.COULD_NOT_RESOLVE_ANY_IP_ADDRESS,
                                        IssueSeverity.ERROR,
                                        {
                                            'resolver': config.resolver,
                                            'nameserver': nameserver
                                        }))
    return issue_list


def complete_nameserver_entries(params_nameserver, ns_dict):
    for nameserver in params_nameserver:
        if nameserver in ns_dict and 'A' in ns_dict[nameserver] and 'AAAA' in ns_dict[nameserver]:
            params_nameserver[nameserver] = ns_dict[nameserver]['A'] + ns_dict[nameserver]['AAAA']


def is_ip_list_unique(nameserver, ip_list, full_resolved_nameserver_dict):
    for ip in ip_list:
        ip_object = ipaddress.ip_address(ip)
        for other_nameserver, other_ip_list in full_resolved_nameserver_dict.items():
            if other_nameserver == nameserver:
                continue
            for other_ip in other_ip_list:
                other_ip_object = ipaddress.ip_address(other_ip)
                if ip_object == other_ip_object:
                    return False
    return True


def check_redundancy(full_resolved_nameserver_dict):
    """
    2.1.2
    """

    found_ipv4 = False
    for nameserver, ip_list in full_resolved_nameserver_dict.items():
        for ip in ip_list:
            ip_object = ipaddress.ip_address(ip)
            # statt isinstance kannst Du auch "ip_object.version" == 4 abfragen
            if isinstance(ip_object, ipaddress.IPv4Address):
                found_ipv4 = True
    if not found_ipv4:
        return NastIssue(IssueCodes.INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IPV4_ADDRESSES,
                         parameter_dict={
                             'expected': "1",
                             'found': "0"
                         })

    for nameserver, ip_list in full_resolved_nameserver_dict.items():
        if is_ip_list_unique(nameserver, ip_list, full_resolved_nameserver_dict):
            break
    else:
        return NastIssue(IssueCodes.INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IP_ADDRESSES,
                         parameter_dict={
                             'expected': "1",
                             'found': "0"
                         })


def resolve_and_check_ip_addresses(config, params):
    issue_list = list()
    ns_to_resolve = list()

    for nameserver in params['nameserver']:
        if not params['nameserver'][nameserver]:
            ns_to_resolve.append(nameserver)

    if ns_to_resolve:
        resolver = Resolver(config.resolver, config.query_executor)
        ns_dict = resolver.run(ns_to_resolve)
        append_issues(issue_list, check_resolver_result(ns_dict, config))
        if contains_errors(issue_list):
            return issue_list
        complete_nameserver_entries(params['nameserver'], ns_dict)

    # check redundancy
    append_issues(issue_list, check_redundancy(params['nameserver']))
    # check IPv6 addresses
    append_issues(issue_list, iana.check_ipaddresses_from_params(params))
    return issue_list
