"""!NAST issues"""
import copy
import json
import sys
from enum import IntEnum
from hashlib import sha1
from nast.logging import JSON_LOGGER
from logging import ERROR, INFO, error

NAST_ISSUES = {
    101: "Missing glue record for the nameserver",
    102: "Provided glue records not applicable",
    104: "Calculated referral response larger than allowed",
    106: "Inconsistent set of nameserver IP addresses",
    107: "Insufficient diversity of nameserver' s IPaddresses",
    108: "Refresh value out of range",
    109: "Retry value out of range",
    110: "Retry value out of range",
    111: "Expire value out of range",
    112: "Minimum TTL out of range",
    113: "Primary Master (MNAME) inconsistent across SOA records",
    115: "SOA record response must be direct",
    116: "SOA record response must be authoritative",
    118: "Inconsistent set of NS RRs",
    120: "Recursive queries should not be allowed",
    125: "Insufficient diversity of nameserver' s IPv4 addresses",
    127: "Insufficient number of nameservers reachable",
    129: "Invalid IPv4 or IPv6 address",
    130: "IPv6 address is not allocated",
    131: "IPv6 address is not routable",
    132: "Could not resolve any IP address for this nameserver",
    133: "Answer must be authoritative",
    200: "DNSKEY RR ZONE flag (bit 7) must be set",
    201: "DNSKEY RR REVOKE flag (bit 8) must not be set",
    202: "DNSKEY RR SEP flag (bit 15) should be set",
    203: "DNSKEY RR RSA key modulus length in bits out of range",
    204: "DNSKEY RR RSA public key exponent length in bits must not exceed 128 bits",
    205: "DNSKEY RR DSA public key parameter T out of range",
    206: "DNSKEY RR DSA public key has invalid size",
    207: "DNSKEY RR public key must be BASE64 encoded",
    208: "Duplicate DNSKEY RR",
    209: "DNSKEY RR has invalid protocol",
    210: "At most 5 DNSKEY RR allowed",
    211: "Inconsistent DNSKEY RR in nameserver response",
    212: "Did not find DNSKEY RR from request in all nameserver responses",
    213: "Did not find any DNSKEY RR from request in all nameserver responses",
    214: "Querying some authoritative nameservers via EDNS0 UDP failed",
    215: "Timeout after switching from UDP to TCP - switch to TCP due to truncation",
    216: "No visible DNSKEY found signing the DNSKEY RR obtained in response",
    217: "No visible DNSKEY found in signing directly or indirectly the SOA RR obtained in response",
    219: "Unable to retrieve DNSKEY RR with TCP or EDNS0",
    218: "Received invalid answer to a DO-Bit query",
    220: "DNSKEY RR has invalid algorithm",
    221: "Unknown flags in DNSKEY RR are set",
    226: "DNSKEY RR ECDSA public key has invalid size",
    227: "DNSKEY RR GOST public key has invalid size",
    228: "DNSKEY RR ED public key has invalid size",
    901: "Unexpected RCODE",
    902: "Timeout",
    903: "Timeout with local resolver",
    904: "Connection refused with local resolver",
    905: "Port unreachable",
    908: "Connection refused",
    909: "Host unreachable",
    910: "Broken pipe",
    911: "Connection aborted",
    999: "Unexpected Exception"
}

NAST_SEVERITIES = {
    1: 'info',
    2: 'warning',
    3: 'error'
}


class IssueSeverity(IntEnum):
    INFO = 1
    WARNING = 2
    ERROR = 3


class IssueCodes(IntEnum):
    MISSING_GLUE_RECORDS = 101
    PROVIDED_GLUE_RECORDS_NOT_APPLICABLE = 102
    CALCULATED_REFERRAL_RESPONSE_LARGER_THAN_ALLOWED = 104
    # IPV6_ADDRESS_NOT_ALLOCATED_OR_ROUTABLE = 105
    INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES = 106
    SOA_RECORD_NOT_AUTHORITATIVE = 116
    INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IP_ADDRESSES = 107
    REFRESH_VALUE_OUT_OF_RANGE = 108
    RETRY_VALUE_OUT_OF_RANGE = 109
    RETRY_VALUE_OUT_OF_RANGE_REFRESH = 110
    EXPIRE_VALUE_OUT_OF_RANGE = 111
    MINTTL_VALUE_OUT_OF_RANGE = 112
    MNANE_INCONSISTENT_ACROSS_SOA_RECORDS = 113
    SOA_RECORD_RESPONSE_MUST_BE_DIRECT = 115
    INCONSISTENT_SET_OF_NAMESERVER_RRS = 118
    RECURSIVE_QUERIES_SHOULD_NOT_BE_ALLOWED = 120
    INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IPV4_ADDRESSES = 125
    INSUFFICIENT_NUMBER_OF_NAMESERVER_REACHABLE = 127
    INVALID_IP_ADDRESS = 129
    IPV6_ADDRESS_NOT_ALLOCATED = 130
    IPV6_ADDRESS_NOT_ROUTABLE = 131
    COULD_NOT_RESOLVE_ANY_IP_ADDRESS = 132
    ANSWER_NOT_AUTHORITATIVE = 133
    DNSKEY_RR_ZONE_FLAG_MUST_BE_SET = 200
    DNSKEY_RR_REVOKE_FLAG_MUST_NOT_BE_SET = 201
    DNSKEY_RR_SEP_FLAG_SHOULD_BE_SET = 202
    DNSKEY_RR_PUBLIC_KEY_RSA_MODULO_INVALID_LENGTH = 203
    DNSKEY_RR_PUBLIC_KEY_RSA_EXPONENT_INVALID_LENGTH = 204
    DNSKEY_RR_PUBLIC_KEY_DSA_PARAMETER_T_OUT_OF_RANGE = 205
    DNSKEY_RR_PUBLIC_KEY_DSA_INVALID_LENGTH = 206
    DNSKEY_RR_PUBLIC_KEY_FORMAT_INVALID = 207
    DNSKEY_RR_DUPLICATE = 208
    DNSKEY_RR_PROTOCOL_INVALID = 209
    DNSKEY_RR_MAX_NUMBER_EXCEEDED = 210
    DNSKEY_RRSET_INCONSISTENT = 211
    DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES = 212
    ALL_DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES = 213
    SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED = 214
    DNSKEY_RRSET_SIGNATURE_VALIDATION_FAILED = 216
    SOA_RRSET_SIGNATURE_VALIDATION_FAILED = 217
    DNSKEY_OR_SOA_EDNS0_QUERY_NOT_SUPPORTED = 218
    DNSKEY_RRSET_NOT_AVAILABLE = 219
    DNSKEY_RR_ALGORITHM_NOT_SUPPORTED = 220
    DNSKEY_RR_UNKNOWN_FLAGS = 221
    DNSKEY_RR_PUBLIC_KEY_ECDSA_INVALID_LENGTH = 226
    DNSKEY_RR_PUBLIC_KEY_GOST_INVALID_LENGTH = 227
    DNSKEY_RR_PUBLIC_KEY_ED_INVALID_LENGTH = 228  # NEW ISSUE CODE FOR ALGORITHM 15,16
    UNEXPECTED_RCODE = 901
    TIMEOUT = 902
    TIMEOUT_WITH_LOCAL_RESOLVER = 903
    CONNECTION_REFUSED_WITH_LOCAL_RESOLVER = 904
    PORT_UNREACHABLE = 905
    CONNECTION_REFUSED = 908
    HOST_UNREACHABLE = 909
    BROKEN_PIPE = 910
    CONNECTION_ABORTED = 911
    UNEXPECTED_EXCEPTION = 999


class NastIssue:
    def __init__(self, code, severity=IssueSeverity.ERROR, parameter_dict=None):
        if int(code) not in NAST_ISSUES:
            raise ValueError("unknown NAST error code [%d]" % (code))
        if severity not in [IssueSeverity.INFO, IssueSeverity.WARNING, IssueSeverity.ERROR]:
            raise ValueError("unknown severity level [%d]" % (severity))
        self.code = int(code)
        self.severity = severity
        copy_parameter_dict = copy.deepcopy(parameter_dict)
        if copy_parameter_dict:
            for key in copy_parameter_dict:
                copy_parameter_dict[key] = str(copy_parameter_dict[key])
        self.parameter_dict = copy_parameter_dict

    def __str__(self):
        return "NastIssue(code=%d, name=%s, severity=%s, params=%s)" % \
            (self.code, self.get_name(), self.get_severity_name(), str(self.parameter_dict))

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if not isinstance(other, NastIssue):
            return False
        return self.code == other.code and self.severity == other.severity

    def __ne__(self, other):
        if not isinstance(other, NastIssue):
            return True
        return self.code != other.code or self.severity != other.severity

    def __lt__(self, other):
        return self.code < other.code

    def __le__(self, other):
        return self.code <= other.code

    def __gt__(self, other):
        return self.code > other.code

    def __ge__(self, other):
        return self.code >= other.code

    def get_code(self):
        return self.code

    def get_severity(self):
        return self.severity

    def get_severity_name(self):
        return NAST_SEVERITIES[self.severity]

    def get_name(self):
        if self.code in NAST_ISSUES:
            return NAST_ISSUES[self.code]
        return "Unknown Errorcode [%d]" % (self.code)

    def get_params(self):
        return self.parameter_dict

    def is_error(self):
        return self.severity == IssueSeverity.ERROR

    def get_hash_for_aggregation(self):
        data = "code=%d, name=%s, severity=%s, " % (self.code,
                                                    self.get_name(),
                                                    self.get_severity_name())
        if self.parameter_dict:
            # on certain errors, we will ignore the parameter_dict
            if self.code not in [108, 109, 110, 111, 112]:
                data += str(self.parameter_dict)
        # print(data)
        return sha1(bytes(data, "US-ASCII")).hexdigest()

    def log(self,severity, exp, stage="", check="", data={}):
        exc_type, exc_obj, exc_tb = sys.exc_info()
        if exc_type:
            data["type"] = exc_type
        if exc_tb:
            data.update({"file": exc_tb.tb_frame.f_code.co_filename,
                        "line": exc_tb.tb_lineno})
        data.update({"stage": stage,
                     "check": check,
                     "issue_code": self.get_code(),
                     "issue_name": self.get_name(),    
                     "exception": exp.__class__.__name__,
                     "description": str(exp)})
        JSON_LOGGER.log_issue(severity, __name__, data)


def contains_errors(issue_list):
    for issue in issue_list:
        if issue.is_error():
            return True
    return False


def append_issues(issue_list, new_issues):
    if not new_issues:
        return
    if isinstance(new_issues, NastIssue):
        issue_list.append(new_issues)
    if isinstance(new_issues, list):
        issue_list.extend(new_issues)


def aggregate_issues(issue_list):
    aggregated_hashes = set()
    aggregated_list = []
    for issue in issue_list:
        hash = issue.get_hash_for_aggregation()
        if hash not in aggregated_hashes:
            aggregated_hashes.add(hash)
            aggregated_list.append(issue)
    return aggregated_list