# -*- coding: utf-8 -*-
"""!NAST metrics"""

import ipaddress

import dns
from prometheus_client import (REGISTRY, CollectorRegistry, Counter,
                               generate_latest, multiprocess)

from nast.issues import (NAST_ISSUES, NAST_SEVERITIES, IssueCodes,
                         IssueSeverity, NastIssue, contains_errors)
from nast.queries import is_in_zone


class PrometheusMetrics():
    def __init__(self):
        ''' NAST DNS QUERY METRICS '''
        self.queries_counter = Counter(
            "nast_dns_queries_total",
            "Nast DNS Query Counter",
            ['status', 'rtype', 'proto'])
        self.dns_query_status_labels = ['SUCCESS', 'FAIL']
        self.dns_query_rtype_labels = ['SOA', 'NS', 'DNSKEY', 'SOA+DNSSEC', 'A', 'AAAA']
        self.dns_query_proto_labels = ['udp', 'tcp']

        ''' NAST CHECK ISSUE METRICS '''
        self.issues_counter = Counter(
            "nast_issues_total",
            "Nast Issues Counter",
            ['severity', 'code', 'name'])
        self.issues_severity_labels = [IssueSeverity.WARNING, IssueSeverity.ERROR]

        ''' NAST CHECK NETWORK METRICS '''
        self.network_error_counter = Counter(
            "nast_network_error_total",
            "Nast Network Error Counter",
            ['code', 'name', 'ipv', 'proto'])
        self.network_error_code_labels = [IssueCodes.UNEXPECTED_EXCEPTION, IssueCodes.TIMEOUT,
                                            IssueCodes.TIMEOUT_WITH_LOCAL_RESOLVER, IssueCodes.PORT_UNREACHABLE,
                                            IssueCodes.CONNECTION_REFUSED, IssueCodes.UNEXPECTED_EXCEPTION,
                                            IssueCodes.COULD_NOT_RESOLVE_ANY_IP_ADDRESS, IssueCodes.IPV6_ADDRESS_NOT_ALLOCATED,
                                            IssueCodes.IPV6_ADDRESS_NOT_ROUTABLE
                                        ]
        self.network_error_ipv_labels = ['4','6','unknown']
        self.network_error_proto_labels = ['udp', 'tcp']

        ''' NAST INTERNAL SERVER ERRORS'''
        self.nast_server_error_counter = Counter(
            "nast_server_errors_total",
            "Nast Server Error Counter",
            ['code'])
        self.nast_server_error_code_labels = ['500','503','unknown']

        ''' NAST SERVED REQUEST TOTAL COUNTER'''
        self.requests_served_counter = Counter(
            "nast_served_requests_total",
            "Nast Served Request Counter",
            ['api', 'status'])
        self.requests_served_api_labels = ['v1','v2']
        self.requests_served_status_labels = ['SUCCESS','FAIL']

        ''' NAST REQUEST TOTAL COUNTER'''
        self.requests_counter = Counter(
            "nast_requests_total",
            "Nast Request Counter",
            ['api'])
        self.requests_api_labels = ['v1','v2']

        self.init_metrics()

    def init_metrics(self):
        self.init_query_metrics()
        self.init_issue_metrics()
        self.init_network_metrics()
        self.init_server_error_metrics()
        self.init_request_metrics()

    def update_metrics(self, api, issue_list, query_results, nameservers, domain):
        self.update_query_metrics(query_results, nameservers, domain)
        self.update_issue_metrics(issue_list)
        self.update_network_metrics(issue_list)
        status = 'SUCCESS' if not contains_errors(issue_list) else 'FAIL'
        self.update_request_served_metrics(api, status)

    def init_query_metrics(self):
        for status in self.dns_query_status_labels:
            for rtype in self.dns_query_rtype_labels:
                if rtype in ('SOA', 'SOA+DNSSEC', 'DNSKEY'):
                    for proto in self.dns_query_proto_labels:
                        self.queries_counter.labels(status, rtype, proto)
                    continue
                self.queries_counter.labels(status, rtype, 'udp')

    def update_query_metrics(self, query_results, nameservers, domain):
        """
        1. Query Related Metrics
        """
        if query_results and nameservers:
            for nameserver, ip_list in nameservers.items():
                for ip_addr in ip_list:
                    for rtype in self.dns_query_rtype_labels:
                        status = 'SUCCESS'
                        if rtype in query_results[nameserver][ip_addr]:
                            if rtype in ('SOA', 'SOA+DNSSEC', 'DNSKEY'):
                                for proto in self.dns_query_proto_labels:
                                    if proto in query_results[nameserver][ip_addr][rtype]:
                                        answer = query_results[nameserver][ip_addr][rtype][proto]
                                        status = 'FAIL' if self.is_query_failed(answer, rtype, domain) else status
                                        self.queries_counter.labels(status, rtype, proto).inc()
                            else:
                                answer = query_results[nameserver][ip_addr][rtype]
                                if rtype in ["A", "AAAA"] and is_in_zone(nameserver, domain) and self.is_query_failed(answer, rtype, domain):
                                    status = 'FAIL'
                                self.queries_counter.labels(status, rtype, 'udp').inc()

    def is_query_failed(self, query, rtype, domain):
        if isinstance(query, dns.message.Message):
            if query.rcode() == dns.rcode.NOERROR:
                if rtype not in ('AAAA', 'A'):
                    rrset = query.get_rrset(query.answer, dns.name.from_text(domain),
                                            dns.rdataclass.IN, dns.rdatatype.from_text(rtype.split('+')[0]))
                    if rrset:
                        return False
                    return True
                return False
            return True
        return True

    def init_issue_metrics(self):
        for severity in self.issues_severity_labels:
            for code in IssueCodes:
                self.issues_counter.labels(NAST_SEVERITIES[int(severity)].upper(), str(int(code)), NAST_ISSUES[int(code)])

    def update_issue_metrics(self, issue_list):
        """
        2. Issue (Error/Warning) Related Metrics
        """
        for issue in issue_list:
            if isinstance(issue, NastIssue):
                self.issues_counter.labels(issue.get_severity_name().upper(), str(issue.get_code()), issue.get_name()).inc()

    def init_network_metrics(self):
        for err_code in self.network_error_code_labels:
            for ipv in self.network_error_ipv_labels:
                for proto in self.network_error_proto_labels:
                    self.network_error_counter.labels(str(int(err_code)), NAST_ISSUES[int(err_code)], ipv, proto)

    def update_network_metrics(self, issue_list):
        """
        3. Network Error (IPv4/v6) Related Metrics
        """
        for issue in issue_list:
            code = issue.get_code()
            if code in self.network_error_code_labels:
                params = issue.get_params()
                # get related ip version from error
                ipv = 'unknown'
                ipv_tags = ['target', 'ip']
                for tag in ipv_tags:
                    if tag in params:
                        try:
                            ipv = '4' if ipaddress.ip_address(params[tag]).version == 4 else '6'
                        except ValueError:
                            pass
                # get related protocol from error
                proto = 'udp'
                proto_tags = ['protocol']
                for tag in proto_tags:
                    if tag in params:
                        proto =  params[tag] if params[tag] in self.network_error_proto_labels else 'udp'
                self.network_error_counter.labels(str(int(code)), NAST_ISSUES[int(code)], ipv, proto).inc()

    def init_server_error_metrics(self):
         for code in self.nast_server_error_code_labels:
            self.nast_server_error_counter.labels(code)

    def update_server_error_metrics(self, error_code):
        """
        4. Internal Server Error Related Metrics
        """
        if error_code in self.nast_server_error_code_labels:
            self.nast_server_error_counter.labels(error_code).inc()
        else:
            self.nast_server_error_counter.labels('unknown').inc()

    def init_request_metrics(self):
        for api in self.requests_api_labels:
            self.requests_counter.labels(api)

        for status in self.requests_served_status_labels:
            for api in self.requests_served_api_labels:
                self.requests_served_counter.labels(api, status)

    def update_request_served_metrics(self, api, status):
        """
        5. Requests Served Related Metrics
        """
        self.requests_served_counter.labels(api, status).inc()

    def update_request_total_metrics(self, api):
        """
        6. Requests Served Related Metrics
        """
        self.requests_counter.labels(api).inc()

    def update_resolver_metrics(self, ns_dict):
        """
        3. Resolver Metrics
        """
        for _, a_aaaa_dict in ns_dict.items():
            for rtype, ip_list in a_aaaa_dict.items():
                if isinstance(ip_list, dns.exception.DNSException):
                    status = 'FAIL'
                else:
                    status = 'SUCCESS'
                self.queries_counter.labels(status, rtype, 'udp').inc()

    def get_sample_of_dns_queries_counter(self,labels):
        status_labels = labels['status'].split(',') if 'status' in labels else self.dns_query_status_labels
        rtype_labels = labels['rtype'].split(',') if 'rtype' in labels else self.dns_query_rtype_labels
        proto_labels = labels['proto'].split(',') if 'proto' in labels else self.dns_query_proto_labels

        sample = 0
        for status in status_labels:
            for rtype in rtype_labels:
                for proto in proto_labels:
                    value = REGISTRY.get_sample_value('nast_dns_queries_total',{'proto': proto,'rtype': rtype,'status': status})
                    sample += int(value) if value else 0

        return sample

    def get_sample_of_issues_counter(self,labels):
        severity_labels = labels['severity'] if 'severity' in labels else self.issues_severity_labels
        code_labels = labels['code'] if 'code' in labels else IssueCodes

        sample = 0
        for severity in severity_labels:
            for code in code_labels:
                value = REGISTRY.get_sample_value('nast_issues_total',{'name': NAST_ISSUES[int(code)],
                                                                            'code': str(int(code)),
                                                                            'severity': NAST_SEVERITIES[int(severity)].upper()})
                sample += int(value) if value else 0

        return sample

    def get_sample_of_network_counter(self,labels):
        code_labels = labels['code'] if 'code' in labels else self.network_error_code_labels
        ipv_labels = labels['ipv'] if 'ipv' in labels else self.network_error_ipv_labels
        proto_labels = labels['proto'] if 'proto' in labels else self.network_error_proto_labels

        sample = 0
        for code in code_labels:
            for ipv in ipv_labels:
                for proto in proto_labels:
                    value = REGISTRY.get_sample_value('nast_network_error_total',{'name': NAST_ISSUES[int(code)],
                                                                                        'code': str(int(code)),
                                                                                        'ipv': ipv, 'proto': proto })
                    sample += int(value) if value else 0
        return sample


METRICS = PrometheusMetrics()


def get_metrics():
    registry = CollectorRegistry()
    multiprocess.MultiProcessCollector(registry=registry)
    return generate_latest(registry)
