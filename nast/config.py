# -*- coding: utf-8 -*-
"""!NAST configuration"""

import os
import dns.name
import json


class Config():
    def __init__(self):
        self.log_enabled = True
        self.log_level = "INFO"
        self.log_file_path = None
        self.hostname = 'localhost'
        self.cors_allow_origins = ["http://localhost:3000","http://localhost:8080","http://172.31.0.5:8080", "https://nast-webui-dev.cloud.denic.de", "http://nast-webui-dev.cloud.denic.de"]
        self.port = 6007
        self.resolver = "172.31.0.2"
        self.query_executor = "http://172.31.0.8:8080/query"
        self.idna_codec = dns.name.IDNA_2008
        self.timeout = 2
        self.conf_path = "conf"
        self.testmode = False
        self.workers = 150
        if os.path.isdir("/etc/nast"):
            self.conf_path = "/etc/nast"
        self.update_from_environment()

    def update_from_environment(self):
        if "LOG_ENABLED" in os.environ:
            self.log_enabled = json.loads(os.environ['LOG_ENABLED'].lower())
        if "LOG_LEVEL" in os.environ:
            self.log_level = os.environ["LOG_LEVEL"]
        if "LOG_FILE_PATH" in os.environ:
            self.log_file_path = os.environ["LOG_FILE_PATH"]
        if "NAST_HOSTNAME" in os.environ:
            self.hostname = os.environ['NAST_HOSTNAME']
        if "NAST_CORS_ALLOW_ORIGINS" in os.environ:
            self.cors_allow_origins = os.environ['NAST_CORS_ALLOW_ORIGINS'].split("[,;]")
        if "NAST_PORT" in os.environ:
            self.port = int(os.environ['NAST_PORT'])
        if "NAST_RESOLVER" in os.environ:
            self.resolver = os.environ['NAST_RESOLVER']
        if "NAST_QUERY_EXECUTOR" in os.environ:
            self.query_executor = os.environ['NAST_QUERY_EXECUTOR']
        if "NAST_CONF" in os.environ:
            self.conf_path = os.environ['NAST_CONF']
        if "NAST_TESTMODE" in os.environ:
            self.testmode = json.loads(os.environ['NAST_TESTMODE'].lower())
        if "NAST_TIMEOUT" in os.environ:
            self.timeout = float(os.environ['NAST_TIMEOUT'])
        if "NAST_QUERY_WORKERS" in os.environ:
            self.workers = int(os.environ['NAST_QUERY_WORKERS'])
