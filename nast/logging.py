# -*- coding: utf-8 -*-
"""!NAST logging"""

from ast import arguments
import copy
import urllib.parse
import json
import logging
import sys
from time import gmtime, strftime
from nast.config import Config

class JSONLogger():

    class ElasticISOTimeFormatter(logging.Formatter):
        def formatTime(self, record, datefmt=None):
            asc_time = strftime("%Y-%m-%dT%H:%M:%S", gmtime(record.created))
            asc_zone = strftime("%z",gmtime(record.created))
            date_time = "%s.%03d%s" % (asc_time, record.msecs, asc_zone)
            return date_time

    def __init__(self):
        self.config = Config()
        self.log_enabled= self.config.log_enabled
        self.log_level = self.get_loglevel_from_string(self.config.log_level)
        # setup logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.log_level)
        # setup formatter
        formatter = self.ElasticISOTimeFormatter( '{"time": "%(asctime)s", ' \
                                        '"level": "%(levelname)s", ' \
                                        '"api": "%(api)s", ' \
                                        '"source": "%(src)s", ' \
                                        '"type": "%(type)s", ' \
                                        '"message": "%(message)s", ' \
                                        '"data": %(data)s}')
        # setup stdout handler
        self.log_handler = logging.StreamHandler(sys.stdout)
        self.log_handler.setLevel(self.log_level)
        self.log_handler.setFormatter(formatter)
        self.logger.addHandler(self.log_handler)
        # additional log file handler if LOG_FILE_PATH was configured
        self.log_file_handler = None
        if self.config.log_file_path:
            self.log_file_handler = logging.FileHandler(self.config.log_file_path, mode='w')
            self.log_file_handler.setLevel(self.log_level)
            self.log_file_handler.setFormatter(formatter)
            self.logger.addHandler(self.log_file_handler)

    def __log(self, severity, src="", msg="", data={}, type="check"):
        if self.log_enabled:
            api = self.get_api_version(src)

            self.logger.log(self.get_loglevel_from_string(str(severity)), msg,
                            extra={"api": api,
                                   "src": src,
                                   "data": json.dumps(data,indent=None,sort_keys=True),
                                   'type': type})

    def log_message(self, severity, src="", msg=""):
        self.__log(severity, src, msg, type="message")
    
    def log_exception(self, severity, src="", msg="", trace=""):
        self.__log(severity, src, msg, data={'trace': trace}, type="exception")

    def log_issue(self, severity, src="", data={}):
        # format nameserver params as lists instead of key/value pairs due to auto creation
        # of multiple dedicated nameserver fields in elastic filebeat json log parser
        if "params" in data and "nameserver" in data["params"]:
            self.format_params_nameserver_to_json_log_array(data["params"])
        self.__log(severity, src, data=data, type="issue")

    def log_resolve(self, severity, src="", data={}):
        # format nameserver params as lists instead of key/value pairs due to auto creation
        # of multiple dedicated nameserver fields in elastic filebeat json log parser
        data_copy = copy.deepcopy(data)
        if "answer" in data_copy and "nameserver" in data_copy["answer"]:
            self.format_params_nameserver_to_json_log_array(data_copy["answer"])
        self.__log(severity, src, data=data_copy, type="resolve")

    def log_check(self, severity, src="", data={}):
        # format nameserver params as lists instead of key/value pairs due to auto creation
        # of multiple dedicated nameserver fields in elastic filebeat json log parser
        data_copy = copy.deepcopy(data)
        # remove optional debug data from log due to max log message length of 8K
        data_copy["answer"].pop("debug", None)
        if "params" in data_copy and "nameserver" in data_copy["params"]:
            self.format_params_nameserver_to_json_log_array(data_copy["params"])
        if "answer" in data_copy and "issues" in data_copy["answer"]:
            self.format_issue_arguments_to_json_log_array(data_copy["answer"]["issues"])
        self.format_check_weblink(data_copy)
        self.__log(severity, src, data=data_copy, type="check")

    def format_params_nameserver_to_json_log_array(self, params):
        log_nameserver=[]
        if isinstance(params["nameserver"], dict):
            for nameserver, ip_list in params["nameserver"].items():
                log_nameserver.append(((nameserver.strip(".") + ',' + ','.join(ip_list)).strip()).strip(","))
            params["nameserver"] = log_nameserver
    
    def format_issue_arguments_to_json_log_array(self, issues):
        for issue in issues:
            log_arguments=[]
            if "arguments" in issue and isinstance(issue['arguments'], dict):
                for arg_key, arg_value in issue['arguments'].items():
                    log_arguments.append(arg_key+ ":"+arg_value)
                issue["arguments"]=log_arguments

    def get_loglevel_from_string(self, level):
        level = level.upper()
        if level == "DEBUG":
            return logging.DEBUG
        if level == "INFO":
            return logging.INFO
        if level == "WARNING":
            return logging.WARNING
        if level in ["CRITICAL", "CRIT"]:
            return logging.CRITICAL
        return logging.INFO

    def get_api_version(self, src):
        api="unknown"
        if src is not None:
            if "api_handler" in src:
                api="v1"
            if "api_handler_v2" in src:
                api="v2"
        return api

    def get_log_handler(self):
        return self.log_handler

    def get_log_file_handler(self):
        return self.log_file_handler

    def get_root_logger(self):
        return self.logger

    def disable_logging(self):
        self.log_enabled=False

    def enable_logging(self):
        self.log_enabled=True

    def format_check_weblink(self, data):
         if "domainname" in data and "params" in data and "nameserver" in data["params"]:
            params = []
            domain = data['domainname']
            nameservers = data["params"]["nameserver"]
            url = '{}/{}'.format('https://nast.denic.de/result', domain)
            
            ns_index=1
            for ns in nameservers:
                ns_key = "nsX" if ns_index > 2 else "ns"+str(ns_index)
                params.append((ns_key,ns))
                ns_index+=1

            if "dnskey" in data["params"]:
                for dnskey in data["params"]["dnskey"]:
                    key_wtext = '{} {} {} {}'.format(str(dnskey['flags']).replace(" ",""),
                    str(dnskey['protocol']).replace(" ",""), str(dnskey['algorithm']).replace(" ",""),
                    dnskey['key'].replace(" ",""))
                    params.append(("dnskey", key_wtext))

            url = '{}?{}'.format(url, urllib.parse.urlencode(params))
            data['link']=url

JSON_LOGGER = JSONLogger()
