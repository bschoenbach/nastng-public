"""!stage 3 checks"""
import base64
import hashlib
import dns.exception
import dns.message
import dns.rdatatype
import dns.rdataclass
import dns.name
import dns.flags
import dns.dnssec
import dns.query
import dns.rdtypes.ANY.DNSKEY
from nast.issues import IssueCodes, IssueSeverity, NastIssue, append_issues, contains_errors


def check_dnssec(config, params, query_results):
    issue_list = list()

    if len(params['dnskey']) > 0:
        query_dnskeys, query_rdata = create_dnssec_dict(query_results, params['nameserver'], params['domain'])
        # checks 3.6.1 - Execution moved to stage0
        #append_issues(issue_list, check_params(params['dnskey']))
        #if contains_errors(issue_list):
        #    return issue_list
        # checks 3.6.2
        issues_3_6_2, visible_dnskeys, rrset_dnskeys = check_rrset_identical_visible(params['nameserver'], params['dnskey'], query_dnskeys)
        append_issues(issue_list, issues_3_6_2)
        # checks 3.6.3
        append_issues(issue_list, check_proof_of_possession(params['nameserver'], visible_dnskeys, query_rdata, params['domain']))
        # checks 3.6.4
        append_issues(issue_list, check_chain_of_trust(params['nameserver'], rrset_dnskeys, query_rdata, params['domain']))
        # checks 3.6.5
        append_issues(issue_list, check_cross_rules(params['nameserver'], query_results, query_rdata))

    return issue_list

def check_cross_rules(nameservers, query_results, query_rdata):
    """
    3.6.5
    """
    issue_list = list()
    append_issues(issue_list, check_edns_support(nameservers, query_results, query_rdata))
    append_issues(issue_list, check_udp_edns_protocol(nameservers, query_results))
    append_issues(issue_list, check_dnskey_rrset(nameservers, query_results, query_rdata))
    return issue_list

def check_edns_support(nameservers, query_results, query_rdata):
    """
    3.6.5.1
    """
    issue_list = list()
    rtypes = ['DNSKEY', 'SOA+DNSSEC']

    for nameserver, ip_list in nameservers.items():
        for ip_addr in ip_list:
            for rtype in rtypes:
                response = get_available_dnssec_query_response(query_results, nameserver, ip_addr, rtype)
                errors = list()
                if response:
                    if is_query_answer_ok(response):
                        # check if DO-Bit is set in ednsflags
                        if dns.flags.DO > response.ednsflags:
                            errors.append('DO-Bit missing')
                        # check if RRSIG is present in answer section
                        if not query_rdata[nameserver][ip_addr][rtype.split('+', maxsplit=1)[0]]['RRSIG']:
                            errors.append('RRSIG missing')
                    # check if edns opt header is ok
                    if isinstance(response, dns.message.BadEDNS):
                        errors.append('OPT invalid')
                    if errors:
                        issue_list.append(NastIssue(IssueCodes.DNSKEY_OR_SOA_EDNS0_QUERY_NOT_SUPPORTED,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'nameserver': str(nameserver),
                                                'ip': str(ip_addr),
                                                'record': rtype.split('+', maxsplit=1)[0],
                                                'found' : ', '.join(errors)
                                            }))

    return issue_list

def check_udp_edns_protocol(nameservers, query_results):
    """
    3.6.5.2
    """
    issue_list = list()
    rtypes = ['DNSKEY', 'SOA+DNSSEC']

    truncation_error = set()
    timeout_error = set()
    connection_error = set()
    unknown_error = set()
    for nameserver, ip_list in nameservers.items():
        for ip_addr in ip_list:
            for rtype in rtypes:
                udp_response = query_results[nameserver][ip_addr][rtype]['udp']
                if udp_response and not is_query_answer_ok(udp_response):
                    # check if UDP query due to truncation
                    if isinstance(udp_response, dns.message.Truncated):
                        truncation_error.add(nameserver)
                    # check if UDP query failed due to timeout
                    elif isinstance(udp_response, dns.exception.Timeout):
                        timeout_error.add(nameserver)
                    # check if UDP query failed due to connection refused
                    elif isinstance(udp_response, ConnectionRefusedError):
                        connection_error.add(nameserver)
                    # UDP query was failed due to unknown dns reason
                    else:
                        unknown_error.add(nameserver)
    if truncation_error:
        issue_list.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED,
                                    IssueSeverity.WARNING, parameter_dict={
                                        'nameservers': ', '.join(sorted(truncation_error)),
                                        'error': 'truncation'
                                    }))
    if timeout_error:
        issue_list.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED,
                                    IssueSeverity.WARNING, parameter_dict={
                                        'nameservers': ', '.join(sorted(timeout_error)),
                                        'error': 'timeout'
                                    }))
    if connection_error:
        issue_list.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED,
                                    IssueSeverity.WARNING, parameter_dict={
                                        'nameservers': ', '.join(sorted(connection_error)),
                                        'error': 'connection refused'
                                    }))
    if unknown_error:
        issue_list.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED,
                                    IssueSeverity.WARNING, parameter_dict={
                                        'nameservers': ', '.join(sorted(unknown_error)),
                                        'error': 'unknown'
                                    }))

    return issue_list

def check_dnskey_rrset(nameservers, query_results, query_rdata):
    """
    3.6.5.3
    """
    issue_list = list()

    for nameserver, ip_list in nameservers.items():
        for ip_addr in ip_list:
            # global check if dnskey rrset is not available
            if not query_rdata[nameserver][ip_addr]['DNSKEY']['RRSET']:
                dnskey_response = get_available_dnssec_query_response(query_results, nameserver, ip_addr, 'DNSKEY')

                # check if dnskey rrset query failed by exception
                if not is_query_answer_ok(dnskey_response):
                    # check if dnskey rrset TCP query failed finally by timeout after switching from UDP to TCP
                    if isinstance(dnskey_response, dns.exception.Timeout):
                        issue_list.append(
                                    NastIssue(IssueCodes.TIMEOUT,
                                    IssueSeverity.ERROR,
                                    parameter_dict={
                                        'nameserver': nameserver,
                                        'ip': str(ip_addr),
                                        'protocol': 'tcp',
                                        'record': 'DNSKEY'
                                    }))
                    # check if dnskey rrset TCP query failed finally by connection refused after switching from UDP to TCP
                    if isinstance(dnskey_response, ConnectionRefusedError):
                        issue_list.append(
                                    NastIssue(IssueCodes.CONNECTION_REFUSED,
                                    IssueSeverity.ERROR,
                                    parameter_dict={
                                        'nameserver': nameserver,
                                        'ip': str(ip_addr),
                                        'protocol': 'tcp',
                                        'record': 'DNSKEY',
                                    }))
                # dnskey rrset not present in query answer
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RRSET_NOT_AVAILABLE,
                                    IssueSeverity.ERROR,
                                    parameter_dict={
                                        'nameserver': nameserver,
                                        'ip': str(ip_addr)
                                     }))
    return issue_list

def check_chain_of_trust(nameservers, rrset_dnskeys, query_rdata, domain):
    """
    3.6.4
    """
    return check_rrsig_of_rrset(nameservers, 'SOA', query_rdata, rrset_dnskeys, domain)

def check_proof_of_possession(nameservers, visible_dnskeys, query_rdata, domain):
    """
    3.6.3
    """
    return check_rrsig_of_rrset(nameservers, 'DNSKEY', query_rdata, visible_dnskeys, domain)

def check_rrsig_of_rrset(nameservers, dtype, query_rdata, dnskeys, domain):
    """
    3.6.3 + 3.6.4 SIGNATURE CHECKS (i.e. rrset of SOA, DNSKEY)
    """
    issue_list = list()
    key_rdataset = to_key_rdataset(dnskeys)
    name =  dns.name.from_text(domain)

    for nameserver, ip_list in nameservers.items():
        for ip_addr in ip_list:
            try:
                rrset = query_rdata[nameserver][ip_addr][dtype]['RRSET']
                rrsigset = query_rdata[nameserver][ip_addr][dtype]['RRSIG']

                if rrset and rrsigset and key_rdataset:
                    dns.dnssec.validate(rrset, rrsigset, {name: key_rdataset}, name)

            except dns.dnssec.ValidationFailure:
                code = (IssueCodes.DNSKEY_RRSET_SIGNATURE_VALIDATION_FAILED if dtype == 'DNSKEY'
                            else IssueCodes.SOA_RRSET_SIGNATURE_VALIDATION_FAILED)
                issue_list.append(NastIssue(code, IssueSeverity.ERROR, parameter_dict={
                                            'nameserver': nameserver,
                                            'ip': str(ip_addr),
                                            'record': str(dtype)
                                        }))

    return issue_list

def check_rrset_identical_visible(nameservers, params_dnskeys, query_dnskeys):
    """
    3.6.2
    """
    issue_list = list()
    visible_dnskeys = list()
    rrset_dnskeys = list()

    # create nameserver related dnskey hashes + comparision sets
    dnssec_struct = dict()
    for nameserver, ip_list in nameservers.items():
        dnssec_struct[nameserver] = {'hashes': list(), 'set': set(), 'keys': list()}
        for ip_addr in ip_list:
            dnskeys = query_dnskeys[nameserver][ip_addr]
            dnssec_struct[nameserver][ip_addr] = sorted(to_key_hashes(dnskeys))
        # check if dnskey rrset of nameserver is identical (all IPs of the nameserver are considered)
        if len(ip_list) >= 2:
            for index in range(0, len(ip_list)-1):
                if dnssec_struct[nameserver][ip_list[index]] != dnssec_struct[nameserver][ip_list[index+1]]:
                    issue_list.append(NastIssue(IssueCodes.DNSKEY_RRSET_INCONSISTENT,
                                                IssueSeverity.ERROR, parameter_dict={
                                                    'nameserver': str(nameserver)
                                                }))
                    dnssec_struct[nameserver]['hashes'] = []
                    dnssec_struct[nameserver]['keys'] = []
                    break
                dnssec_struct[nameserver]['hashes'] = dnssec_struct[nameserver][ip_list[index]]
                dnssec_struct[nameserver]['keys'] = query_dnskeys[nameserver][ip_list[index]]
        if len(ip_list) == 1:
            dnssec_struct[nameserver]['hashes'] = dnssec_struct[nameserver][ip_list[0]]
            dnssec_struct[nameserver]['keys'] = query_dnskeys[nameserver][ip_list[0]]
    # check if dnskey rrsets of all nameservers are identical
    # contains sets of nameservers with identical rrset e.g. [{'ns1','n2'},{'ns3'}] where rrset of ns1 == ns2 != ns3
    identical_nsset = list()
    for nameserver in nameservers:
        for nameserver_x in nameservers:
            if dnssec_struct[nameserver]['hashes'] == dnssec_struct[nameserver_x]['hashes']:
                dnssec_struct[nameserver]['set'].add(nameserver_x)
        if dnssec_struct[nameserver]['set'] not in identical_nsset:
            identical_nsset.append(dnssec_struct[nameserver]['set'])
        if len(dnssec_struct[nameserver]['set']) == len(nameservers):
            rrset_dnskeys = dnssec_struct[nameserver]['keys']
            break
    if len(identical_nsset) > 1:
        identical_nsset_string = list()
        for nsset in identical_nsset:
            identical_nsset_string.append(', '.join(sorted(list(nsset))))
        message = ' != '.join(identical_nsset_string)
        issue_list.append(NastIssue(IssueCodes.DNSKEY_RRSET_INCONSISTENT,
                                        IssueSeverity.ERROR, parameter_dict={
                                            'nameservers': str(message)
                                        }))
    # check if at least one param dnskey is visible
    at_least_one_key_visible = False
    for key in params_dnskeys:
        key_hash = to_key_hash(key)
        visible = True
        for nsset in identical_nsset:
            nameserver_x = next(iter(nsset))
            if key_hash not in dnssec_struct[nameserver_x]['hashes']:
                visible = False
                break
        if not visible:
            issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES,
                                        IssueSeverity.WARNING, parameter_dict={
                                            'key': to_key_wire_text(key)
                                        }))
        else:
            at_least_one_key_visible = True
            visible_dnskeys.append(key)
    if not at_least_one_key_visible and params_dnskeys:
        issue_list.append(NastIssue(IssueCodes.ALL_DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES,
                                        IssueSeverity.ERROR
                                        ))

    return issue_list, visible_dnskeys, rrset_dnskeys

def check_params(dnskeys):
    """
    3.6.1
    """
    issue_list = list()

    append_issues(issue_list, check_params_uniqueness(dnskeys))
    append_issues(issue_list, check_params_protocol(dnskeys))
    append_issues(issue_list, check_params_flags(dnskeys))
    append_issues(issue_list, check_params_algorithm(dnskeys))
    append_issues(issue_list, check_params_public_key(dnskeys))

    return issue_list

def check_params_uniqueness(dnskeys):
    """
    3.1 + 3.6.1 GENERAL CHECKS
    """
    issue_list = list()

    # max five keys can be handed over in request
    if len(dnskeys) > 5:
        issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_MAX_NUMBER_EXCEEDED,
                                        IssueSeverity.ERROR))
    # every key must be unique
    hashed_keys = list()
    for key in dnskeys:
        key_hash = to_key_hash(key)
        if key_hash in hashed_keys:
            issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_DUPLICATE,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'key': to_key_wire_text(key)
                                            }))
        else:
            hashed_keys.append(key_hash)

    return issue_list

def check_params_protocol(dnskeys):
    """
    3.6.1.1 CHECK PROTOCOL
    """
    issue_list = list()

    for key in dnskeys:
        # CHECK PROTOCOL: must be 3 else specific ERROR Issue
        proto = to_int(key['protocol'])
        if proto != 3:
            issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PROTOCOL_INVALID,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'expected': '3',
                                                'found': str(proto)
                                            }))
    return issue_list

def check_params_flags(dnskeys):
    """
    3.6.1.1 CHECK FLAGS
    """
    issue_list = list()

    for key in dnskeys:
        # CHECK FLAGS: must be in range [256, 257] else specific ERROR, WARNING Issue
        flags = to_int(key['flags'])

        if isinstance(flags, int):
            if not (flags & 0b100000000):
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_ZONE_FLAG_MUST_BE_SET,
                                            IssueSeverity.ERROR))
            
            if flags & 0b010000000:
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_REVOKE_FLAG_MUST_NOT_BE_SET,
                                                IssueSeverity.ERROR))

            if not (flags & 0b000000001):
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_SEP_FLAG_SHOULD_BE_SET,
                                            IssueSeverity.WARNING))
        else:
            issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_UNKNOWN_FLAGS,
                                            IssueSeverity.ERROR))

    return issue_list

def check_params_algorithm(dnskeys):
    """
    3.6.1.3 CHECK ALGORITHM
    """
    issue_list = list()
    # SUPPORT OF NEW ED25519 (15) AND ED448 (16) POSTPONED UNTIL RELEASE IN RRI
    supported_algos = [3, 5, 6, 7, 8, 10, 12, 13, 14 ] #, 15, 16]
    for key in dnskeys:
        alg = to_int(key['algorithm'])
        if alg not in supported_algos :
            issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_ALGORITHM_NOT_SUPPORTED,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'expected': ','.join([str(item) for item in supported_algos]),
                                                'found': str(alg)
                                            }))

    return issue_list

def check_params_public_key(dnskeys):
    """
    3.6.1.4 CHECK PUBLIC KEY
    """
    issue_list = list()

    for key in dnskeys:
        alg = to_int(key['algorithm'])
        pub_key_base64 = ''.join(key['key']).replace(" ","")
        pub_key_bytes = bytes()
        pub_key_len_byte = 0
        pub_key_len_bit = 0

        # Base64 encoding check
        if not is_base64_encoded(pub_key_base64):
            issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_FORMAT_INVALID,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'expected': 'Base64 String',
                                                'found': str(pub_key_base64)
                                            }))
            continue
        pub_key_bytes = base64.b64decode(pub_key_base64)
        pub_key_len_byte = len(pub_key_bytes)
        pub_key_len_bit = pub_key_len_byte*8

        # RSA related encoding checks
        if alg in [5, 7, 8, 10]:
            if pub_key_bytes[0] == 0:
                exp_offset = 3
                exp_len_byte = int.from_bytes(pub_key_bytes[1:exp_offset], 'big')
            if pub_key_bytes[0] != 0:
                exp_offset = 1
                exp_len_byte = int.from_bytes(pub_key_bytes[0:exp_offset], 'big')
            mod_len_byte = 0 if (exp_offset + exp_len_byte) > pub_key_len_byte else pub_key_len_byte - exp_offset - exp_len_byte

            # check modulo length
            if (mod_len_byte*8) not in range(512,4097) :
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_MODULO_INVALID_LENGTH,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'expected': 'modulo bit length in range [512,4096]',
                                                'found': 'modulo bit length '+str((mod_len_byte*8))
                                            }))
            # check exp length
            if (exp_len_byte*8) not in range(8,129):
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_EXPONENT_INVALID_LENGTH,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'expected': 'exponent bit length in range [8,128]',
                                                'found': 'exponent bit length '+str(exp_len_byte*8)
                                            }))

        # DSA related encoding checks
        if alg in [3, 6]:
            # check range of parameter T
            param_t = int(pub_key_bytes[0])
            if param_t not in range(0,9):
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_DSA_PARAMETER_T_OUT_OF_RANGE,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'expected': 'T parameter in range [0,8]',
                                                'found': 'T parameter '+str(param_t)
                                            }))
            # check key length according parameter T
            expected_key_len_byte = (param_t * 24) + 213
            if pub_key_len_byte != expected_key_len_byte:
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_DSA_INVALID_LENGTH,
                                            IssueSeverity.ERROR, parameter_dict={
                                                'expected': 'key byte length '+str(expected_key_len_byte),
                                                'found': 'key byte length '+str(pub_key_len_byte)
                                            }))

        # GOST related encoding checks
        if alg == 12:
            if pub_key_len_bit != 512:
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_GOST_INVALID_LENGTH,
                                        IssueSeverity.ERROR, parameter_dict={
                                            'expected': 'key bit length 512',
                                            'found': 'key bit length '+str(pub_key_len_bit)
                                        }))


        # ECDSA related encodingTrue checks
        if alg in [13, 14]:
            if alg == 13 and pub_key_len_bit != 512:
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ECDSA_INVALID_LENGTH,
                                        IssueSeverity.ERROR, parameter_dict={
                                            'expected': 'key bit length 512',
                                            'found': 'key bit length '+str(pub_key_len_bit)
                                        }))
            if alg == 14 and pub_key_len_bit != 768:
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ECDSA_INVALID_LENGTH,
                                        IssueSeverity.ERROR, parameter_dict={
                                            'expected': 'key bit length 768',
                                            'found': 'key bit length '+str(pub_key_len_bit)
                                        }))

        # ED related encoding checks
        if alg in [15, 16] :
            if alg == 15 and pub_key_len_bit != 256:
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ED_INVALID_LENGTH,
                                        IssueSeverity.ERROR, parameter_dict={
                                            'expected': 'key bit length 256',
                                            'found': 'key bit length '+str(pub_key_len_bit)
                                        }))
            if alg == 16 and pub_key_len_bit != 456:
                issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ED_INVALID_LENGTH,
                                        IssueSeverity.ERROR, parameter_dict={
                                            'expected': 'key bit length 456',
                                            'found': 'key bit length '+str(pub_key_len_bit)
                                        }))

    return issue_list

def is_base64_encoded(str_or_bytes):
    if len(str_or_bytes) == 0:
        return False
    try:
        if isinstance(str_or_bytes, str):
        # If there's any unicode here, an exception will be thrown and the function will return false
            sb_bytes = bytes(str_or_bytes, 'ascii')
        elif isinstance(str_or_bytes, bytes):
            sb_bytes = str_or_bytes
        else:
            raise ValueError("Argument must be string or bytes")
        return base64.b64encode(base64.b64decode(sb_bytes)) == sb_bytes
    except Exception:
        return False

def to_int(str_int):
    try:
        return int(str_int)
    except ValueError:
        return str_int

def to_key_wire_text(dnskey):
    key_wtext = '{} {} {}'.format(str(dnskey['flags']).replace(" ",""),
                    str(dnskey['algorithm']).replace(" ",""), dnskey['key'].replace(" ",""))
    return key_wtext

def to_key_wire_texts(dnskeys):
    key_wtexts = list()
    for key in dnskeys:
        key_wtexts.append(to_key_wire_text(key))
    return ','.join(key_wtexts)

def to_key_rdataset(dnskeys):
    rdataset = list()
    for key in dnskeys:
        rdataset.append(to_key_rdata(key))
    if len(rdataset) > 0:
        return dns.rdataset.from_rdata_list(3600, rdataset)
    return None

def to_key_rdata(dnskey):
    return dns.rdtypes.ANY.DNSKEY.DNSKEY(dns.rdataclass.IN, dns.rdatatype.DNSKEY,
                                            to_int(dnskey['flags']), 3, to_int(dnskey['algorithm']),
                                            base64.b64decode(dnskey['key'].replace(" ","")))

def to_key_hash(dnskey):
    key_str = str(dnskey['flags']).replace(" ","")+str(dnskey['algorithm']).replace(" ","")+dnskey['key'].replace(" ","")
    key_hash = hashlib.md5(key_str.encode('utf-8')).hexdigest()
    return key_hash

def to_key_hashes(dnskeys):
    hashes = list()
    for key in dnskeys:
        hashes.append(to_key_hash(key))
    return hashes

def is_query_answer_ok(answer):
    if not isinstance(answer, dns.message.Message):
        return False
    if answer.rcode() != dns.rcode.NOERROR:
        return False
    return True

def get_available_dnssec_query_response(query_results, nameserver, ip_addr, rtype):
    if rtype in ['DNSKEY','SOA+DNSSEC']:
        proto = 'tcp' if 'tcp' in query_results[nameserver][ip_addr][rtype] else 'udp'
        return query_results[nameserver][ip_addr][rtype][proto]
    return None

def create_dnssec_dict(query_results, nameserver_dict, domain):
    dnskey_dict = dict()
    rdata_dict = dict()

    for nameserver, ip_list in nameserver_dict.items():
        dnskey_dict[nameserver] = dict()
        rdata_dict[nameserver] = dict()
        for ip_addr in ip_list:
            dnskey_dict[nameserver][ip_addr] = list()
            rdata_dict[nameserver][ip_addr] = dict()
            rdata_dict[nameserver][ip_addr]['SOA'] = {'RRSET': None, 'RRSIG': None}
            rdata_dict[nameserver][ip_addr]['DNSKEY'] = { 'RRSET': None, 'RRSIG': None }
            # SOA
            querry_response = get_available_dnssec_query_response(query_results, nameserver, ip_addr, 'SOA+DNSSEC')
            if is_query_answer_ok(querry_response):
                soa_rrset = querry_response.get_rrset(querry_response.answer, dns.name.from_text(domain),
                                dns.rdataclass.IN, dns.rdatatype.SOA)
                rrsig_soa_rrset = querry_response.get_rrset(querry_response.answer, dns.name.from_text(domain),
                                        dns.rdataclass.IN, dns.rdatatype.RRSIG, dns.rdatatype.SOA)
                rdata_dict[nameserver][ip_addr]['SOA']['RRSET'] = soa_rrset
                rdata_dict[nameserver][ip_addr]['SOA']['RRSIG'] = rrsig_soa_rrset
            # DNSKEY
            querry_response = get_available_dnssec_query_response(query_results, nameserver, ip_addr, 'DNSKEY')
            if is_query_answer_ok(querry_response):
                dnskey_rrset = querry_response.get_rrset(querry_response.answer, dns.name.from_text(domain),
                                dns.rdataclass.IN, dns.rdatatype.DNSKEY)
                rrsig_dnskey_rrset = querry_response.get_rrset(querry_response.answer, dns.name.from_text(domain),
                                        dns.rdataclass.IN, dns.rdatatype.RRSIG, dns.rdatatype.DNSKEY)
                if not dnskey_rrset:
                    continue
                for record in dnskey_rrset:
                    dnskey_dict[nameserver][ip_addr].append({'flags': int(record.flags),
                                                            'algorithm': int(record.algorithm),
                                                            'key': str(base64.b64encode(record.key), "ascii")})
                rdata_dict[nameserver][ip_addr]['DNSKEY']['RRSET'] = dnskey_rrset
                rdata_dict[nameserver][ip_addr]['DNSKEY']['RRSIG'] = rrsig_dnskey_rrset

    return dnskey_dict, rdata_dict
