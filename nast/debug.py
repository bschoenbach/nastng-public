"""!NAST debug information"""
import dns.message
import dns.flags
import dns.rcode


def get_records(rr_set):
    result = []
    for record in rr_set:
        result.extend(str(record).split("\n"))
    return result


def get_dns_answer(record):
    if isinstance(record, dns.message.Message):
        return {'result': 'success',
                'id': record.id,
                'flags': dns.flags.to_text(record.flags),
                'rcode': dns.rcode.to_text(record.rcode()),
                'answer': get_records(record.answer),
                'additional': get_records(record.additional),
                'authority': get_records(record.authority),
                'question': get_records(record.question),
                }
    if isinstance(record, dns.exception.Timeout):
        return {'result': 'failed',
                'error': record.__class__.__name__,
                'message': 'The DNS operation timed out'}
    if isinstance(record, dns.message.Truncated):
        return {'result': 'failed',
                'error': record.__class__.__name__,
                'message': 'The answer was truncated'}

    return {'result': 'failed',
            'error': record.__class__.__name__,
            'message': str(record)}


def get_parsed_query_results(query_results):
    if not query_results:
        return {}
    data = {}
    for nameserver in query_results:
        data[nameserver] = {}
        for ip in query_results[nameserver]:
            data[nameserver][ip] = {}
            for rr_type in query_results[nameserver][ip]:
                data[nameserver][ip][rr_type] = {}
                if rr_type in ('SOA', 'SOA+DNSSEC', 'DNSKEY'):
                    for proto in ['udp', 'tcp']:
                        if proto in query_results[nameserver][ip][rr_type]:
                            data[nameserver][ip][rr_type][proto] = get_dns_answer(query_results[nameserver][ip][rr_type][proto])
                else:
                    data[nameserver][ip][rr_type]['udp'] = get_dns_answer(query_results[nameserver][ip][rr_type])

    return data