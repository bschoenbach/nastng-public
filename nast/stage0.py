"""!stage 0 checks"""
import ipaddress
from logging import INFO
from typing import Dict, List

import dns.message
import dns.name
import dns.rdataclass
import dns.rdatatype
import dns.rrset

from nast import stage3
from nast.config import Config
from nast.issues import IssueCodes, IssueSeverity, NastIssue, append_issues
from nast.queries import is_in_zone


def parameter_check(_config: Config, params: Dict) -> List[NastIssue]:
    # do we need _config?
    issue_list = list()

    convert_idn_to_ace(params, _config.idna_codec)

    strip_dots(params)

    if len(params["nameserver"]) < 2:
        append_issues(
            issue_list,
            NastIssue(
                IssueCodes.INSUFFICIENT_NUMBER_OF_NAMESERVER_REACHABLE,
                parameter_dict={
                    "expected": "2",
                    "found": str(len(params["nameserver"])),
                },
            ),
        )

    # check ip addresses of glue-rr
    append_issues(issue_list, check_ip_addresses_of_glue_rr(params))

    # check glue records
    append_issues(issue_list, check_glue_records(params))

    # check referral response size
    append_issues(issue_list, check_referral_response_size(params))

    # checks dnssec key parameters 3.6.1
    if len(params["dnskey"]) > 0:
        append_issues(issue_list, check_dnssec_params(params["dnskey"]))
    return issue_list


def check_dnssec_params(dnskeys):
    """
    3.6.1
    """
    issue_list = list()

    append_issues(issue_list, stage3.check_params_uniqueness(dnskeys))
    append_issues(issue_list, stage3.check_params_protocol(dnskeys))
    append_issues(issue_list, stage3.check_params_flags(dnskeys))
    append_issues(issue_list, stage3.check_params_algorithm(dnskeys))
    append_issues(issue_list, stage3.check_params_public_key(dnskeys))

    return issue_list


def convert_idn_to_ace(params: Dict, idna_codec: dns.name.IDNACodec) -> None:
    """Converts domain and nameserver names into ACE-Strings in the passed params dict.

    Args:
        params (Dict): params dict to convert names in
        idna_codec (dns.name.IDNACodec): Codec to use for the conversion
    """

    # Convert domain name to ACE string
    params["domain"] = dns.name.from_text(
        params["domain"], idna_codec=idna_codec
    ).to_text()

    # Create a dict where all keys (the nameserver names) are converted to ACE strings
    nameserver = {
        dns.name.from_text(key, idna_codec=idna_codec).to_text(): value
        for key, value in params.get("nameserver", {}).items()
    }

    # Replace the nameserver dict with the newly created dict
    params["nameserver"] = nameserver


def check_glue_records(params: Dict) -> List[NastIssue]:
    """Check if glue records are provided if necessary, as defined in
    Section 2.1.3.1 of the Nameserver Predelegation Check Documentation.

    Removes any provided glue records from the params dict if they are not required.

    Args:
        params (Dict): params dict to check glue records in

    Returns:
        List[NastIssue]: List of Issues found (may be empty)
    """
    new_issues = []

    domain = params["domain"]

    for nameserver, ip_list in params["nameserver"].items():
        in_zone = is_in_zone(nameserver, domain)
        if in_zone and not ip_list:
            issue_params = {"ns": nameserver}
            new_issues.append(
                NastIssue(
                    IssueCodes.MISSING_GLUE_RECORDS, IssueSeverity.ERROR, issue_params
                )
            )

        if not in_zone and ip_list:
            issue_params = {"ns": nameserver}
            new_issues.append(
                NastIssue(
                    IssueCodes.PROVIDED_GLUE_RECORDS_NOT_APPLICABLE,
                    IssueSeverity.WARNING,
                    issue_params,
                )
            )
            params["nameserver"][nameserver] = []

    return new_issues


def check_ip_addresses_of_glue_rr(params: Dict) -> List[NastIssue]:
    """Validates that the passed addresses for nameservers are valid IPv4 or IPv6 addresses.

    Args:
        params (Dict): params dict to validate

    Returns:
        List[NastIssue]: List of Issues found (may be empty).
    """

    issue_list = list()

    for nameserver in params["nameserver"]:
        for ip_address in params["nameserver"][nameserver]:
            try:
                ipaddress.ip_address(ip_address)
            except ValueError:
                issue_list.append(
                    NastIssue(
                        IssueCodes.INVALID_IP_ADDRESS,
                        IssueSeverity.ERROR,
                        {"nameserver": nameserver, "ip": ip_address},
                    )
                )
            except BaseException as exp:
                unexpected_excp_issue= NastIssue(
                        IssueCodes.UNEXPECTED_EXCEPTION,
                        IssueSeverity.ERROR,
                        {"nameserver": nameserver, "ip": ip_address},
                    )
                unexpected_excp_issue.log(INFO, exp, 0, "check_ip_addresses_of_glue_rr",
                                    {"params": {"nameserver": {nameserver:[ip_address]}}})
                issue_list.append(unexpected_excp_issue)
    return issue_list


def strip_dots(params: Dict) -> None:
    """Strips dots from domain and nameserver names in the passed 'params' dict.

    Args:
        params (Dict): params dict to convert names in
    """
    params["domain"] = params["domain"].strip(".")

    # Create a dict where all keys (the nameserver names) are stripped of trailing dots
    nameserver = {key.strip("."): value for key, value in params["nameserver"].items()}

    # Replace the nameserver dict with the newly created dict
    params["nameserver"] = nameserver


def check_referral_response_size(params: Dict) -> List[NastIssue]:
    """Validates that the referral response for the given params dict will not exceed 512 byte.

    Args:
        params (Dict): params dict to calculate referral response length in

    Returns:
        List[NastIssue]: List of Issues found (may be empty)
    """
    issue_list = list()
    # make sure domainname ends with dot
    domain = params["domain"].strip(".") + "."
    suffix = ("a" * 62 + ".") * 3
    qname = (suffix + domain)[-190:]
    if qname[0] == ".":
        qname = "a" + qname[-189:]

    # Build DNS message with query section
    query = dns.message.make_query(
        qname=qname, rdtype=dns.rdatatype.A, rdclass=dns.rdataclass.IN
    )
    answer = dns.message.make_response(query=query)
    answer.index = {}

    # add authority section
    answer.authority.append(
        dns.rrset.from_text(
            domain,
            86400,
            dns.rdataclass.IN,
            dns.rdatatype.NS,
            *params["nameserver"].keys()
        )
    )

    # add additional section with glue records if there are any
    for nameserver, glue_records in params["nameserver"].items():
        a_records = []
        aaaa_records = []

        for record in glue_records:
            try:
                ip_addr = ipaddress.ip_address(record)
                if ip_addr.version == 4:
                    a_records.append(record)
                else:
                    aaaa_records.append(record)

            except ValueError:
                # necessary capture due to possible invalid ip address param string,
                # pre-checked in check_ip_addresses_of_glue_rr()
                # We do not want to add the same issue again in this check!
                # ipaddress.ip_address() only raises ValueError,
                # so we should still handle other exceptions.
                pass
            except BaseException as err:
                unexpected_excp_issue =  NastIssue(
                        IssueCodes.UNEXPECTED_EXCEPTION,
                        IssueSeverity.ERROR,
                        parameter_dict={
                            "nameserver": nameserver,
                            "ip": record,
                            "error": err,
                        },
                    )
                unexpected_excp_issue.log(INFO, err, 0, "check_referral_response_size",
                                    {"params": {"nameserver": {nameserver:[record]}}})
                issue_list.append(unexpected_excp_issue)

        nameserver_host = dns.name.from_text(nameserver)

        if a_records:
            answer.additional.append(
                dns.rrset.from_text(
                    nameserver_host,
                    86400,
                    dns.rdataclass.IN,
                    dns.rdatatype.A,
                    *a_records
                )
            )
        if aaaa_records:
            answer.additional.append(
                dns.rrset.from_text(
                    nameserver_host,
                    86400,
                    dns.rdataclass.IN,
                    dns.rdatatype.AAAA,
                    *aaaa_records
                )
            )

    wire = answer.to_wire(origin=dns.name.from_text("."))

    if len(wire) > 512:
        issue_list.append(
            NastIssue(
                IssueCodes.CALCULATED_REFERRAL_RESPONSE_LARGER_THAN_ALLOWED,
                parameter_dict={
                    "length in octets": str(len(wire)),
                    "length allowed": "512",
                },
            )
        )

    return issue_list
