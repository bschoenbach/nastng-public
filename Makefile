CONTAINER=nast-server
PYLINT_FILES=`find api.py cli.py nast -name '*.py'`

all: build start

build:
	docker build -t ${CONTAINER}:latest .

start: nast-resolver nast-executor
	- @docker stop ${CONTAINER} > /dev/null 2>&1 || echo -n ""
	- @docker rm ${CONTAINER}
	docker run --ulimit nofile=10000:10000 -p 8816:8816 -e --rm -d \
		--net=nast-test-net --ip=172.31.0.3 --ip6=fd00:10:10::3 \
		-e NAST_HOSTNAME=172.31.0.3 \
		-e NAST_PORT=8816 \
		-e NAST_RESOLVER=172.31.0.2 \
		-e NAST_QUERY_EXECUTOR=http://nast-executor:8080/query \
		-e LOG_LEVEL=DEBUG \
		-e NAST_TESTMODE=1 \
		--name ${CONTAINER} ${CONTAINER}:latest

start_prod: nast-resolver-prod nast-executor-prod
	- @docker stop ${CONTAINER} > /dev/null 2>&1 || echo -n ""
	- @docker rm ${CONTAINER}
	docker run --ulimit nofile=10000:10000 -p 8816:8816 -e --rm -d \
		--net=nast-test-net --ip=172.31.0.3 --ip6=fd00:10:10::3 \
		-e NAST_HOSTNAME=172.31.0.3 \
		-e NAST_PORT=8816 \
		-e NAST_RESOLVER=172.31.0.2 \
		-e LOG_LEVEL=DEBUG \
		--name ${CONTAINER} ${CONTAINER}:latest

start_fg: nast-resolver
	- @docker stop ${CONTAINER} > /dev/null 2>&1 || echo -n ""
	- @docker rm ${CONTAINER}
	docker run --ulimit nofile=10000:10000 --rm -t -i \
	--net=nast-test-net --ip=172.31.0.3 --ip6=fd00:10:10::3 \
	-e NAST_HOSTNAME=172.31.0.3 \
	-e NAST_PORT=8816 \
	-e NAST_RESOLVER=172.31.0.2 \
	-e LOG_LEVEL=DEBUG \
	-e NAST_TESTMODE=1 \
	--name ${CONTAINER} ${CONTAINER}:latest

fg: start_fg

start_local:
	- @docker stop ${CONTAINER} > /dev/null 2>&1 || echo -n ""
	- @docker rm ${CONTAINER}
	NAST_HOSTNAME=127.0.0.1 NAST_PORT=8816 NAST_RESOLVER=172.31.0.2 \
	NAST_TESTMODE=1 ./api.py


stop:
	- docker stop ${CONTAINER}


exec:
	docker exec -t -i ${CONTAINER} /bin/sh


logs:
	docker logs -f ${CONTAINER}

clean:
	rm -rf .coverage htmlcov *.log nosetests_*.xml
	#find ./ -name "__pycache__" -exec rm -rf {} \;


update-iana:
	wget -O conf/ipv6-unicast-address-assignments.xml https://www.iana.org/assignments/ipv6-unicast-address-assignments/ipv6-unicast-address-assignments.xml
	wget -O conf/iana-ipv6-special-registry.xml https://www.iana.org/assignments/iana-ipv6-special-registry/iana-ipv6-special-registry.xml

nast-query-executor:
	docker build -t nast-executor:latest -f nastpp/Dockerfile ./nastpp

nast-testcontainer:
	docker build -t nast-testcontainer:latest -f tests/Dockerfile.tests .

nast-lastgenerator-build:
	docker build -t nast-lastgenerator:latest  -f tests/performance/lastgenerator/Dockerfile.lastgenerator .

nast-lastgenerator-start:
	- @docker stop nast-lasttest > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-lasttest > /dev/null 2>&1 || echo -n ""
	@docker run -d --rm -v `pwd`:/nast --name nast-lasttest -p 8001:8001 \
		--net=nast-test-net --ip=172.31.0.7 \
		nast-lastgenerator:latest

lasttests: nast-lastgenerator-build nast-lastgenerator-start

unittests:
	@docker run --rm -v `pwd`:/nast --name nast-test \
	    -e LOG_ENABLED=0 \
		-e NAST_TESTMODE=1 \
		nast-testcontainer:latest \
		nosetests-3 -s -v --with-xunit --xunit-file=nosetests_unittests.xml tests/unittests/*.py

functiontests:
	@docker run --rm -v `pwd`:/nast --name nast-test \
	    -e LOG_ENABLED=0 \
		-e NAST_TESTMODE=1 \
		--net=nast-test-net --ip=172.31.0.100 --ip6=fd00:10:10::100 \
		nast-testcontainer:latest \
		nosetests-3 -s -v --with-xunit --xunit-file=nosetests_functiontests.xml tests/functiontests/*.py

functiontests-logging:
	@docker run --rm -v `pwd`:/nast --name nast-test \
	    -e LOG_ENABLED=1 \
		-e LOG_FILE_PATH=nast.log \
		-e NAST_TESTMODE=1 \
		--net=nast-test-net --ip=172.31.0.100 --ip6=fd00:10:10::100 \
		nast-testcontainer:latest \
		nosetests-3 -s -v --with-xunit --xunit-file=nosetests_functiontests.xml tests/functiontests/logging/*.py && rm  -f nast.log

apitests:
	@docker run --rm -v `pwd`:/nast --name nast-test \
		--net=nast-test-net --ip=172.31.0.100 --ip6=fd00:10:10::100 \
		-e NAST_TESTMODE=1 \
		-e NAST_PORT=8816 \
		-e NAST_HOSTNAME=172.31.0.3 \
		nast-testcontainer:latest \
		nosetests-3 -s -v --with-xunit --xunit-file=nosetests_apitests.xml tests/apitests/*.py

test: build start nast-resolver nast-testcontainer unittests functiontests functiontests-logging apitests

coverage: nast-testcontainer nast-resolver
	@docker run --rm -v `pwd`:/nast --name nast-test \
		--net=nast-test-net --ip=172.31.0.100 --ip6=fd00:10:10::100 \
		-e LOG_ENABLED=1 \
		-e LOG_FILE_PATH=nast.log \
		-e NAST_TESTMODE=1 \
		-e NAST_PORT=8816 \
		-e NAST_HOSTNAME=172.31.0.3 \
		nast-testcontainer:latest \
		make coverage-in-container


coverage-in-container:
	- @coverage3 erase
	coverage-3 run -a --branch --source=nast \
		/usr/bin/nosetests-3 --with-xunit --xunit-file=nosetests_unittests.xml tests/unittests/*.py
	coverage-3 run -a --branch --source=nast \
		/usr/bin/nosetests-3 --with-xunit --xunit-file=nosetests_functiontests.xml tests/functiontests/*.py
	coverage-3 run -a --branch --source=nast \
		/usr/bin/nosetests-3 --with-xunit --xunit-file=nosetests_functiontests_logging.xml tests/functiontests/logging/*.py && rm  -f nast.log

	coverage3 run -a --source=api,nast --branch ./api.py >webserver.log 2>&1 &
	sleep 3
	/usr/bin/nosetests-3 -s -v --with-xunit --xunit-file=nosetests_apitests.xml tests/apitests/*.py

	- @pkill -f ".*python.*coverage.*api\.py" > /dev/null 2>&1 || echo -n ""
	@sleep 2
	@coverage-3 html
	@coverage-3 report
	@coverage-3 xml
	@echo "Results in HTML: see firefox htmlcov/index.html"



pylint: nast-testcontainer
	@docker run \
		--rm \
		-v `pwd`:/nast \
		--name nast-test nast-testcontainer:latest \
		python3 -m pylint \
			--rcfile=pylint.rc \
			-f parseable ${PYLINT_FILES} \
			> pylint.log || echo "INFO: pylint found issues"
	cat pylint.log
	@docker run \
		--rm \
		-v `pwd`:/nast \
		--name nast-test nast-testcontainer:latest \
		python3 -m pylint \
			--rcfile=pylint.rc \
			-E ${PYLINT_FILES}


shutdown:
	@echo "shutting down testenvironment..."
	- @docker stop nast-resolver > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-executor > /dev/null 2>&1 || echo -n ""
	- @docker stop ${CONTAINER} > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns1 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns1a > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns2 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns2a > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns3 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns4 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns5 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns6 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns7 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns8 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns9 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns10 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns11 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns12 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns13 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-ns14 > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-lasttest > /dev/null 2>&1 || echo -n ""
	- @docker stop nast-legacy > /dev/null 2>&1 || echo -n ""
	- @docker stop ipv6nat > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-resolver > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-executor > /dev/null 2>&1 || echo -n ""
	- @docker rm ${CONTAINER} > /dev/null 2>&1 || echo -n ""
	- @docker rm ipv6nat > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns1 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns1a > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns2 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns2a > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns3 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns4 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns5 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns6 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns7 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns8 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns9 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns10 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns11 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns12 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns13 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns14 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-lasttest > /dev/null 2>&1 || echo -n ""
	- @docker network rm nast-test-net > /dev/null 2>&1 || echo -n ""
	@echo "done"

network:
	docker run --rm -d --name ipv6nat --privileged --network host -v /var/run/docker.sock:/var/run/docker.sock:ro -v /lib/modules:/lib/modules:ro robbertkl/ipv6nat
	docker network create --driver bridge --ipv6 --subnet=172.31.0.0/16 --subnet=fd00:10:10::/48 --gateway=172.31.0.1 --gateway=fd00:10:10::1 nast-test-net

nast-executor:
	- @docker stop nast-executor > /dev/null 2>&1 || echo -n ""
	docker build -t nast-executor:latest -f nastpp/Dockerfile ./nastpp
	docker run --ulimit nofile=10000:10000 -p 8080:8080 --rm -d --net=nast-test-net \
		--ip=172.31.0.8 --ip6=fd00:10:10::8 \
		-e NAST_QDNS_SERVICE_PORT=8080 \
		-e NAST_QDNS_SERVICE_LOGLEVEL=INFO \
		-e NAST_QDNS_SERVICE_THREADS=150 \
		--name nast-executor nast-executor:latest

nast-executor-prod:
	- @docker stop nast-executor > /dev/null 2>&1 || echo -n ""
	docker build -t nast-executor:latest -f nastpp/Dockerfile ./nastpp
	docker run --ulimit nofile=10000:10000 -p 8080:8080 --rm -d --net=nast-test-net \
		--ip=172.31.0.8 --ip6=fd00:10:10::8 \
		-e NAST_QDNS_SERVICE_PORT=8080 \
		-e NAST_QDNS_SERVICE_LOGLEVEL=INFO \
		-e NAST_QDNS_SERVICE_THREADS=150 \
		--name nast-executor nast-executor:latest

nast-resolver:
	- @docker stop nast-resolver > /dev/null 2>&1 || echo -n ""
	docker build -t nast-resolver:latest resolver
	docker run --ulimit nofile=10000:10000 --rm -d --net=nast-test-net --ip=172.31.0.2 --ip6=fd00:10:10::2 -e TESTMODE=1 --name nast-resolver nast-resolver:latest

nast-resolver-prod:
	- @docker stop nast-resolver > /dev/null 2>&1 || echo -n ""
	docker build -t nast-resolver:latest resolver
	docker run --ulimit nofile=10000:10000 --rm -d --net=nast-test-net \
		--ip=172.31.0.2 --ip6=fd00:10:10::2 \
		--name nast-resolver nast-resolver:latest


nast-legacy:
	- @docker stop nast-legacy > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-legacy
	podman build -t nast-legacy:latest -f tests/old_nast/Dockerfile tests/old_nast
	docker run --rm -d \
		--net=nast-test-net --ip=172.31.0.4 --ip6=fd00:10:10::4 \
		-e RESOLVER=172.31.0.2 \
		--name nast-legacy nast-legacy:latest

testenv: shutdown network nast-resolver nast-executor nameserver

nameserver: sign-zones nast-ns nast-nsd ns1 ns1a ns2 ns2a ns3 ns4 ns5 ns6 ns7 \
	ns8 ns9 ns10 ns11 ns12 ns14


sign-zones:
	@echo "Signiere Zonen..."
	docker build -t nast-signer:latest -f tests/Dockerfile.signer tests
	mkdir -p tests/zones-signed
	docker run -v `pwd`/tests/zones:/zones -v `pwd`/tests/zones-signed:/zones-signed \
		-v `pwd`/tests/keys:/keys -e PRESIGN=1  --rm --name nast-signer nast-signer:latest

nast-ns:
	docker build -t nast-ns:latest -f tests/Dockerfile.nameserver tests

nast-nsd:
	docker build -t nast-nsd:latest -f tests/Dockerfile.nsd tests

ns1:
	- @docker stop nast-ns1 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns1 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns1 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.1.1 --ip6=fd00:10:10::1:1 --name nast-ns1 nast-ns:latest

ns1a:
	- @docker stop nast-ns1a > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns1a > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns1a -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.1.2 --ip6=fd00:10:10::1:2 --name nast-ns1a nast-ns:latest

ns2:
	- @docker stop nast-ns2 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns2 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns2 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.2.2 --ip6=fd00:10:10::2:2 --name nast-ns2 nast-ns:latest

ns2a:
	- @docker stop nast-ns2a > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns2a > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns2a -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.2.3 --ip6=fd00:10:10::2:3 --name nast-ns2a nast-ns:latest

ns3:
	- @docker stop nast-ns3 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns3 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns3 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.3.3 --ip6=fd00:10:10::3:3 --name nast-ns3 nast-ns:latest

ns4:
	- @docker stop nast-ns4 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns4 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns4 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.4.4 --ip6=fd00:10:10::4:4 --name nast-ns4 nast-ns:latest

ns5:
	- @docker stop nast-ns5 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns5 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns5 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.5.5 --ip6=fd00:10:10::5:5 --name nast-ns5 nast-ns:latest

ns6:
	- @docker stop nast-ns6 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns6 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns6 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.6.6 --ip6=fd00:10:10::6:6 --name nast-ns6 nast-ns:latest

ns7:
	- @docker stop nast-ns7 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns7 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns7 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.7.7 --ip6=fd00:10:10::7:7 --name nast-ns7 nast-ns:latest

ns8:
	- @docker stop nast-ns8 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns8 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns8 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.8.8 --ip6=fd00:10:10::8:8 --name nast-ns8 nast-ns:latest

ns9:
	- @docker stop nast-ns9 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns9 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns9 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.9.9 --ip6=fd00:10:10::9:9 --name nast-ns9 nast-ns:latest

ns10:
	- @docker stop nast-ns10 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns10 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns10 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.10.10 --ip6=fd00:10:10::10:10 --name nast-ns10 nast-ns:latest

ns11:
	- @docker stop nast-ns11 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns11 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns11 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.11.11 --ip6=fd00:10:10::11:11 --name nast-ns11 nast-ns:latest

ns12:
	- @docker stop nast-ns12 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns12 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns12 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.12.12 --ip6=fd00:10:10::12:12 --name nast-ns12 nast-ns:latest

ns14:
	- @docker stop nast-ns14 > /dev/null 2>&1 || echo -n ""
	- @docker rm nast-ns14 > /dev/null 2>&1 || echo -n ""
	docker run -e NS=ns14 -d --cap-add=NET_ADMIN --net=nast-test-net --ip=172.31.14.14 --ip6=fd00:10:10::14:14 --name nast-ns14 nast-nsd:latest
