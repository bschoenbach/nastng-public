#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""!NAST CommandLine Client"""

from argparse import ArgumentParser
import json
import requests


def get_args(args=None):
    parser = ArgumentParser(description='NAST CommandLine Client')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-i', type=str, dest='inputfile',
                       help='file with json data',
                       )
    group.add_argument('-r', type=str, dest='resolve',
                       help='domain name',
                       )

    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('-u', type=str, dest='uri',
                       help='NAST URI',
                       default="http://172.31.0.4:8816/nast"
                       )
    group.add_argument('--legacy', action='store_true',
                       help='Uses the legacy NAST in docker testenvironment (http://172.31.0.4:8816/nast)',
                       )
    group.add_argument('--ng', action='store_true',
                       help='Uses the new NAST in docker testenvironment (http://172.31.0.3:8816/nast)',
                       )
    group.add_argument('--local', action='store_true',
                       help='Uses NAST on localhost (http://127.0.0.1:8816/nast)',
                       )

    parser.add_argument('--v2', action='store_true',
                        help='use the version 2 API',
                        )
    parser.add_argument('--debug', action='store_true',
                        help='Enables debug mode in nastng',
                        )
    parser.add_argument('-f', type=str, dest='format', required=False,
                        choices=['xml', 'json'],
                        help='format: xml, json',
                        default="json"
                        )
    parser.add_argument('-v', action='store_true', dest='verbose', required=False,
                        help='shows debug information',
                        default=False
                        )

    args = parser.parse_args(args)
    if args.ng:
        args.uri = "http://172.31.0.3:8816/nast"
    if args.legacy:
        args.uri = "http://172.31.0.4:8816/nast"
    if args.local:
        args.uri = "http://127.0.0.1:8816/nast"

    return args


def load_params_from_json_file(filename):
    with open(filename) as handle:
        return json.load(handle)


def make_query(uri, result_format, params, debug=None, api_version=1):
    query = "%s/%s/%s" % (uri, result_format,
                          requests.utils.quote(params['domain']))
    if api_version == 2:
        query = "%s/%s" % (uri.replace("/nast", "/v2/check"),
                           requests.utils.quote(params['domain']))

    urlparams = []
    if "nameserver" in params:
        nsno = 1
        for nameserver in params["nameserver"]:
            if params["nameserver"][nameserver]:
                nameserver += "," + ",".join(params["nameserver"][nameserver])
            if nsno <= 2:
                urlparams.append("ns%d=%s" % (nsno, requests.utils.quote(nameserver)))
            else:
                urlparams.append("nsX=%s" % (requests.utils.quote(nameserver),))

            nsno += 1
    if "policy" in params and api_version == 1:
        urlparams.append("policy=%s" % (requests.utils.quote(params['policy'])))
    if "dnskey" in params:
        for dnskey in params["dnskey"]:
            key = "%d %s %d %s" % (dnskey["flags"], str(dnskey["protocol"]), dnskey["algorithm"], dnskey["dnskey"])
            urlparams.append("dnskey=%s" % (requests.utils.quote(key)))
    if debug:
        urlparams.append("debug=True")

    if urlparams:
        query += "?" + "&".join(urlparams)
    return query


def do_check(args):
    params = load_params_from_json_file(args.inputfile)
    api_version = 1
    if args.v2:
        api_version = 2
    query = make_query(args.uri, args.format, params, args.debug, api_version)
    print("Query: %s\n" % (query))

    result = requests.get(query,)
    if args.verbose:
        print("Status-Code: %d" % (result.status_code))
        print("Headers: " + str(result.headers))
    if "content-type" in result.headers:
        if "json" in result.headers['content-type']:
            print(json.dumps(result.json(), indent=4, sort_keys=True))
            return
    print(str(result.text))


def do_resolve(args):
    uri = args.uri.strip("/").rstrip("/nast")
    query = "%s/v2/resolve/%s" % (uri, requests.utils.quote(args.resolve))
    print("Query: %s\n" % (query))
    result = requests.get(query,)
    if args.verbose:
        print("Status-Code: %d" % (result.status_code))
        print("Headers: " + str(result.headers))
    if "content-type" in result.headers:
        if "json" in result.headers['content-type']:
            print(json.dumps(result.json(), indent=4, sort_keys=True))
            return
    print(str(result.text))


def main():
    args = get_args()
    if args.inputfile:
        do_check(args)
    if args.resolve:
        do_resolve(args)


if __name__ == "__main__":
    main()
