#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from distutils.core import setup

setup(
    name="nast",
    version="trunk",
    description="NAST",
    author="DNS Team",
    author_email="team-dns@denic.de",
    license="BSD-like",
    packages=["nast"],
    scripts=[],
)
