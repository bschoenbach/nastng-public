#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from time import perf_counter
from nast.resolver import Resolver
from nast.queries import Queries, QueriesExecutor

def main():
    nameserver = {"ns1.denic.nast": [
        "172.31.1.1",
        "fd00:10:10::1:1"
    ],
        "ns2.denic.nast": [
            "172.31.2.2",
            "fd00:10:10::2:2"
    ],
        "ns3.denic.nast": [
            "172.31.3.3",
            "fd00:10:10::3:3"
    ],
        "ns4.denic.net": ["172.31.4.4", "fd00:10:10::4:4"]
    }

    t1_start = perf_counter()
    i = 0
    while i < 1000:
        i += 1
        Resolver('172.31.0.2','http://172.31.0.8:8080/query').run(nameserver)
        #Queries().run(nameserver, "denic.nast")
        QueriesExecutor("http://172.31.0.8:8080/query").run(nameserver, "denic.nast")

    t1_stop = perf_counter()
    runtime = t1_stop - t1_start
    qps = i / runtime
    print("runtime: %0.3f, qps: %d, rtt:" % (runtime, qps))


if __name__ == "__main__":
    main()