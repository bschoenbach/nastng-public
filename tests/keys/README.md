Keys werden so erstellt:

    dnssec-keygen  -a RSASHA256 -b 2048 -f KSK -G ZONENAME
    dnssec-keygen  -a RSASHA256 -b 1024 -G ZONENAME
    
    dnssec-dsfromkey FILENAME.key