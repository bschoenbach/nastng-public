import unittest
from nast.config import Config
from cli import make_query
import json
import requests
import xmltodict


class ApiTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None
    
    def test_get_homepage(self):
        result = requests.get("http://%s:%d/" % (self.config.hostname,
                                                 self.config.port))
        self.assertEqual(200, result.status_code)
        self.assertEqual("text/plain; charset=utf-8", result.headers['Content-Type'])
        self.assertEqual("", result.text)
    
    def test_get_alive(self):
        result = requests.get("http://%s:%d/alive" % (self.config.hostname,
                                                      self.config.port))
        self.assertEqual(200, result.status_code)
        self.assertEqual("text/plain; charset=utf-8", result.headers['Content-Type'])
        self.assertEqual("alive", result.text)
    
    def test_get_ready(self):
        result = requests.get("http://%s:%d/ready" % (self.config.hostname,
                                                      self.config.port))
        self.assertEqual(200, result.status_code)
        self.assertEqual("text/plain; charset=utf-8", result.headers['Content-Type'])
        self.assertEqual("ready", result.text)
    
    def test_get_metrics(self):
        result = requests.get("http://%s:%d/metrics" % (self.config.hostname,
                                                        self.config.port))
        self.assertEqual(200, result.status_code)
        self.assertEqual("text/plain; charset=utf-8", result.headers['Content-Type'])
        self.assertIn("# TYPE nast_dns_queries_total counter", result.text)
    
    def test_v1_invalid_domainname(self):
        params = {'domain': '   .denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "json", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(400, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/problem+json", result.headers['content-type'])
    
    def test_v1_json_success(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "json", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        self.assertEqual({'success': True}, data)
    
    def test_v1_json_success_mixed_ipv6(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10:0:0:0:1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10:0::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "json", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        self.assertEqual({'success': True}, data)
    
    def test_v1_json_rri_glue_wrong_nameserver(self):
        params = {'domain': 'xn---test-glues-ncb87aud4a26f.de',
                  'nameserver': {'dns1.xn---test-glues-ncb87aud4a26f.de.': ['172.31.1.1', 'fd00:10:10:0:0:0:1:1'],
                                 'ns2.nic.nast': []
                                 }}
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "json", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        self.assertEqual({'success': True}, data)
    
    def test_v1_xml_success(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "xml", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/xml;charset=UTF-8", result.headers['content-type'])

        data = result.text.strip()
        self.assertEqual('<?xml-stylesheet type="text/xsl" href="../meta/nast_answer.xsl"?>\n'
                         '<predelegation-check success="true" xsi:schemaLocation="../meta/nast_answer.xsd" xmlns="http://schema.denic.de/rng/nast" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>', data)

        doc = xmltodict.parse(data)
        self.assertEqual("true", doc['predelegation-check']['@success'])
    
    def test_v1_json_lots_of_issues(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.10.10', 'fd00:10:10::10:10'],
                                 'ns3.denic.nast': ['172.31.7.7', 'fd00:10:10::7:7'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257,  'protocol': 3,  'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "json", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])
        data = result.json()

        # print(json.dumps(data, indent=4))

        with open("tests/testdata/test_json_lots_of_issues.json", mode="r", encoding="utf-8") as fh:
            expected = json.load(fh)
            self.assertEqual(expected, data)
    
    def test_v1_xml_lots_of_issues(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.10.10', 'fd00:10:10::10:10'],
                                 'ns3.denic.nast': ['172.31.7.7', 'fd00:10:10::7:7'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257,  'protocol': 3,  'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "xml", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/xml;charset=UTF-8", result.headers['content-type'])

        data = result.text.strip()
        # print(data)

        with open("tests/testdata/test_xml_lots_of_issues.xml", mode="r", encoding="utf-8") as fh:
            expected = fh.read()
            self.assertEqual(expected.strip(), data.strip())

        doc = xmltodict.parse(data)
        # print(str(doc))
        self.assertEqual("false", doc['predelegation-check']['@success'])
    
    def test_v2_check_success(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257,  'protocol': 3,  'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/v2/check" % (self.config.hostname,
                                                      self.config.port),
                           "json", params, None, 2)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        self.assertEqual({'success': True}, data)
    
    def test_v2_check_invalid_param_protocol_in_dnskey(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 256,  'protocol': "s",  'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/v2/check" % (self.config.hostname,
                                                      self.config.port),
                           "json", params, None, 2)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(400, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/problem+json", result.headers['content-type'])
    
    def test_v2_check_failed(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.10.10', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257,  'protocol': 3,  'algorithm': 8,
                              'dnskey': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        query = make_query("http://%s:%d/v2/check" % (self.config.hostname,
                                                      self.config.port),
                           "json", params, None, 2)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        expected = {'issues': [
            {'arguments': {'ip': '172.31.10.10',
                           'nameserver': 'ns3.denic.nast',
                           'protocol': 'udp'},
             'code': 902,
             'message': 'Timeout',
             'severity': 'error'},
            {'arguments': {'ip': '172.31.10.10',
                           'nameserver': 'ns3.denic.nast',
                           'protocol': 'tcp'},
             'code': 902,
             'message': 'Timeout',
             'severity': 'warning'},
            {'arguments': {'determined_ips': "['172.31.3.3', 'fd00:10:10::3:3']",
                           'ns': 'ns3.denic.nast',
                           'provided_ips': "['172.31.10.10', 'fd00:10:10::3:3']"},
             'code': 106,
             'message': 'Inconsistent set of nameserver IP addresses',
             'severity': 'error'}],
            'success': False}
        self.assertEqual(expected, data)
    
    def test_v2_resolve_success(self):
        result = requests.get("http://%s:%d/v2/resolve/denic.nast" % (self.config.hostname,
                                                                      self.config.port))
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        expected = {'dnskey': [{'algorithm': 8, 'flags': 257,
                                'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31joSTHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do+cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxaaeNLcWnA8pU=',
                                'protocol': 3}],
                    'domainname': 'denic.nast',
                    'nameserver': {'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                                   'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                                   'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:3'],
                                   'ns4.denic.net.': []}}

        self.assertEqual(expected, data)
    
    def test_v2_resolve_failed(self):
        result = requests.get("http://%s:%d/v2/resolve/nonexistent.nast" % (self.config.hostname,
                                                                            self.config.port))
        self.assertEqual(404, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        self.assertEqual({}, data)
    
    def test_v1_aggregate_issues_json(self):
        # soa-values-to-high.nast
        params = {'domain': 'soa-values-to-high.nast',
                  'nameserver': {'ns1.nic.nast': [],
                                 'ns2.nic.nast': []},
                  }
        query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                  self.config.port),
                           "json", params)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        self.assertEqual(4, len(data['issues']))
        # print(json.dumps(data, indent=4, sort_keys=True))
    
    def test_v2_aggregate_issues_json(self):
        # soa-values-to-high.nast
        params = {'domain': 'soa-values-to-high.nast',
                  'nameserver': {'ns1.nic.nast': [],
                                 'ns2.nic.nast': []},
                  }
        query = make_query("http://%s:%d/v2/check" % (self.config.hostname,
                                                      self.config.port),
                           "json", params, None, 2)
        # print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/json", result.headers['content-type'])

        data = result.json()
        self.assertEqual(4, len(data['issues']))
        # print(json.dumps(data, indent=4, sort_keys=True))
#

    def test_v2_domainname_longerthan_63_char_withumlaut_resolve(self):
        params = {'domain': '12345678901234567890123456789012345678901234567890123456ä.de',
            }
        query = make_query("http://%s:%d/v2/resolve" % (self.config.hostname,
                                                self.config.port),
                    "json", params, None, 2)
        
        #print("Query: %s" % (query))
        result = requests.get(query)
        self.assertEqual(400, result.status_code)
        data = result.json()
        self.assertEqual('A DNS label is > 63 octets long.', data['detail'])
 
    def test_v2_domainname_longerthan_63_char_without_umlaut_resolve(self):
            params = {'domain': '1234567890123456789012345678901234567890123456789012345600000000.de',
                    }
            query = make_query("http://%s:%d/v2/resolve" % (self.config.hostname,
                                                        self.config.port),
                            "json", params, None, 2)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            #print(result)
            self.assertEqual(400, result.status_code)
            data = result.json()
            self.assertEqual('A DNS label is > 63 octets long.', data['detail'])
            
    def test_v2_domainname_longerthan_63_char_withumlaut_check(self):
            # soa-values-to-high.nast
            params = {'domain': '12345678901234567890123456789012345678901234567890123456ä.de',
                      'nameserver': {'ns1.nic.nast': [],
                                    'ns2.nic.nast': []},
                    }
            query = make_query("http://%s:%d/v2/check" % (self.config.hostname,
                                                        self.config.port),
                            "json", params, None, 2)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            #print(result)
            self.assertEqual(400, result.status_code)
            data = result.json()
            self.assertEqual('A DNS label is > 63 octets long.', data['detail'])

    def test_v2_domainname_longerthan_63_char_without_umlaut_check(self):
            # soa-values-to-high.nast
            params = {'domain': '1234567890123456789012345678901234567890123456789012345600000000.de',
                      'nameserver': {'ns1.nic.nast': [],
                                    'ns2.nic.nast': []},
                    }
            query = make_query("http://%s:%d/v2/check" % (self.config.hostname,
                                                        self.config.port),
                            "json", params, None, 2)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            self.assertEqual(400, result.status_code)
            data = result.json()
            self.assertEqual('A DNS label is > 63 octets long.', data['detail'])

    def test_v1_domainname_longerthan_63_char_withumlaut(self):
            params = {'domain': '12345678901234567890123456789012345678901234567890123456ä.de',
                      'nameserver': {'ns1.nic.nast': [],
                                    'ns2.nic.nast': []},
                    }
            query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                        self.config.port),
                            "json", params)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            self.assertEqual(400, result.status_code)
            data = result.json()
            self.assertEqual('A DNS label is > 63 octets long.', data['detail'])
            

    def test_v1_domainname_longerthan_63_char_without_umlaut(self):
            params = {'domain': '1234567890123456789012345678901234567890123456789012345600000000.de',
                      'nameserver': {'ns1.nic.nast': [],
                                    'ns2.nic.nast': []},
                    }
            query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                        self.config.port),
                            "json", params)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            self.assertEqual(400, result.status_code)
            data = result.json()
            self.assertEqual('A DNS label is > 63 octets long.', data['detail'])

    def test_v2_domainname_EQorLessthan_63_char_resolve_success(self):
            params = {'domain': 'denic.nast',
                    }
            query = make_query("http://%s:%d/v2/resolve" % (self.config.hostname,
                                                        self.config.port),
                            "json", params, None, 2)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            self.assertEqual(200, result.status_code) 
            #print(result.json())           

    def test_v2_domainname_EQorLessthan_63_char_check_success(self):
            params = {'domain': '123456789012345678901234567890123456789012345678901234560000000.de',
                      'nameserver': {'ns1.nic.nast': [],
                                    'ns2.nic.nast': []},
                    }
            query = make_query("http://%s:%d/v2/check" % (self.config.hostname,
                                                        self.config.port),
                            "json", params, None, 2)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            #print(result)
            self.assertEqual(200, result.status_code)
            #print(result.json())

    def test_v1_domainname_EQorLessthan_63_char_success(self):
            params = {'domain': '123456789012345678901234567890123456789012345678901234560000000.de',
                      'nameserver': {'ns1.nic.nast': [],
                                    'ns2.nic.nast': []},
                    }
            query = make_query("http://%s:%d/nast" % (self.config.hostname,
                                                        self.config.port),
                            "json", params)
            
            #print("Query: %s" % (query))
            result = requests.get(query)
            #print(result)
            self.assertEqual(200, result.status_code)
            #print(result.json())
            

