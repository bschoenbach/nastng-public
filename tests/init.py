#!/usr/bin/python3
import os
import os.path
import sys
import logging
import json
import subprocess
from shutil import copyfile


def setup_logging(name, log_level_str="DEBUG"):
    log_level = logging.DEBUG
    # setup logging
    logger = logging.getLogger()
    logger.setLevel(log_level)
    log_handler = logging.StreamHandler(sys.stdout)
    log_handler.setLevel(log_level)

    # create log-formatter
    formatter = logging.Formatter('%(asctime)s ' + name + '[%(process)d]: %(levelname)s - %(message)s')
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)


def check_env():
    if "NS" in os.environ:
        logging.info("NS=%s", os.environ["NS"])
    elif "PRESIGN" in os.environ:
        logging.info("presigning zones")
    else:
        logging.critical("NS-Variable nicht gesetzt!")
        exit(1)


def load_config(filename):
    logging.debug("Loading zone config from %s", filename)
    if os.path.isfile(filename):
        with open(filename) as fd:
            return json.load(fd)
    else:
        logging.critical("File does not exist or is not readable! [%s]", filename)
        exit(1)


def create_zones(ns, config):
    with open("/etc/bind/zones.conf", "w") as zones_fd:
        for zone in config:
            if ns in zone["ns"]:
                zones_fd.write("zone \"%s\" IN {\n" % (zone["zone"]))
                zones_fd.write("    type master;\n")
                zones_fd.write("    file \"/etc/bind/master/db.%s\";\n" % (zone["zone"]))
                zones_fd.write("};\n")
                ns_conf = zone["ns"][ns]
                copyfile("/" + ns_conf["file"], "/etc/bind/master/db.%s" % (zone["zone"]))
        pass


def presign_zones(config):
    for zone in config:
        if "dnssec" in zone:
            for dnssec in zone["dnssec"]:
                if isinstance(dnssec["source"], dict):
                    for nameserver, source in dnssec["source"].items():
                        logging.info("Signiere %s => %s", source, dnssec["target"][nameserver])
                        cmd = ["dnssec-signzone",
                               "-f",
                               dnssec["target"][nameserver],
                               "-x",
                               "-o",
                               zone["zone"],
                               ]
                        if "additional_params" in dnssec:
                            cmd.extend(dnssec["additional_params"])
                        if "nsec3" in dnssec:
                            cmd.extend(["-A",
                                        "-3", dnssec["nsec3"]["salt"],
                                        "-H", str(dnssec["nsec3"]["iterations"])])
                        cmd.append(source)
                        cmd.extend(dnssec["keys"][nameserver])
                        logging.debug(" ".join(cmd))
                        subprocess.check_call(cmd)
                else:
                    logging.info("Signiere %s => %s", dnssec["source"], dnssec["target"])
                    cmd = ["dnssec-signzone",
                           "-f",
                           dnssec["target"],
                           "-x",
                           "-o",
                           zone["zone"],
                           ]
                    if "additional_params" in dnssec:
                        cmd.extend(dnssec["additional_params"])
                    if "nsec3" in dnssec:
                        cmd.extend(["-A",
                                    "-3", dnssec["nsec3"]["salt"],
                                    "-H", str(dnssec["nsec3"]["iterations"])])
                    cmd.append(dnssec["source"])
                    cmd.extend(dnssec["keys"])
                    logging.debug(" ".join(cmd))
                    subprocess.check_call(cmd)


def install_config(ns):
    iptables = "/conf/iptables/%s" % (ns)
    if os.path.isfile(iptables):
        logging.info("installing iptables rules [%s]", iptables)
        subprocess.check_call(["/bin/sh", iptables])

    named_conf = "/conf/named/%s-named.conf" % (ns)
    if os.path.isfile(named_conf):
        logging.info("installing special named.conf [%s]", named_conf)
        copyfile(named_conf, "/etc/bind/named.conf")

    nsd_conf = "/conf/nsd/%s-nsd.conf" % (ns)
    if os.path.isfile(nsd_conf):
        logging.info("installing special nsd.conf [%s]", nsd_conf)
        copyfile(nsd_conf, "/etc/nsd/nsd.conf")

#########################################################################################################
# Main
#########################################################################################################


if __name__ == "__main__":
    setup_logging("init.py")
    check_env()
    if "PRESIGN" in os.environ:
        presign_zones(load_config("/root/zones.json"))
    else:
        create_zones(os.environ["NS"], load_config("/root/zones.json"))
        install_config(os.environ["NS"])
