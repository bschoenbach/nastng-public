import unittest
from nast.config import Config
from nast.metrics import METRICS, get_metrics
from nast.api_handler_v2 import prepare_answer_v2
from nast.issues import IssueCodes, IssueSeverity
import dns
import dns.exception
from nast import controller
from nast.issues import NastIssue, IssueCodes, IssueSeverity
from pprint import pprint


class MetricsTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.config.timeout = 0.5
        cls.maxDiff = None

    def test_query_metrics_no_fail(self):
        params = {
                'domain': 'denic.nast',
                'nameserver': {
                    'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                    'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                    'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:3'],
                    'ns4.denic.net.': []
                },
                'dnskey': []
        }
        METRICS.init_metrics()
        sample_before = METRICS.get_sample_of_dns_queries_counter({'status':'FAIL'})
        issue_list, nameserver_resolved, query_results = controller.pre_delegation_check(params)
        METRICS.update_metrics('v2', issue_list, query_results, nameserver_resolved, params['domain'])
        sample_after = METRICS.get_sample_of_dns_queries_counter({'status':'FAIL'})
        self.assertEqual(0, sample_after - sample_before)
        
    def test_query_metrics_AAAA_A_fail(self):
        params = {
                'domain': 'denic.nast',
                'nameserver': {
                    'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                    'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                    'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:3'],
                    'ns2.dropping.nast': []
                },
                'dnskey': []
        }
        METRICS.init_metrics()
        sample_before = METRICS.get_sample_of_dns_queries_counter({'status':'FAIL'})
        issue_list, nameserver_resolved, query_results = controller.pre_delegation_check(params)
        METRICS.update_metrics('v2', issue_list, query_results, nameserver_resolved, params['domain'])
        sample_after = METRICS.get_sample_of_dns_queries_counter({'status':'FAIL'})
        self.assertEqual(2, sample_after - sample_before)

    def issue_metrics_no_issues(self):
        params = {
                    'domain': 'denic.nast',
                    'nameserver': {
                        'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                        'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                        'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:3'],
                        'ns4.denic.net.': []
                    },
                    'dnskey': []
            }
        METRICS.init_metrics()
        sample_before = METRICS.get_sample_of_issues_counter({})
        issue_list, nameserver_resolved, query_results = controller.pre_delegation_check(params)
        METRICS.update_metrics('v2', issue_list, query_results, nameserver_resolved, params['domain'])
        sample_after = METRICS.get_sample_of_issues_counter({})
        self.assertEqual(0, sample_after - sample_before)

    def test_issue_metrics_warnings_dnssec(self):
        params = {
                    'domain': 'denic.nast',
                    'nameserver': {
                        'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                        'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                        'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:3'],
                        'ns4.denic.net.': []
                    },
                    'dnskey': [
                        {'flags': 256, 'protocol': 3, 'algorithm': 8, 'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='}
                    ]
                }
        METRICS.init_metrics()
        sample_before = METRICS.get_sample_of_issues_counter({'severity':[IssueSeverity.WARNING]})
        issue_list, nameserver_resolved, query_results = controller.pre_delegation_check(params)
        METRICS.update_metrics('v2', issue_list, query_results, nameserver_resolved, params['domain'])
        sample_after = METRICS.get_sample_of_issues_counter({'severity':[IssueSeverity.WARNING]})
        self.assertEqual(2, sample_after - sample_before)

    def test_network_metrics_no_errors(self):
        params = {
                    'domain': 'denic.nast',
                    'nameserver': {
                        'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                        'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                        'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:3'],
                        'ns4.denic.net.': []
                    },
                    'dnskey': []
            }
        METRICS.init_metrics()
        sample_before = METRICS.get_sample_of_network_counter({})
        issue_list, nameserver_resolved, query_results = controller.pre_delegation_check(params)
        METRICS.update_metrics('v2', issue_list, query_results, nameserver_resolved, params['domain'])
        sample_after = METRICS.get_sample_of_network_counter({})
        self.assertEqual(0, sample_after - sample_before)

    def test_issue_metrics_ipv6_errors(self):
        params = {
                    'domain': 'denic.nast',
                    'nameserver': {
                        'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                        'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                        'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:47'],
                        'ns4.denic.net.': []
                    },
                    'dnskey': []
            }
        METRICS.init_metrics()
        sample_before = METRICS.get_sample_of_network_counter({'code': [IssueCodes.TIMEOUT], 'ipv': ['6']})
        issue_list, nameserver_resolved, query_results = controller.pre_delegation_check(params)
        METRICS.update_metrics('v2', issue_list, query_results, nameserver_resolved, params['domain'])
        sample_after = METRICS.get_sample_of_network_counter({'code': [IssueCodes.TIMEOUT], 'ipv': ['6']})
        self.assertEqual(2, sample_after - sample_before)



    
