import unittest
import dns.exception
from nast.config import Config
from nast.stage1 import (check_resolver_result,
                         complete_nameserver_entries,
                         resolve_and_check_ip_addresses)
from nast.issues import NastIssue, IssueCodes, IssueSeverity


class Stage1Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.config.timeout = 0.5
        cls.maxDiff = None

    def test_complete_nameserver_entries(self):
        nameserver_dict = {'ns1.nic.nast': [],
                           'ns2.domain.nast': ['123.123.123.123']}
        ns_dict = {'ns1.nic.nast': {'A': ['1.2.3.4', '5.6.7.8'], 'AAAA': ['2001:abcd::53', '2001:1234::53']}}
        complete_nameserver_entries(nameserver_dict, ns_dict)
        self.assertEqual({'ns1.nic.nast': ['1.2.3.4', '5.6.7.8', '2001:abcd::53', '2001:1234::53'],
                          'ns2.domain.nast': ['123.123.123.123']},
                         nameserver_dict)

    def test_complete_nameserver_entries_but_ns_dict_is_empty(self):
        nameserver_dict = {'ns1.nic.nast': [],
                           'ns2.domain.nast': ['123.123.123.123']}
        ns_dict = {}
        complete_nameserver_entries(nameserver_dict, ns_dict)
        self.assertEqual({'ns1.nic.nast': [],
                          'ns2.domain.nast': ['123.123.123.123']},
                         nameserver_dict)

    def test_complete_nameserver_entries_adds_only_A(self):
        nameserver_dict = {'ns1.nic.nast': [],
                           'ns2.domain.nast': ['123.123.123.123']}
        ns_dict = {'ns1.nic.nast': {'A': ['1.2.3.4', '5.6.7.8'], 'AAAA': []}}
        complete_nameserver_entries(nameserver_dict, ns_dict)
        self.assertEqual({'ns1.nic.nast': ['1.2.3.4', '5.6.7.8'],
                          'ns2.domain.nast': ['123.123.123.123']},
                         nameserver_dict)

    def test_complete_nameserver_entries_adds_only_AAAA(self):
        nameserver_dict = {'ns1.nic.nast': [],
                           'ns2.domain.nast': ['123.123.123.123']}
        ns_dict = {'ns1.nic.nast': {'A': [], 'AAAA': ['2001:abcd::53', '2001:1234::53']}}
        complete_nameserver_entries(nameserver_dict, ns_dict)
        self.assertEqual({'ns1.nic.nast': ['2001:abcd::53', '2001:1234::53'],
                          'ns2.domain.nast': ['123.123.123.123']},
                         nameserver_dict)

    def test_resolve_and_check_ip_addresses_nothing_to_resolve(self):
        # we only have nameservers with glue records
        params = {'domain': 'test.nast',
                  'nameserver': {'ns1.test.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns2.test.nast': ['172.31.4.4', 'fd00:10:10::4:4']
                                 }}
        issue_list = resolve_and_check_ip_addresses(self.config, params)
        self.assertEqual([], issue_list)

    def test_resolve_and_check_ip_addresses_2ns_1glue(self):
        params = {'domain': 'test.nast',
                  'nameserver': {'ns1.test.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns3.nic.nast': [],
                                 'ns4.nic.nast': []
                                 }}
        issue_list = resolve_and_check_ip_addresses(self.config, params)
        self.assertEqual([], issue_list)
        self.assertEqual(sorted(['172.31.1.1', 'fd00:10:10::1:1']), sorted(params['nameserver']['ns1.test.nast']))
        self.assertEqual(sorted(['172.31.3.3', 'fd00:10:10::3:3']), sorted(params['nameserver']['ns3.nic.nast']))
        self.assertEqual(sorted(['172.31.4.4', 'fd00:10:10::4:4']), sorted(params['nameserver']['ns4.nic.nast']))

    def test_resolve_and_check_ip_addresses_unresolvable(self):
        params = {'domain': 'test.nast',
                  'nameserver': {'ns1.nic.nast': [],
                                 'nonexisting-123.nic.nast': []
                                 }}
        issue_list = resolve_and_check_ip_addresses(self.config, params)
        self.assertEqual(1, len(issue_list))
        self.assertEqual([NastIssue(IssueCodes.COULD_NOT_RESOLVE_ANY_IP_ADDRESS)], issue_list)

    def test_resolve_and_check_ip_addresses_unresolvable2(self):
        params = {'domain': 'test.nast',
                  'nameserver': {'ns1.nic.nast': [],
                                 'nonexisting-456.nonexisting.nast': []
                                 }}
        issue_list = resolve_and_check_ip_addresses(self.config, params)
        self.assertEqual(1, len(issue_list))
        self.assertEqual([NastIssue(IssueCodes.COULD_NOT_RESOLVE_ANY_IP_ADDRESS)], issue_list)

    def test_resolve_and_check_ip_addresses_timeout(self):
        params = {'domain': 'test.nast',
                  'nameserver': {'ns1.nic.nast': [],
                                 'nonexisting-789.dropping.nast': []
                                 }}
        issue_list = resolve_and_check_ip_addresses(self.config, params)
        # print(str(issue_list))
        # we get race conditions with resolver at this point. When resolver
        # is startet fresh, we run into timeout, but if this test is restarted
        # we might get an answer from resolver, indicating a SRVFAIL
        self.assertEqual(2, len(issue_list))
        self.assertEqual([NastIssue(IssueCodes.TIMEOUT_WITH_LOCAL_RESOLVER),
                          NastIssue(IssueCodes.TIMEOUT_WITH_LOCAL_RESOLVER)], issue_list)

    def test_check_resolver_result(self):
        ns_dict = {'ns1.nic.nast': {'A': ['172.31.1.1'], 'AAAA': ['fd00:10:10::1:1']},
                   'nonexisting.nic.nast': {'A': [], 'AAAA': []},
                   'ns8.nic.nast': {'A': dns.exception.Timeout(), 'AAAA': dns.exception.Timeout()},
                   'ns1.nic.nast': {'A': dns.exception.DNSException(), 'AAAA': dns.exception.DNSException()},
                   }
        issue_list = check_resolver_result(ns_dict, self.config)
        self.assertEqual(5, len(issue_list))
        self.assertEqual([NastIssue(IssueCodes.UNEXPECTED_EXCEPTION),
                          NastIssue(IssueCodes.UNEXPECTED_EXCEPTION),
                          NastIssue(IssueCodes.COULD_NOT_RESOLVE_ANY_IP_ADDRESS),
                          NastIssue(IssueCodes.TIMEOUT_WITH_LOCAL_RESOLVER),
                          NastIssue(IssueCodes.TIMEOUT_WITH_LOCAL_RESOLVER)], issue_list)
