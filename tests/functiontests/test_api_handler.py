import unittest
import json
import xmltodict
from flask import Response

from nast.config import Config
from nast.api_handler import get_query, get_homepage, get_alive, get_ready, \
    prepare_answer, answer_json, answer_xml
from nast.issues import IssueSeverity, IssueCodes, NastIssue


class ApiHandlerTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_get_homepage(self):
        self.assertEqual("", get_homepage().get_data(as_text=True))

    def test_get_alive(self):
        self.assertEqual("alive", get_alive().get_data(as_text=True))

    def test_get_ready(self):
        self.assertEqual("ready", get_ready().get_data(as_text=True))

    def test_prepare_answer_issues_without_params(self):
        issue_list = [NastIssue(IssueCodes.IPV6_ADDRESS_NOT_ALLOCATED,
                                IssueSeverity.ERROR)]
        data = prepare_answer(issue_list)
        self.assertEqual({'issues': [{'code': 130,
                                      'message': 'IPv6 address is not allocated',
                                      'severity': 'error'}],
                          'success': False}, data)
        result = answer_xml(data)
        data = result.get_data(as_text=True).strip()
        expected = '<?xml-stylesheet type="text/xsl" href="../meta/nast_answer.xsl"?>\n' + \
            '<predelegation-check success="false" xsi:schemaLocation="../meta/nast_answer.xsd" xmlns="http://schema.denic.de/rng/nast" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n' + \
            '  <issues>\n' + \
            '    <issue code="130" severity="error">\n' + \
            '      <message>IPv6 address is not allocated</message>\n' + \
            '    </issue>\n' + \
            '  </issues>\n' + \
            '</predelegation-check>'
        self.assertEqual(expected.strip(), data)
        # print(str(data))

    def test_two_nameserver(self):
        answer = get_query("json", "two-ns.nast",
                           "ns1.nic.nast",
                           "ns2.nic.nast")
        self.assertEqual({'success': True}, answer)

    def test_get_query_json_success(self):
        answer = get_query("json", "denic.nast",
                           "ns1.denic.nast,172.31.1.1,fd00:10:10::1:1",
                           "ns2.denic.nast,172.31.2.2,fd00:10:10::2:2",
                           None, ["ns3.denic.nast,172.31.3.3,fd00:10:10::3:3",
                                  "ns4.denic.net"],
                           ["257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU="])
        self.assertEqual({'success': True}, answer)

    def test_xml_success(self):
        result = get_query("xml", "denic.nast",
                           "ns1.denic.nast,172.31.1.1,fd00:10:10::1:1",
                           "ns2.denic.nast,172.31.2.2,fd00:10:10::2:2",
                           None, ["ns3.denic.nast,172.31.3.3,fd00:10:10::3:3",
                                  "ns4.denic.net"],
                           ["257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU="])

        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/xml;charset=UTF-8", result.headers['content-type'])
        data = result.get_data(as_text=True).strip()
        # print(str(data))
        self.assertEqual('<?xml-stylesheet type="text/xsl" href="../meta/nast_answer.xsl"?>\n'
                         '<predelegation-check success="true" xsi:schemaLocation="../meta/nast_answer.xsd" xmlns="http://schema.denic.de/rng/nast" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>', data)

        doc = xmltodict.parse(data)
        self.assertEqual("true", doc['predelegation-check']['@success'])

    def test_json_lots_of_issues(self):
        answer = get_query("json", "denic.nast",
                           "ns1.denic.nast,172.31.1.1,fd00:10:10::1:1",
                           "ns2.denic.nast,172.31.10.10,fd00:10:10::10:10",
                           None, ["ns3.denic.nast,172.31.7.7,fd00:10:10::7:7",
                                  "ns4.denic.net"],
                           ["257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU="])

        # with open("tests/testdata/test_json_lots_of_issues.json", mode="w", encoding="utf-8") as fh:
        #    json.dump(answer, fh, indent=4, sort_keys=True)

        with open("tests/testdata/test_json_lots_of_issues.json", mode="r", encoding="utf-8") as fh:
            expected = json.load(fh)
            self.assertEqual(expected, answer)

    def test_xml_lots_of_issues(self):
        result = get_query("xml", "denic.nast",
                           "ns1.denic.nast,172.31.1.1,fd00:10:10::1:1",
                           "ns2.denic.nast,172.31.10.10,fd00:10:10::10:10",
                           None, ["ns3.denic.nast,172.31.7.7,fd00:10:10::7:7",
                                  "ns4.denic.net"],
                           ["257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU="])

        self.assertEqual(200, result.status_code)
        self.assertIn("content-type", result.headers)
        self.assertEqual("application/xml;charset=UTF-8", result.headers['content-type'])

        data = result.get_data(as_text=True).strip()
        # print(data)
        # with open("tests/testdata/test_xml_lots_of_issues.xml", mode="w", encoding="utf-8") as fh:
        #    fh.write(data)
        with open("tests/testdata/test_xml_lots_of_issues.xml", mode="r", encoding="utf-8") as fh:
            expected = fh.read()
            self.assertEqual(expected.strip(), data.strip())

        doc = xmltodict.parse(data)
        # print(str(doc))
        self.assertEqual("false", doc['predelegation-check']['@success'])

    def test_get_query_with_debug(self):
        answer = get_query("json", "simple2ns.nast",
                           "ns3.nic.nast",
                           "ns4.nic.nast",
                           None, None, None, True)

        self.assertEqual(True, answer['success'])
        self.assertIn("debug", answer)
        self.assertIn("queries", answer['debug'])
        self.assertEqual("simple2ns.nast. 86400 IN SOA ns1.nic.nast. network.denic.de. 2021041501 3600 900 604800 86400",
                         answer['debug']['queries']['ns3.nic.nast']['172.31.3.3']['SOA']['udp']['answer'][0])

        #print(json.dumps(answer['debug']['queries'], indent=4))
