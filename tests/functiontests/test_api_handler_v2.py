import unittest
from nast.config import Config
from nast.api_handler_v2 import get_check, get_resolve
from nast.issues import IssueSeverity, IssueCodes, NastIssue
import json


class ApiResolverTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_get_resolve_success(self):
        answer = get_resolve("denic.nast")
        self.assertEqual({'domainname': 'denic.nast',
                          'nameserver': {
                              'ns1.denic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                              'ns2.denic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                              'ns3.denic.nast.': ['172.31.3.3', 'fd00:10:10::3:3'],
                              'ns4.denic.net.': [],
                          },
                          'dnskey': [{'flags': 257,
                                      'algorithm': 8,
                                      'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo'
                                             'STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq'
                                             '6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do'
                                             '+cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98'
                                             'q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66'
                                             'udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa'
                                             'aeNLcWnA8pU=',
                                      'protocol': 3}]}, answer)

    def test_get_resolve_nxdomain(self):
        answer = get_resolve("nonexistant.nast")
        self.assertEqual(({}, 404), answer)

    def test_get_check_success(self):
        answer = get_check("denic.nast", "ns1.denic.nast.,172.31.1.1,fd00:10:10::1:1",
                            "ns2.denic.nast.,172.31.2.2,fd00:10:10::2:2",
                            ["ns3.denic.nast.,172.31.3.3,fd00:10:10::3:3", "ns4.denic.net."],
                            ["257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU="],
                            debug=True
                          )
        self.assertEqual(answer["success"], True)
        self.assertEqual("issues" in answer, False)
        self.assertEqual("debug" in answer, True)
