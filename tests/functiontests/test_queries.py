import unittest
import dns
from nast.config import Config
from nast.queries import Queries
dns.message
import time


class QueriesTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.config.timeout = 0.5
        cls.maxDiff = None

    def test_get_rrset(self):
        cut = Queries(self.config.query_executor)
        nameserver = {
            "ns1.denic.nast": [
                "172.31.1.1",
                "fd00:10:10::1:1"
            ]
        }
        result = (cut.run(nameserver, 'denic.nast.'))
        rrset = result["ns1.denic.nast"]["172.31.1.1"]['A'].get_rrset(
            dns.message.ANSWER,
            dns.name.from_text('ns1.denic.nast.'),
            dns.rdataclass.IN,
            dns.rdatatype.A
        )
        self.assertEqual('ns1.denic.nast. 86400 IN A 172.31.1.1', rrset.to_text())
        self.assertEqual('172.31.1.1', str(rrset[0]))

    def test_async_with_timeouts(self):
        cut = Queries(self.config.query_executor)

        import time
        start_time = time.time()
        result = cut.run({
            'ns10.nic.nast.': ['172.31.10.10', 'fd00:10:10::10:10']
        }, 'reachability-dropps.nast.')
        end_time = time.time()

        self.assertLess(end_time - start_time, 5)
        self.assertIsInstance(result['ns10.nic.nast.']['172.31.10.10']['SOA']['udp'],
                              dns.exception.Timeout)
        self.assertIsInstance(result['ns10.nic.nast.']['172.31.10.10']['SOA']['tcp'],
                              dns.exception.Timeout)
        self.assertIsInstance(result['ns10.nic.nast.']['fd00:10:10::10:10']['SOA']['udp'],
                              dns.exception.Timeout)
        self.assertIsInstance(result['ns10.nic.nast.']['fd00:10:10::10:10']['SOA']['tcp'],
                              dns.exception.Timeout)

    def test_empty_nameserver_dict(self):
        cut = Queries(self.config.query_executor)
        result = cut.run(dict(), 'no-nameserver.nast.')
        self.assertEqual(0, len(result))
