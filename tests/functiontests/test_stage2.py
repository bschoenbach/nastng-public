import unittest
import json
from nast.config import Config
import dns
import dns.exception
from nast import stage2
from nast.issues import NastIssue, IssueCodes, IssueSeverity
from pprint import pprint


class Stage2Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.config.timeout = 0.5
        cls.maxDiff = None

    def test_run_stage2_no_errors(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10::4:4'],
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        self.assertEqual(4, len(result))
        self.assertEqual(2, len(result['ns1.denic.nast']))
        self.assertEqual(2, len(result['ns2.denic.nast']))
        self.assertEqual(2, len(result['ns2.denic.nast']))
        self.assertEqual(2, len(result['ns4.denic.net']))
        # print(str(result))
        for nameserver in result:
            for ip_address in result[nameserver]:
                self.assertIn("SOA", result[nameserver][ip_address])
                for protocol in ['tcp', 'udp']:
                    self.assertIn(protocol, result[nameserver][ip_address]['SOA'])
                    self.assertIsInstance(result[nameserver][ip_address]['SOA'][protocol], dns.message.Message)

    def test_run_stage2_no_errors_long_ipv6(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10:0:0:0:1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10:0:0:0:2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10:0:0:0:3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10:0:0:0:4:4'],
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        self.assertEqual(4, len(result))
        self.assertEqual(2, len(result['ns1.denic.nast']))
        self.assertEqual(2, len(result['ns2.denic.nast']))
        self.assertEqual(2, len(result['ns2.denic.nast']))
        self.assertEqual(2, len(result['ns4.denic.net']))
        # print(str(result))
        for nameserver in result:
            for ip_address in result[nameserver]:
                self.assertIn("SOA", result[nameserver][ip_address])
                for protocol in ['tcp', 'udp']:
                    self.assertIn(protocol, result[nameserver][ip_address]['SOA'])
                    self.assertIsInstance(result[nameserver][ip_address]['SOA'][protocol], dns.message.Message)

    def test_run_stage2_rri_glue(self):
        params = {'domain': 'xn---test-glues-ncb87aud4a26f.de',
                  'nameserver': {'dns1.xn---test-glues-ncb87aud4a26f.de.': ['172.31.1.1', 'fd00:10:10:0:0:0:1:1'],
                                 'ns2.nic.nast': []
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)

    def test_run_stage2_rri_glue_wrong_nameserver(self):
        params = {'domain': 'xn---test-glues-ncb87aud4a26f.de',
                  'nameserver': {'xn---test-glues-ncb87aud4a26f.de.': ['172.31.1.1', 'fd00:10:10:0:0:0:1:1'],
                                 'ns2.nic.nast': []
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)

        self.assertEqual([
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_RRS, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_RRS, IssueSeverity.ERROR),
        ], issue_list)

    def test_run_stage2_nameserver_not_authoritative(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.0.2', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10::4:4'],
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([NastIssue(IssueCodes.UNEXPECTED_RCODE),
                          NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES),
                          NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES),
                          NastIssue(IssueCodes.UNEXPECTED_RCODE),
                          NastIssue(IssueCodes.ANSWER_NOT_AUTHORITATIVE)],
                         issue_list)

        self.assertEqual({'entity': 'SOA', 'rcode': 'REFUSED', 'target': '172.31.0.2'},
                         issue_list[0].get_params())
        self.assertEqual({'determined_ips': '[]',
                          'ns': 'ns3.denic.nast',
                          'provided_ips': "['172.31.0.2', 'fd00:10:10::3:3']"}, issue_list[1].get_params())
        self.assertEqual({'determined_ips': "['172.31.3.3', 'fd00:10:10::3:3']",
                          'ns': 'ns3.denic.nast',
                          'provided_ips': "['172.31.0.2', 'fd00:10:10::3:3']"},
                         issue_list[2].get_params())
        self.assertEqual({'ip': '172.31.0.2',
                          'nameserver': 'ns3.denic.nast',
                          'proto': 'udp',
                          'record': 'NS'},                           
                         issue_list[3].get_params())

    def test_run_stage2_recursive_answer(self):
        params = {'domain': 'recursive-queries-allowed.nast',
                  'nameserver': {'ns3.nic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns12.nic.nast': ['172.31.12.12', 'fd00:10:10::12:12']
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([NastIssue(IssueCodes.RECURSIVE_QUERIES_SHOULD_NOT_BE_ALLOWED,
                                    IssueSeverity.WARNING),
                          NastIssue(IssueCodes.RECURSIVE_QUERIES_SHOULD_NOT_BE_ALLOWED,
                                    IssueSeverity.WARNING)],
                         issue_list)
        self.assertEqual({"nameserver": "ns12.nic.nast",
                          "ip": "172.31.12.12"}, issue_list[0].get_params())
        self.assertEqual({"nameserver": "ns12.nic.nast",
                          "ip": "fd00:10:10::12:12"}, issue_list[1].get_params())

    def test_ipv6_unreachable(self):
        params = {
            'domain': 'reachability-ipv4.nast.',
            'nameserver': {
                'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns6.nic.nast.': ['172.31.6.6', 'fd00:10:10::6:6']
            }
        }

        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual(2, len(issue_list))
        self.assertEqual([
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING)
        ], issue_list)
        self.assertEqual({"nameserver": "ns6.nic.nast",
                          "ip": "fd00:10:10::6:6",
                          "protocol": "udp"},
                         issue_list[0].get_params())
        self.assertEqual({"nameserver": "ns6.nic.nast",
                          "ip": "fd00:10:10::6:6",
                          "protocol": "tcp"},
                         issue_list[1].get_params())

    def test_ipv4_unreachable(self):
        params = {
            'domain': 'reachability-ipv6.nast',
            'nameserver': {
                'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns5.nic.nast.': ['172.31.5.5', 'fd00:10:10::5:5']
            }
        }

        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual(2, len(issue_list))
        self.assertEqual([
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING)
        ], issue_list)
        self.assertEqual({"nameserver": "ns5.nic.nast",
                          "ip": "172.31.5.5",
                          "protocol": "udp"},
                         issue_list[0].get_params())
        self.assertEqual({"nameserver": "ns5.nic.nast",
                          "ip": "172.31.5.5",
                          "protocol": "tcp"},
                         issue_list[1].get_params())

    def test_tcp_unreachable(self):
        params = {
            'domain': 'reachability-udp.nast.',
            'nameserver': {
                'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns7.nic.nast.': ['172.31.7.7', 'fd00:10:10::7:7']
            }
        }

        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual(2, len(issue_list))
        self.assertEqual([
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING)
        ], issue_list)
        self.assertEqual({"nameserver": "ns7.nic.nast",
                          "ip": "172.31.7.7",
                          "protocol": "tcp"},
                         issue_list[0].get_params())
        self.assertEqual({"nameserver": "ns7.nic.nast",
                          "ip": "fd00:10:10::7:7",
                          "protocol": "tcp"},
                         issue_list[1].get_params())

    def test_udp_unreachable(self):
        params = {
            'domain': 'reachability-tcp.nast.',
            'nameserver': {
                'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns8.nic.nast.': ['172.31.8.8', 'fd00:10:10::8:8']
            }
        }

        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual(2, len(issue_list))
        self.assertEqual([
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT)
        ], issue_list)
        self.assertEqual({"nameserver": "ns8.nic.nast",
                          "ip": "172.31.8.8",
                          "protocol": "udp"},
                         issue_list[0].get_params())
        self.assertEqual({"nameserver": "ns8.nic.nast",
                          "ip": "fd00:10:10::8:8",
                          "protocol": "udp"},
                         issue_list[1].get_params())

    def test_run_stage2_nameserver_unreachable(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.10.10', 'fd00:10:10::10:10'],
                                 'ns3.denic.nast': ['172.31.99.2', 'fd00:10:10::99:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10::4:4'],
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        # for item in issue_list:
        #    print(str(item))
        case1 = ([
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING),
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING),
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING),
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING),
        ] == issue_list)
        case2 = ([
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING),
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.TIMEOUT, IssueSeverity.WARNING),
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.HOST_UNREACHABLE, IssueSeverity.WARNING),
            NastIssue(IssueCodes.TIMEOUT),
            NastIssue(IssueCodes.HOST_UNREACHABLE, IssueSeverity.WARNING),
        ] == issue_list)

        self.assertEqual(8, len(issue_list))
        self.assertTrue(case1 or case2)

        self.assertEqual({"nameserver": "ns2.denic.nast",
                          "ip": "172.31.10.10",
                          "protocol": "udp"},
                         issue_list[0].get_params())
        self.assertEqual({"nameserver": "ns2.denic.nast",
                          "ip": "172.31.10.10",
                          "protocol": "tcp"},
                         issue_list[1].get_params())

        self.assertEqual({"nameserver": "ns3.denic.nast",
                          "ip": "172.31.99.2",
                          "protocol": "udp"},
                         issue_list[4].get_params())
        self.assertEqual({"nameserver": "ns3.denic.nast",
                          "ip": "fd00:10:10::99:3",
                          "protocol": "tcp"},
                         issue_list[7].get_params())

    def test_run_stage2_nameserver_rejects_packages(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.9.9', 'fd00:10:10::9:9'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10::4:4'],
                                 }
                  }
        # Bei IPv6 kommt der CONNECTION_REFUSED erst nach einer Sekunde, daher verlängern
        # wir hier den Timeout
        # TODO: Warum ist das so?
        my_config = self.config
        my_config.timeout = 2
        result, issue_list = stage2.run_stage2(my_config, params)
        # print(str(issue_list))
        self.assertEqual(4, len(issue_list))
        # self.assertEqual([NastIssue(IssueCodes.CONNECTION_REFUSED, IssueSeverity.ERROR),
        #                  NastIssue(IssueCodes.CONNECTION_REFUSED, IssueSeverity.WARNING),
        #                  NastIssue(IssueCodes.CONNECTION_REFUSED, IssueSeverity.ERROR),
        #                  NastIssue(IssueCodes.CONNECTION_REFUSED, IssueSeverity.WARNING),
        #                  ], issue_list)
        self.assertEqual({"nameserver": "ns2.denic.nast",
                          "ip": "172.31.9.9",
                          "protocol": "udp"},
                         issue_list[0].get_params())
        self.assertEqual({"nameserver": "ns2.denic.nast",
                          "ip": "172.31.9.9",
                          "protocol": "tcp"},
                         issue_list[1].get_params())
        self.assertEqual({"nameserver": "ns2.denic.nast",
                          "ip": "fd00:10:10::9:9",
                          "protocol": "tcp"},
                         issue_list[3].get_params())

    def test_nameserver_doesnt_know_zone_unexpected_rcode(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.3', 'fd00:10:10::2:3'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10::4:4']

                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual(8, len(issue_list))
        self.assertEqual([NastIssue(IssueCodes.UNEXPECTED_RCODE),
                          NastIssue(IssueCodes.UNEXPECTED_RCODE),
                          NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES),
                          NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES),
                          NastIssue(IssueCodes.UNEXPECTED_RCODE),
                          NastIssue(IssueCodes.ANSWER_NOT_AUTHORITATIVE),
                          NastIssue(IssueCodes.UNEXPECTED_RCODE),
                          NastIssue(IssueCodes.ANSWER_NOT_AUTHORITATIVE)], issue_list)
        self.assertEqual({"target": "172.31.2.3",
                          "entity": "SOA",
                          "rcode": "REFUSED"},
                         issue_list[0].get_params())
        self.assertEqual({"target": "fd00:10:10::2:3",
                          "entity": "SOA",
                          "rcode": "REFUSED"},
                         issue_list[1].get_params())

    def test_check_a_aaaa_for_nameserver(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1',
                                                    'fd00:10:10::2:2'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3',
                                                    'fd00:10:10::4:4'],
                                 'ns4.denic.net': ['fd00:10:10::4:4'],
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)

        correct = [
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR),
            NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_IP_ADDRESSES, IssueSeverity.ERROR)
        ]

        self.assertEqual(correct, issue_list)
        for i in range(3):
            self.assertEqual({
                'ns': 'ns1.denic.nast',
                'provided_ips': "['172.31.1.1', 'fd00:10:10::1:1', 'fd00:10:10::2:2']",
                'determined_ips': "['172.31.1.1', 'fd00:10:10::1:1']",
            }, issue_list[i].get_params())

        for i in range(3):
            self.assertEqual({
                'ns': 'ns3.denic.nast',
                'provided_ips': "['172.31.3.3', 'fd00:10:10::3:3', 'fd00:10:10::4:4']",
                'determined_ips': "['172.31.3.3', 'fd00:10:10::3:3']",
            }, issue_list[3 + i].get_params())

    def test_check_soa_correct_close(self):
        params = {'domain': 'soa-correct-close.nast.',
                  'nameserver': {'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.nic.nast.': ['172.31.2.2', 'fd00:10:10::2:2']
                                 }
                  }

        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)

    def test_check_soa_all_values_to_low(self):
        params = {'domain': 'soa-values-to-low.nast',
                  'nameserver': {'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.nic.nast.': ['172.31.2.2', 'fd00:10:10::2:2']
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        correct = [
            NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING),
            NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING),
            NastIssue(IssueCodes.EXPIRE_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING),
            NastIssue(IssueCodes.MINTTL_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING)
        ] * 4

        self.assertEqual(correct, issue_list)

        self.assertEqual({
            'expected': '[3600..86400]',
            'found': '3599',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[0].get_params())
        self.assertEqual({
            'expected': '[900..28800]',
            'found': '899',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[1].get_params())
        self.assertEqual({
            'expected': '[604800..3600000]',
            'found': '604799',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[2].get_params())
        self.assertEqual({
            'expected': '[180..86400]',
            'found': '179',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[3].get_params())

    def test_check_soa_all_values_to_high(self):
        params = {'domain': 'soa-values-to-high.nast',
                  'nameserver': {'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.nic.nast.': ['172.31.2.2', 'fd00:10:10::2:2']
                                 }
                  }

        result, issue_list = stage2.run_stage2(self.config, params)
        correct = [
            NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING),
            NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING),
            NastIssue(IssueCodes.EXPIRE_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING),
            NastIssue(IssueCodes.MINTTL_VALUE_OUT_OF_RANGE, IssueSeverity.WARNING)
        ] * 4

        self.assertEqual(correct, issue_list)

        self.assertEqual({
            'expected': '[3600..86400]',
            'found': '86401',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[0].get_params())
        self.assertEqual({
            'expected': '[900..28800]',
            'found': '28801',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[1].get_params())
        self.assertEqual({
            'expected': '[604800..3600000]',
            'found': '3600001',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[2].get_params())
        self.assertEqual({
            'expected': '[180..86400]',
            'found': '86401',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[3].get_params())

    def test_check_soa_different_mname_and_retry_not_in_range(self):
        params = {'domain': 'soa-mname-different.nast',
                  'nameserver': {'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.nic.nast.': ['172.31.2.2', 'fd00:10:10::2:2']
                                 }
                  }

        result, issue_list = stage2.run_stage2(self.config, params)
        correct = [
            NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE_REFRESH, IssueSeverity.WARNING),
            NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE_REFRESH, IssueSeverity.WARNING),
            NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE_REFRESH, IssueSeverity.WARNING),
            NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE_REFRESH, IssueSeverity.WARNING),
            NastIssue(IssueCodes.MNANE_INCONSISTENT_ACROSS_SOA_RECORDS, IssueSeverity.WARNING),
        ]

        self.assertEqual(correct, issue_list)

        self.assertEqual({
            'expected': '[10000..26667]',
            'found': '9999',
            'nameserver': 'ns1.nic.nast.',
            'ip': '172.31.1.1'
        }, issue_list[0].get_params())
        self.assertEqual({
            'expected': '[3750..10000]',
            'found': '10001',
            'nameserver': 'ns2.nic.nast.',
            'ip': '172.31.2.2'
        }, issue_list[2].get_params())

        self.assertEqual({
            'master': '[ns1.nic.nast., ns1.nic.nast., ns2.nic.nast., ns2.nic.nast.]',
        }, issue_list[4].get_params())

    def test_check_ns_rrset(self):
        params = {'domain': 'ns-rrset-different.nast.',
                  'nameserver': {'ns1.nic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.nic.nASt': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'NS3.nic.Nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.nic.nast': ['172.31.4.4', 'fd00:10:10::4:4']
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)

        self.assertEqual([NastIssue(IssueCodes.INCONSISTENT_SET_OF_NAMESERVER_RRS)] * 4, issue_list)

        self.assertEqual({
            'NS': 'ns1.nic.nast',
            'IP': '172.31.1.1',
            'NS host names': "['ns1.nic.nast', 'ns2.nic.nast', 'ns3.nic.nast']",
        }, issue_list[0].get_params())
        self.assertEqual({
            'NS': 'ns1.nic.nast',
            'IP': 'fd00:10:10::1:1',
            'NS host names': "['ns1.nic.nast', 'ns2.nic.nast', 'ns3.nic.nast']",
        }, issue_list[1].get_params())
        self.assertEqual({
            'NS': 'ns4.nic.nast',
            'IP': '172.31.4.4',
            'NS host names': "['ns1.nic.nast', 'ns2.nic.nast', 'ns3.nic.nast', "
                             "'ns4.nic.nast', 'ns5.nic.nast']",
        }, issue_list[2].get_params())
        self.assertEqual({
            'NS': 'ns4.nic.nast',
            'IP': 'fd00:10:10::4:4',
            'NS host names': "['ns1.nic.nast', 'ns2.nic.nast', 'ns3.nic.nast', "
                             "'ns4.nic.nast', 'ns5.nic.nast']",
        }, issue_list[3].get_params())

    def test_ipv6_iana_check(self):
        params = {'domain': 'invalid-aaaa-rr.nast.',
                  'nameserver': {'ns1.nic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.invalid-aaaa-rr.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.invalid-aaaa-rr.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.invalid-aaaa-rr.nast': ['172.31.4.4', 'fd00:10:10::4:4']
                                 }
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        iana_issues_130 = []
        iana_issues_131 = []
        for issue in issue_list:
            if issue.get_code() == 130:
                iana_issues_130.append(issue)
            if issue.get_code() == 131:
                iana_issues_131.append(issue)
        self.assertEqual(3, len(iana_issues_130))
        self.assertEqual(1, len(iana_issues_131))
