import unittest
import dns
from nast.config import Config
from nast.resolver import Resolver
import time


class ResolverTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.config.timeout = 0.5
        cls.maxDiff = None

    def test_no_nameserver(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run([])
        self.assertEqual({}, result)

    def test_2_ns_with_1_ip_v4_resolver_queried_by_ipv4(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2-1xv4.resolvertest.nast"])

        self.assertEqual(result, {"ns1-1xv4.resolvertest.nast": {'A': ["81.91.170.12"], 'AAAA': []},
                                  "ns2-1xv4.resolvertest.nast": {'A': ["148.251.94.99"], 'AAAA': []}})

    def test_2_ns_with_1_ip_v4_resolver_queried_by_ipv6(self):
        cut = Resolver("fd00:10:10::2", self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2-1xv4.resolvertest.nast"])

        self.assertEqual(result, {"ns1-1xv4.resolvertest.nast": {'A': ["81.91.170.12"], 'AAAA': []},
                                  "ns2-1xv4.resolvertest.nast": {'A': ["148.251.94.99"], 'AAAA': []}})

    def test_ns1_1xv4_ns2_nxdomain(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "unresolvable.resolvertest.nast"])
        self.assertEqual(result, {"ns1-1xv4.resolvertest.nast": {'A': ["81.91.170.12"], 'AAAA': []},
                                  "unresolvable.resolvertest.nast": {'A': [], 'AAAA': []}})

    def test_ns1_1xv4_ns2_1xv6(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns1-1xv6.resolvertest.nast"])
        self.assertEqual(result, {"ns1-1xv4.resolvertest.nast": {'A': ["81.91.170.12"], 'AAAA': []},
                                  "ns1-1xv6.resolvertest.nast": {'A': [], 'AAAA': ["2001:1634:9::53"]}})

    def test_ns1_1xv4_1xv6_ns2_1xv4_1xv6(self):
        # Nameserver 1 and 2 have each 1 A- and 1 AAAA-RR
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4-1xv6.resolvertest.nast",
                          "ns2-1xv4-1xv6.resolvertest.nast"])
        self.assertEqual(result, {"ns1-1xv4-1xv6.resolvertest.nast": {'A': ["81.91.170.12"], 'AAAA': ['2001:668:1f:11::106']},
                                  "ns2-1xv4-1xv6.resolvertest.nast": {'A': ["77.19.123.13"], 'AAAA': ["2001:5411:abcd:9876::13"]}})

    def test_ns1_3xv4_2xv6_ns2_2xv4_2xv6(self):
        # Nameserver 1 and 2 have each 1 A- and 1 AAAA-RR
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-3xv4-2xv6.resolvertest.nast",
                          "ns-2xv4-2xv6.resolvertest.nast"])

        self.assertIn("ns1-3xv4-2xv6.resolvertest.nast", result)
        self.assertIn("ns-2xv4-2xv6.resolvertest.nast", result)
        self.assertEqual(sorted(result['ns1-3xv4-2xv6.resolvertest.nast']['A']),
                         sorted(["110.112.1.1", "110.112.111.111", "222.222.222.222"]))
        self.assertEqual(sorted(result['ns1-3xv4-2xv6.resolvertest.nast']['AAAA']),
                         sorted(['2001:1111:1111::1', '2001:2222:2222::2']))
        self.assertEqual(sorted(result['ns-2xv4-2xv6.resolvertest.nast']['A']),
                         sorted(['66.39.126.17', '42.42.42.42']))
        self.assertEqual(sorted(result['ns-2xv4-2xv6.resolvertest.nast']['AAAA']),
                         sorted(['2001:19:3::cafe', '2001:2147:79::beef']))

    def test_ns1_1xv4_ns2_rejects(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2.rejecting.nast"])
        self.assertIn("ns1-1xv4.resolvertest.nast", result)
        self.assertIn("ns2.rejecting.nast", result)
        self.assertEqual({'A': ['81.91.170.12'], 'AAAA': []}, result['ns1-1xv4.resolvertest.nast'])
        self.assertIsInstance(result['ns2.rejecting.nast']['A'], dns.exception.Timeout)
        self.assertIsInstance(result['ns2.rejecting.nast']['AAAA'], dns.exception.Timeout)

    def test_ns1_1xv4_ns2_dropps(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2.dropping.nast"])
        self.assertIn("ns1-1xv4.resolvertest.nast", result)
        self.assertIn("ns2.dropping.nast", result)
        self.assertEqual({'A': ['81.91.170.12'], 'AAAA': []}, result['ns1-1xv4.resolvertest.nast'])
        self.assertIsInstance(result['ns2.dropping.nast']['A'], dns.exception.Timeout)
        self.assertIsInstance(result['ns2.dropping.nast']['AAAA'], dns.exception.Timeout)

    def test_ns1_1xv4_ns2_nonexistant(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2.nonexisting.nast"])
        self.assertIn("ns1-1xv4.resolvertest.nast", result)
        self.assertIn("ns2.nonexisting.nast", result)
        self.assertEqual({'A': ['81.91.170.12'], 'AAAA': []}, result['ns1-1xv4.resolvertest.nast'])
        self.assertEqual({'A': [], 'AAAA': []}, result['ns2.nonexisting.nast']),

    def test_4ns(self):
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns1-1xv6.resolvertest.nast",
                          "ns1-1xv4-1xv6.resolvertest.nast",
                          "ns2-1xv4-1xv6.resolvertest.nast"])
        self.assertEqual(4, len(result))
        self.assertEqual({'A': ['81.91.170.12'], 'AAAA': []}, result['ns1-1xv4.resolvertest.nast'])
        self.assertEqual({'A': [], 'AAAA': ['2001:1634:9::53']}, result['ns1-1xv6.resolvertest.nast'])
        self.assertEqual({'A': ['81.91.170.12'], 'AAAA': ['2001:668:1f:11::106']}, result['ns1-1xv4-1xv6.resolvertest.nast'])
        self.assertEqual({'A': ['77.19.123.13'], 'AAAA': ['2001:5411:abcd:9876::13']}, result['ns2-1xv4-1xv6.resolvertest.nast'])

    def test_resolver_rejects_packets(self):
        # Bei IPv6 kommt der CONNECTION_REFUSED erst nach einer Sekunde, daher verlängern
        # qir hier den Timeout
        cut = Resolver("172.31.9.9", self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2-1xv4.resolvertest.nast"])

        # print(str(result))
        self.assertEqual(2, len(result))
        self.assertIsInstance(result['ns1-1xv4.resolvertest.nast']['A'],  ConnectionRefusedError)
        self.assertIsInstance(result['ns1-1xv4.resolvertest.nast']['AAAA'], ConnectionRefusedError)
        self.assertIsInstance(result['ns2-1xv4.resolvertest.nast']['A'], ConnectionRefusedError)
        self.assertIsInstance(result['ns2-1xv4.resolvertest.nast']['AAAA'], ConnectionRefusedError)
        #self.assertEqual({'A': [], 'AAAA': []}, result['ns1-1xv4.resolvertest.nast'])
        #self.assertEqual({'A': [], 'AAAA': []}, result['ns2-1xv4.resolvertest.nast'])

    def test_resolver_drops_packets(self):
        cut = Resolver("172.31.10.10", self.config.query_executor)
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2-1xv4.resolvertest.nast"])

        self.assertIsInstance(result['ns1-1xv4.resolvertest.nast']['A'], dns.exception.Timeout)
        self.assertIsInstance(result['ns1-1xv4.resolvertest.nast']['AAAA'], dns.exception.Timeout)
        self.assertIsInstance(result['ns2-1xv4.resolvertest.nast']['A'], dns.exception.Timeout)
        self.assertIsInstance(result['ns2-1xv4.resolvertest.nast']['AAAA'], dns.exception.Timeout)

    def test_tsuname(self):
        # see https://tsuname.io/
        cut = Resolver(self.config.resolver, self.config.query_executor)
        result = cut.run(["ns1.loop.xxx",
                          "ns2.loop.yyy"])
        self.assertEqual(result, {"ns1.loop.xxx": {'A': [], 'AAAA': []},
                                  "ns2.loop.yyy": {'A': [], 'AAAA': []}})
        result = cut.run(["ns1.bigloop.xxx",
                          "ns2.bigloop.xxx"])
        self.assertEqual(result, {"ns1.bigloop.xxx": {'A': [], 'AAAA': []},
                                  "ns2.bigloop.xxx": {'A': [], 'AAAA': []}})

    def test_async_with_timeouts(self):
        cut = Resolver("172.31.10.10", self.config.query_executor)

        start_time = time.time()
        result = cut.run(["ns1-1xv4.resolvertest.nast",
                          "ns2-1xv4.resolvertest.nast"])
        end_time = time.time()
        self.assertLess(end_time - start_time, 5)

        self.assertIsInstance(result['ns1-1xv4.resolvertest.nast']['A'], dns.exception.Timeout)
        self.assertIsInstance(result['ns1-1xv4.resolvertest.nast']['AAAA'], dns.exception.Timeout)
        self.assertIsInstance(result['ns2-1xv4.resolvertest.nast']['A'], dns.exception.Timeout)
        self.assertIsInstance(result['ns2-1xv4.resolvertest.nast']['AAAA'], dns.exception.Timeout)
