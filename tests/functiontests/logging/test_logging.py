from cmath import log
from distutils.command.config import config
import json
from logging import INFO
from pprint import pprint
import unittest

from jsonschema import validate
import jsonschema
import urllib.parse
from api import handle_exception
from nast import stage2
from nast.api_handler import get_query
from nast.config import Config
from nast.api_handler_v2 import get_check, get_resolve
from nast.logging import JSON_LOGGER


class JSONLoggingTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.config.timeout = 0.5
        cls.maxDiff = None
        cls.log_file = open(cls.config.log_file_path, 'r')
        cls.log_schema = cls.import_json_schema(cls, './tests/functiontests/logging/JSONLoggingSchema.json')

    def import_json_schema(self, file_path_schema):
        # Opening JSON file
        schema_file = open(file_path_schema)        
        # returns JSON object as a dictionary
        return json.load(schema_file)
    
    def validate_json_schema(self, json_data):
        try:
            validate(instance=json_data, schema=self.log_schema)
        except jsonschema.exceptions.ValidationError as error:
            print(error)
            return False
        return True

    def validate_url(self, url):
        try:
            result = urllib.parse.urlparse(url)
            return all([result.scheme, result.netloc])
        except ValueError:
            return False
     
    def test_v2_json_logging_resolve_no_fail(self):
        exp_params = {
                'domainname': 'denic.nast',
                'nameserver': [
                    'ns1.denic.nast,172.31.1.1,fd00:10:10::1:1',
                    'ns2.denic.nast,172.31.2.2,fd00:10:10::2:2',
                    'ns3.denic.nast,172.31.3.3,fd00:10:10::3:3',
                    'ns4.denic.net'
                ],
                'dnskey': [{'flags': 257,
                            'algorithm': 8,
                            'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo'
                                   'STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq'
                                   '6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do'
                                   '+cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98'
                                   'q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66'
                                   'udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa'
                                   'aeNLcWnA8pU=',
                            'protocol': 3}]
        }
        get_resolve("denic.nast")

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)
        
        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(exp_params['domainname'], log_dict['data']['domainname'])
        self.assertEqual(log_dict['data']['answer']['domainname'],  exp_params['domainname'])
        self.assertEqual(exp_params['nameserver'], sorted(log_dict['data']['answer']['nameserver']))
        self.assertEqual(exp_params['dnskey'], sorted(log_dict['data']['answer']['dnskey']))
        self.assertEqual("resolve", log_dict['type'])
        self.assertEqual("v2", log_dict['api'])
    
    def test_v2_json_logging_resolve_fail(self):
        exp_params = {
                'domainname': 'unknown.de',
        }
        get_resolve(exp_params['domainname'])

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)
        
        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(exp_params['domainname'], log_dict['data']['domainname'])
        self.assertEqual([], log_dict['data']['answer']['nameserver'])
        self.assertEqual("resolve", log_dict['type'])
        self.assertEqual("v2", log_dict['api'])

    def test_v2_json_logging_check_no_fail(self):
        exp_params = {
                'domain': 'denic.nast',
                'nameserver': [
                    'ns1.denic.nast,172.31.1.1,fd00:10:10::1:1',
                    'ns2.denic.nast,172.31.2.2,fd00:10:10::2:2',
                    'ns3.denic.nast,172.31.3.3,fd00:10:10::3:3',
                    'ns4.denic.net'
                ],
                'dnskey': []
        }
        get_check(exp_params['domain'], exp_params['nameserver'][0],
                exp_params['nameserver'][1], exp_params['nameserver'][2:len(exp_params['nameserver'])],
                debug=True
                )

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)

        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(log_dict['data']['answer']['success'], True)
        self.assertEqual('debug' in log_dict['data']['answer'], False)
        self.assertEqual(exp_params, log_dict['data']['params'])
        self.assertEqual("check", log_dict['type'])
        self.assertEqual("v2", log_dict['api'])
        self.assertEqual(True, self.validate_url(log_dict['data']['link']))
    
    def test_v2_json_logging_check_fail_lots_of_issues(self):
        exp_params = {
                'domain': 'denic.nast',
                'nameserver': [
                    'ns1.denic.nast,172.31.1.1,fd00:10:10::1:1',
                    'ns2.denic.nast,172.31.2.2,fd00:10:10::2:2',
                    'ns3.denic.nast,172.31.7.7,fd00:10:10::7:7',
                    'ns4.denic.net'
                ],
                'dnskey': [
                    { "flags": 257,
                      "protocol": 3,
                      "algorithm": 8,
                      'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo'
                             'STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq'
                             '6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do'
                             '+cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98'
                             'q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66'
                             'udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa'
                             'aeNLcWnA8pU=',
                      }
                ]
        }
        get_check(exp_params['domain'], exp_params['nameserver'][0],
                exp_params['nameserver'][1], exp_params['nameserver'][2:len(exp_params['nameserver'])],
                dnskey=[str(exp_params['dnskey'][0]['flags'])+" "+str(exp_params['dnskey'][0]['protocol'])+" "+
                                str(exp_params['dnskey'][0]['algorithm'])+" "+exp_params['dnskey'][0]['key']],
                debug=True
                )

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)

        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(log_dict['data']['answer']['success'], False)
        self.assertEqual('debug' in log_dict['data']['answer'], False)
        self.assertEqual(exp_params, log_dict['data']['params'])
        self.assertEqual("check", log_dict['type'])
        self.assertEqual("v2", log_dict['api'])
        self.assertEqual(True, self.validate_url(log_dict['data']['link']))

    def test_json_logging_issues_unexpected_exception(self):
        params={'blah': 'blubb', 'nameserver': {'ns1.test.dummy': ["12345", "xd.fg:fg::0"]}}
        stage2.check_is_reachable(Exception(), params)

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)

        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(log_dict['data']['issue_code'], 999)
        self.assertEqual(log_dict['data']['issue_name'], 'Unexpected Exception')
        self.assertEqual(log_dict['data']['check'], 'check_is_reachable')
        self.assertEqual(log_dict['data']['stage'], 2)
        self.assertEqual(log_dict['data']['params'], params)
        self.assertEqual("issue", log_dict['type'])
    
    def test_json_logging_message(self):
        JSON_LOGGER.log_message(INFO, __name__, "Hello from test log")

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)

        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(log_dict['message'], "Hello from test log")
        self.assertEqual(log_dict['data'], {})
        self.assertEqual("message", log_dict['type'])
    
    def test_v1_json_logging_check_no_fail(self):
        exp_params = {
                'domain': 'denic.nast',
                'nameserver': [
                    'ns1.denic.nast,172.31.1.1,fd00:10:10::1:1',
                    'ns2.denic.nast,172.31.2.2,fd00:10:10::2:2',
                    'ns3.denic.nast,172.31.3.3,fd00:10:10::3:3',
                    'ns4.denic.net'
                ],
                'dnskey': []
        }
        get_query("json", "denic.nast",
                "ns1.denic.nast,172.31.1.1,fd00:10:10::1:1",
                "ns2.denic.nast,172.31.2.2,fd00:10:10::2:2",
                None, ["ns3.denic.nast,172.31.3.3,fd00:10:10::3:3",
                "ns4.denic.net"],
                [])

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)

        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(log_dict['data']['answer']['success'], True)
        self.assertEqual(exp_params, log_dict['data']['params'])
        self.assertEqual("check", log_dict['type'])
        self.assertEqual("v1", log_dict['api'])
        self.assertEqual(True, self.validate_url(log_dict['data']['link']))
    
    def test_v1_json_logging_check_fail_lots_of_issues(self):
        exp_params = {
                'domain': 'denic.nast',
                'nameserver': [
                    'ns1.denic.nast,172.31.1.1,fd00:10:10::1:1',
                    'ns2.denic.nast,172.31.10.10,fd00:10:10::10:10',
                    'ns3.denic.nast,172.31.7.7,fd00:10:10::7:7',
                    'ns4.denic.net'
                ],
                'dnskey': [
                    { "flags": 257,
                      "protocol": 3,
                      "algorithm": 8,
                      'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo'
                             'STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq'
                             '6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do'
                             '+cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98'
                             'q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66'
                             'udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa'
                             'aeNLcWnA8pU=',
                      }
                ]
        }
        get_query("json", "denic.nast",
                "ns1.denic.nast,172.31.1.1,fd00:10:10::1:1",
                "ns2.denic.nast,172.31.10.10,fd00:10:10::10:10",
                None, ["ns3.denic.nast,172.31.7.7,fd00:10:10::7:7",
                       "ns4.denic.net"],
                ["257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU="])


        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)

        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(log_dict['data']['answer']['success'], False)
        self.assertEqual(exp_params, log_dict['data']['params'])
        self.assertEqual("check", log_dict['type'])
        self.assertEqual("v1", log_dict['api'])
        self.assertEqual(True, self.validate_url(log_dict['data']['link']))

    def test_json_logging_exception_internal_server_error(self):
        ugly_stack_trace="""['Traceback (most recent call last):\\\\n', 
        '  File \\"/usr/local/lib/python3.10/site-packages/flask/app.py\\",
        line 1517, in full_dispatch_request\\\\n    rv = self.dispatch_request()\\\\n',
        '  File \\"/usr/local/lib/python3.10/site-packages/flask/app.py\\",
        line 1503, in dispatch_request\\\\n    return self.ensure_sync(self.view_functions[rule.endpoint])(**req.view_args)\\\\n',
        '  File \\"/usr/local/lib/python3.10/site-packages/connexion/decorators/decorator.py\\", line 68, in wrapper\\\\n
        response = function(request)\\\\n', '  File \\"/usr/local/lib/python3.10/site-packages/connexion/decorators/uri_parsing.py\\",
        line 149, in wrapper\\\\n    response = function(request)\\\\n', '
        File \\"/usr/local/lib/python3.10/site-packages/connexion/decorators/validation.py\\", line 399, in wrapper\\\\n
        return function(request)\\\\n', '  File \\"/usr/local/lib/python3.10/site-packages/connexion/decorators/response.py\\",
        line 112, in wrapper\\\\n    response = function(request)\\\\n', 
        '  File \\"/usr/local/lib/python3.10/site-packages/connexion/decorators/parameter.py\\", line 120, in wrapper\\\\n
        return function(**kwargs)\\\\n', '  File \\"/usr/local/lib/python3.10/site-packages/nast/api_handler.py\\", line 58, in get_query\\\\n
        JSON_LOGGER.log_check(INFO, __name__, {\\"params\\": original_params, \\"domainname\\": domainname, \\"answer\\": answer, \\"format\\": result_format})\\\\n',
        '  File \\"/usr/local/lib/python3.10/site-packages/nast/logging.py\\", line 89, in log_check\\\\n
        self.format_issue_arguments_to_json_log_array(data_copy[\\"answer\\"][\\"issues\\"])\\\\n', '  
        File \\"/usr/local/lib/python3.10/site-packages/nast/logging.py\\", line 102, in format_issue_arguments_to_json_log_array\\\\n
        if isinstance(issue[\\\\'arguments\\\\'], dict):\\\\n', \\"KeyError: 'arguments'\\\\n\\"]
        """
        handle_exception(Exception("TEST EXCEPTION MESSAGE", ugly_stack_trace))

        log_message = self.log_file.readline()
        log_dict = json.loads(log_message)

        self.assertEqual(True, self.validate_json_schema((log_dict)))
        self.assertEqual(0, log_dict['message'].find("Internal Server Error: (TEST EXCEPTION MESSAGE,"))
        self.assertEqual("exception", log_dict['type'])