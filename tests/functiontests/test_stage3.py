from logging import ERROR
import unittest
from nast.config import Config
import dns
import dns.exception
from nast import stage3
from nast import stage2
from nast.debug import get_parsed_query_results
from nast.issues import NastIssue, IssueCodes, IssueSeverity
from pprint import pprint


class Stage3Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.config.timeout = 0.5
        cls.maxDiff = None

    def test_run_stage3_no_errors(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10::4:4']
                                 },
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 8,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        stage3_issues = stage3.check_dnssec(self.config, params, result)
        self.assertEqual([], stage3_issues)

    def test_run_stage3_no_errors_long_ipv6(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10:0:0:0:1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10:0:0:0:2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10:0:0:0:3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10:0:0:0:4:4']
                                 },
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 8,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        stage3_issues = stage3.check_dnssec(self.config, params, result)
        self.assertEqual([], stage3_issues)

    def test_zone_with_zsk_only(self):
        params = {'domain': 'only-zsk.nast',
                  'nameserver': {'ns3.nic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.nic.nast': ['172.31.4.4', 'fd00:10:10::4:4']
                                 },
                  'dnskey': [{'flags': 256, 'protocol': 3, 'algorithm': 8,
                              'key': 'AwEAAbtEP99lYd3exuQlsZx1C1KP0P986MCh8uxXLlpLUBdBLIWQYTIG U3LRnws65Mmv3/HO5ZkEluRoYam7ZX6202SkJwSYL7q6nR+Egu1lc27E StP38DLlqc4AuI9Yimd5qmqk+uVuCeHH+oAe9g8cL6j0hQURn4/T+QPy /hpUz4xK2Bv0Bd6FK4rmKRC2sEbN7yXQE0x9tRt7XhpPSDTq0WVwPPHm D/+Y/fk09yyZqwSKwoaaXCibyphvHWvha4+upB/CrW9sawaS5h9wZTEz eiGUyqhj3G9atKV4KD0s4amupxkb/EOBIWtkrCgvkJl9AV1CkZgsfytC 6P9p6uzNBgU='
                              }]
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        stage3_issues = stage3.check_dnssec(self.config, params, result)
        self.assertEqual(0, len(stage3_issues))

    def test_run_stage3_no_dnskey_visible(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': ['172.31.4.4', 'fd00:10:10::4:4']
                                 },
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 10,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              },
                             {'flags': 257, 'protocol': 3, 'algorithm': 5,
                                 'key': 'AQPSKmynfzW4kyBv 015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8no kfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6R foRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='
                              }
                             ]
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        stage3_issues = stage3.check_dnssec(self.config, params, result)
        self.assertEqual(3, len(stage3_issues))
        self.assertEqual([
            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
            NastIssue(IssueCodes.ALL_DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.ERROR)
        ], stage3_issues)
        self.assertEqual([
            {"key": "257 10 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31joSTHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do+cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxaaeNLcWnA8pU="},
            {"key": "257 5 AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=="},
            None
        ], [
            stage3_issues[0].get_params(),
            stage3_issues[1].get_params(),
            stage3_issues[2].get_params()
        ])

    def test_run_stage3_dnskey_rrset_not_identical(self):
        params = {'domain': 'dnskey-rrset-unidentical.nast',
                  'nameserver': {'ns3.nic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.nic.nast': ['172.31.4.4', 'fd00:10:10::4:4']
                                 },
                  'dnskey': [
                      {'flags': 257, 'protocol': 3, 'algorithm': 8,
                       'key': 'AwEAAbEEn3av/QlxSo2N1wwdXsLgIAJBxy76CPKAu9WRNYWR1hfuZVLl coewFgOaPCi0e4w2tdf3SgU7QPGd/BeA3Ix+pX7KfiOSMuv+L/RJsgZj YltFcOhw1MVj5W4PquZ3J91z+bAkBCIL3btkLA/ecjzgcZ57bK4DRc0B RlhP5GmU5VCDDaeL3Pnk9rC1EkqJcO3WRvqvt32GLl91q0olusxc2hyd LGFP0yitGkgR98fvyy2W4znRw2/xp5U+D+fVhsUIfl+jMPcNa/7e/4/K 8gecew5DLgzrZWjxLn8tHFvTMftwRvHHX8gxcpp+r34PZj1JLdJH6Ih+ Gw/cvlWr14s='
                       },
                      {'flags': 256, 'protocol': 3, 'algorithm': 8,
                       'key': 'AwEAAaJeCnLC/59Uew/tYi7davko23zKeG6PDqBsdbDOQA5FTYkQi1RZ sLU6+MJdiuwxfjLDxOBI6b42dQab1xeG5J0J2WDgkigbZbWX3mu6wNPr LNtTo9QuzgswQ/Bubz/8w/4e3S1tqI/e/7UAKp65n3ukRiev/iYoCgqD 0idil0kzpoEQ/ulPK17cE0ewc4x4zvU5sRvFeWnjJQun4nge+ps1cTmN aqZHMJ9HBqbhnKSRkMvSkKTR52ANT1HkRFQ4O2tv3BrJLSEvjpsZp/g1 EIzJIrByg55jbQhU8Bez9hEuzs7dg1ikQNVRb3NYwZOyfG3JuhNxe18Z 5k69qD5S198='
                       }
                  ]
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        stage3_issues = stage3.check_dnssec(self.config, params, result)
        self.assertEqual(4, len(stage3_issues))
        self.assertEqual([
            NastIssue(IssueCodes.DNSKEY_RRSET_INCONSISTENT, IssueSeverity.ERROR),
            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
            NastIssue(IssueCodes.ALL_DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.ERROR)
        ], stage3_issues)
        self.assertEqual([
            {'nameservers': 'ns3.nic.nast != ns4.nic.nast'},
            {'key': '257 8 AwEAAbEEn3av/QlxSo2N1wwdXsLgIAJBxy76CPKAu9WRNYWR1hfuZVLlcoewFgOaPCi0e4w2tdf3SgU7QPGd/BeA3Ix+pX7KfiOSMuv+L/RJsgZjYltFcOhw1MVj5W4PquZ3J91z+bAkBCIL3btkLA/ecjzgcZ57bK4DRc0BRlhP5GmU5VCDDaeL3Pnk9rC1EkqJcO3WRvqvt32GLl91q0olusxc2hydLGFP0yitGkgR98fvyy2W4znRw2/xp5U+D+fVhsUIfl+jMPcNa/7e/4/K8gecew5DLgzrZWjxLn8tHFvTMftwRvHHX8gxcpp+r34PZj1JLdJH6Ih+Gw/cvlWr14s='},
            {'key': '256 8 AwEAAaJeCnLC/59Uew/tYi7davko23zKeG6PDqBsdbDOQA5FTYkQi1RZsLU6+MJdiuwxfjLDxOBI6b42dQab1xeG5J0J2WDgkigbZbWX3mu6wNPrLNtTo9QuzgswQ/Bubz/8w/4e3S1tqI/e/7UAKp65n3ukRiev/iYoCgqD0idil0kzpoEQ/ulPK17cE0ewc4x4zvU5sRvFeWnjJQun4nge+ps1cTmNaqZHMJ9HBqbhnKSRkMvSkKTR52ANT1HkRFQ4O2tv3BrJLSEvjpsZp/g1EIzJIrByg55jbQhU8Bez9hEuzs7dg1ikQNVRb3NYwZOyfG3JuhNxe18Z5k69qD5S198='},
            None
        ], [
            stage3_issues[0].get_params(),
            stage3_issues[1].get_params(),
            stage3_issues[2].get_params(),
            stage3_issues[3].get_params()
        ])

    def test_run_stage3_dnskey_proof_of_possession_fail(self):
        params = {'domain': 'dnskey-invalid-sig.nast',
                  'nameserver': {'ns3.nic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.nic.nast': ['172.31.4.4', 'fd00:10:10::4:4']
                                 },
                  'dnskey': [
                      {'flags': 257, 'protocol': 3, 'algorithm': 8,
                       'key': 'AwEAAcUePQHu2ZKaXOIkqOC4tqJm14Ze4OKUA4VICwhk9nFI5xUnhmLH +v8WgKSYHzGDagmceD2lKh+Go2XSslPyZpJ3g0KxAJNu5jvcgEollN5k pgpKcz0IAtPJS6jQssofQqvnFow3LBXpooAcnNQDZvWpay4b2+JuTE3q CDtvGckGA7V6K9gc+m2GGWuKnHN0zPAQ8ogIGZrqQqX8R2FUXz/NCUUJ ZSYv9teoPVtlzTr6zwZMJseIja75nJ767n/83DFAuTTv0m4ljrIIW3I5 JLoWhv6YiQAiWGzEl6EJDJy9zneV3jFRFX1ICMLs28XRlMm1Le2XNAoe Bt4Ok3tAChs='
                       }
                  ]
                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        stage3_issues = stage3.check_dnssec(self.config, params, result)

        self.assertEqual(3, len(stage3_issues))
        self.assertEqual([
            NastIssue(IssueCodes.DNSKEY_RRSET_SIGNATURE_VALIDATION_FAILED, IssueSeverity.ERROR),
            NastIssue(IssueCodes.DNSKEY_RRSET_SIGNATURE_VALIDATION_FAILED, IssueSeverity.ERROR),
            NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED, IssueSeverity.WARNING)
        ], stage3_issues)
        self.assertEqual([
            {'nameserver': 'ns3.nic.nast', 'ip': '172.31.3.3', 'record': 'DNSKEY'},
            {'nameserver': 'ns3.nic.nast', 'ip': 'fd00:10:10::3:3', 'record': 'DNSKEY'},
            {'nameservers': 'ns3.nic.nast, ns4.nic.nast', 'error': 'truncation'}
        ], [
            stage3_issues[0].get_params(),
            stage3_issues[1].get_params(),
            stage3_issues[2].get_params()
        ])

    def test_run_stage3_soa_chain_of_trust_fail(self):
        params = {'domain': 'soa-invalid-sig.nast',
                  'nameserver': {'ns3.nic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.nic.nast': ['172.31.4.4', 'fd00:10:10::4:4']
                                 },
                  'dnskey': [
                      {'flags': 257, 'protocol': 3, 'algorithm': 8,
                       'key': 'AwEAAdf5VjMNvZSqKh6+9xYu1DWwu2Du5fz1BiiEGRiUPFRe8i1MCQ76 DKx58EulDtmCOeZ1c0eXw2EzxVyx6Fn90rgmoWBow1ThZhzgAd3NhbQQ pAxKcIZ7EgC5UCBTq0Miugbj9kGMwQ/sNBvTld7kr0bYWT0d7oBbDXkn IV5a1H9trvJ3pCWZ+Bp7uH3N54TFLHraEbzPmasgzvptkSk2SLSFlBkF PN82PSrhX4oqnGjcthy+0Texw6UHkAlV99Yq0FrCkIMNN/vZyuQQnQt/ og+LNPHReshbhu8iCnPdH77Ow5RbIhhz7P96vXB9iSCdWNU+uxPFKypL EnDioJ6WeZ0='
                       }
                  ]

                  }
        result, issue_list = stage2.run_stage2(self.config, params)
        self.assertEqual([], issue_list)
        stage3_issues = stage3.check_dnssec(self.config, params, result)

        self.assertEqual(2, len(stage3_issues))
        self.assertEqual([
            NastIssue(IssueCodes.SOA_RRSET_SIGNATURE_VALIDATION_FAILED, IssueSeverity.ERROR),
            NastIssue(IssueCodes.SOA_RRSET_SIGNATURE_VALIDATION_FAILED, IssueSeverity.ERROR)
        ], stage3_issues)
        self.assertEqual([
            {'nameserver': 'ns4.nic.nast', 'ip': '172.31.4.4', 'record': 'SOA'},
            {'nameserver': 'ns4.nic.nast', 'ip': 'fd00:10:10::4:4', 'record': 'SOA'},
        ], [
            stage3_issues[0].get_params(),
            stage3_issues[1].get_params()
        ])
