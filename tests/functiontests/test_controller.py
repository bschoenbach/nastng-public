import unittest
from nast.config import Config
from nast.controller import pre_delegation_check


class ControllerTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_pre_delegation_check_ns_and_glues_and_dnssec_ok(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'protocol': 3, 'algorithm': 8,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        issue_list, nameserver, query_results = pre_delegation_check(params)
        self.assertEqual([], issue_list)
