import unittest
from nast.config import Config
from nast import stage2
from nast.issues import NastIssue, IssueCodes, IssueSeverity
import dns.exception


class Stage2Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_check_is_reachable_udp_with_unexpected_exception(self):
        new_issue = stage2.check_is_reachable(Exception(), {'blah': 'blubb', 'nameserver': {'ns1.test.dummy': ["12345", "xd.fg:fg::0"]}})
        self.assertEqual(NastIssue(IssueCodes.UNEXPECTED_EXCEPTION), new_issue)
        self.assertEqual({'blah': 'blubb', 'nameserver': str({'ns1.test.dummy': ["12345", "xd.fg:fg::0"]})}, new_issue.get_params())

    def test_check_is_reachable_tcp_with_unexpected_exception(self):
        new_issue = stage2.check_is_reachable(Exception(), {'blah': 'blubb', 'nameserver': {'ns1.test.dummy': ["12345", "xd.fg:fg::0"]}}, 'tcp')
        self.assertEqual(NastIssue(IssueCodes.UNEXPECTED_EXCEPTION, IssueSeverity.WARNING), new_issue)
        self.assertEqual({'blah': 'blubb', 'nameserver': str({'ns1.test.dummy': ["12345", "xd.fg:fg::0"]})}, new_issue.get_params())

    def test_check_a_aaaa_for_nameserver_with_exceptions_ip_is_glue(self):
        query_result = {
            'ns1.blah.de': {
                '172.31.1.1': {
                    'A': dns.exception.Timeout(),
                    'AAAA': ConnectionRefusedError()
                }
            }
        }
        issue_list = stage2.check_a_aaaa_for_nameserver(query_result, 'blah.de')
        self.assertEqual(0, len(issue_list))

    def test_check_a_aaaa_for_nameserver_with_exceptions_ip_is_not_glue(self):
        query_result = {
            'ns1.blubb.de': {
                '172.31.1.1': {
                    'A': dns.exception.Timeout(),
                    'AAAA': ConnectionRefusedError()
                }
            }
        }
        issue_list = stage2.check_a_aaaa_for_nameserver(query_result, 'blah.de')
        self.assertEqual(0, len(issue_list))

    def test_check_cname(self):
        query = dns.message.make_query(dns.name.from_text('nic.nast'), dns.rdatatype.SOA,
                                       dns.rdataclass.IN)
        answer = dns.message.make_response(query)
        answer.flags = answer.flags | dns.flags.AA
        answer.index = None

        answer.answer.append(
            dns.rrset.from_text(dns.name.from_text('nic.nast'), 86400, dns.rdataclass.IN,
                                dns.rdatatype.SOA, 'ns1.denic.nast. its.denic.nast. 2021051001 '
                                '10800 1800 3600000 1800'))

        query_result = {
            'ns1.nic.nast': {
                '172.31.1.1': {
                    'SOA': {
                        'udp': answer
                    }
                }
            }
        }
        new_issues = stage2.check_cname(query_result, 'nic.nast')
        self.assertEqual([], new_issues)

    def test_check_cname_with_cname(self):
        query = dns.message.make_query(dns.name.from_text('nic.nast'), dns.rdatatype.SOA,
                                       dns.rdataclass.IN)
        answer = dns.message.make_response(query)
        answer.flags = answer.flags | dns.flags.AA
        answer.index = None

        answer.answer.append(
            dns.rrset.from_text(dns.name.from_text('nic.nast'), 86400, dns.rdataclass.IN,
                                dns.rdatatype.CNAME, 'soa.nic.nast.'))

        answer.answer.append(
            dns.rrset.from_text(dns.name.from_text('soa.nic.nast'), 86400, dns.rdataclass.IN,
                                dns.rdatatype.SOA, 'ns1.denic.nast. its.denic.nast. 2021051001 '
                                                   '10800 1800 3600000 1800'))

        # print(answer)
        query_result = {
            'ns1.nic.nast': {
                '172.31.1.1': {
                    'SOA': {
                        'udp': answer
                    }
                }
            }
        }
        new_issues = stage2.check_cname(query_result, 'nic.nast')

        self.assertEqual([NastIssue(IssueCodes.SOA_RECORD_RESPONSE_MUST_BE_DIRECT)], new_issues)
        self.assertEqual({
            'nameserver': 'ns1.nic.nast',
            'ip': '172.31.1.1',
            'recordset': 'nic.nast. 86400 IN CNAME soa.nic.nast.'
        }, new_issues[0].get_params())
