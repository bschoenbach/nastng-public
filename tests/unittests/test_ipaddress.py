import unittest
import ipaddress


class IPAddressTest(unittest.TestCase):
    def test_ipv4_normal(self):
        result = ipaddress.ip_address("192.168.0.1")
        self.assertIsInstance(result, ipaddress.IPv4Address)
        self.assertEqual(4, result.version)
        self.assertEqual("192.168.0.1", str(result))

    def test_ipv4_compressed(self):
        self.assertRaises(ValueError, ipaddress.ip_address, "194.53")

    def test_ipv4_compressed_2(self):
        self.assertRaises(ipaddress.AddressValueError, ipaddress.IPv4Address, "194.53")

    def test_ipv4_octal(self):
        pass
        #result = ipaddress.ip_address("194.1.2.065")
        #self.assertEqual("194.1.2.65", str(result))
        # print(str(adr))
        #self.assertRaises(ValueError, ipaddress.ip_address, "194.1.2.065")

    def test_ipv4_equal(self):
        addr1 = ipaddress.ip_address("194.1.2.65")
        addr2 = ipaddress.ip_address("194.1.2.65")
        self.assertTrue(addr1 == addr2)
        self.assertEqual(addr1, addr2)

    def test_ipv4_as_ipv6_bytes(self):
        addr1 = ipaddress.ip_address("::FFFF:10.113.46.5")
        addr2 = ipaddress.ip_address("10.113.46.5")
        self.assertTrue(addr1.ipv4_mapped)
        self.assertEqual(addr1.ipv4_mapped, addr2)
        self.assertEqual(6, addr1.version)
        self.assertEqual("10.113.46.5", str(addr1.ipv4_mapped))

    def test_ipv4_as_ipv6_hex(self):
        addr1 = ipaddress.ip_address("::FFFF:0A71:2E05")
        addr2 = ipaddress.ip_address("10.113.46.5")
        self.assertTrue(addr1.ipv4_mapped)
        self.assertEqual(6, addr1.version)
        self.assertEqual("10.113.46.5", str(addr1.ipv4_mapped))
        self.assertEqual(addr1.ipv4_mapped, addr2)

    def test_int_as_ipv4_in_string(self):
        self.assertRaises(ValueError, ipaddress.ip_address, "2130706433")

    def test_int_as_ipv4(self):
        result = ipaddress.ip_address(2130706433)
        self.assertEqual(4, result.version)
        self.assertEqual("127.0.0.1", str(result))
