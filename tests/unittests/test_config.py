import os
import unittest
from nast.config import Config


def clear_env():
    if "LOG_ENABLED" in os.environ:
        del os.environ["LOG_ENABLED"]
    if "LOG_LEVEL" in os.environ:
        del os.environ["LOG_LEVEL"]
    if "LOG_FILE_PATH" in os.environ:
        del os.environ["LOG_FILE_PATH"]
    if "NAST_HOSTNAME" in os.environ:
        del os.environ["NAST_HOSTNAME"]
    if "NAST_CORS_ALLOW_ORIGINS" in os.environ:
        del os.environ["NAST_CORS_ALLOW_ORIGINS"]
    if "NAST_PORT" in os.environ:
        del os.environ["NAST_PORT"]
    if "NAST_RESOLVER" in os.environ:
        del os.environ["NAST_RESOLVER"]
    if "NAST_CONF" in os.environ:
        del os.environ["NAST_CONF"]
    if "NAST_TESTMODE" in os.environ:
        del os.environ["NAST_TESTMODE"]
    if "NAST_TIMEOUT" in os.environ:
        del os.environ["NAST_TIMEOUT"]


class ConfigTest(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):
        clear_env()

    def test_defaults(self):
        clear_env()
        config = Config()
        self.assertEqual(True, config.log_enabled)
        self.assertEqual("INFO", config.log_level)
        self.assertEqual(None, config.log_file_path)
        self.assertEqual("localhost", config.hostname)
        self.assertCountEqual(["http://localhost:3000","http://localhost:8080","http://172.31.0.5:8080", "https://nast-webui-dev.cloud.denic.de", "http://nast-webui-dev.cloud.denic.de"], config.cors_allow_origins)
        self.assertEqual(6007, config.port)
        self.assertEqual("172.31.0.2", config.resolver)
        self.assertEqual(2, config.timeout)
        self.assertEqual("conf", config.conf_path)
        self.assertEqual(False, config.testmode)
        self.assertEqual(150, config.workers)

    def test_update_from_environment(self):
        os.environ["LOG_ENABLED"] = "False"
        os.environ["LOG_LEVEL"] = "DEBUG"
        os.environ["LOG_FILE_PATH"] = "nast.log"
        os.environ["NAST_TESTMODE"] = "0"
        os.environ["NAST_HOSTNAME"] = "hostname"
        os.environ["NAST_CORS_ALLOW_ORIGINS"] = "http://test-corsorigin-1:8080,http://test-corsorigin-2:8080"
        os.environ["NAST_PORT"] = "1234"
        os.environ["NAST_RESOLVER"] = "172.31.0.3"
        os.environ["NAST_CONF"] = "/somewhere/else"
        os.environ["NAST_TIMEOUT"] = "1.2"
        os.environ["NAST_QUERY_WORKERS"] = "50"
        
        config = Config()
        self.assertEqual(False, config.log_enabled)
        self.assertEqual("DEBUG", config.log_level)
        self.assertEqual("nast.log", config.log_file_path)
        self.assertEqual("hostname", config.hostname)
        self.assertCountEqual(["http://test-corsorigin-1:8080,http://test-corsorigin-2:8080"], config.cors_allow_origins)
        self.assertEqual(1234, config.port)
        self.assertEqual("172.31.0.3", config.resolver)
        self.assertEqual(1.2, config.timeout)
        self.assertEqual("/somewhere/else", config.conf_path)
        self.assertEqual(False, config.testmode)
        self.assertEqual(50, config.workers)

    def test_etc_exists(self):
        clear_env()
        try:
            os.mkdir("/etc/nast")
            config = Config()
            os.rmdir("/etc/nast")
            self.assertEqual("/etc/nast", config.conf_path)
        except BaseException:
            pass
