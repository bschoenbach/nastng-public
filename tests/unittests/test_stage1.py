import unittest
from nast.config import Config
from nast import stage1
from nast.issues import NastIssue, IssueCodes, IssueSeverity


class Stage1Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_check_redundancy(self):
        full_resolved_nameserver_dict = {
            'ns1.nic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
            'ns2.nic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
        }
        new_issue = stage1.check_redundancy(full_resolved_nameserver_dict)
        self.assertEqual(None, new_issue)

        nameserver = {'ns1.denic.nast': ['172.31.1.1'],
                      'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2']}
        issue = stage1.check_redundancy(nameserver)
        self.assertEqual(None, issue)

    def test_check_redundancy_2x_same_ips(self):
        full_resolved_nameserver_dict = {
            'ns1.nic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
            'ns2.nic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
            'ns3.nic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
        }
        new_issue = stage1.check_redundancy(full_resolved_nameserver_dict)
        self.assertEqual(None, new_issue)

    def test_check_redundancy_1x_ipv4_1x_ipv6(self):
        full_resolved_nameserver_dict = {
            'ns1.nic.nast': ['172.31.1.1'],
            'ns2.nic.nast': ['fd00:10:10::2:2'],
        }
        new_issue = stage1.check_redundancy(full_resolved_nameserver_dict)
        self.assertEqual(None, new_issue)

    def test_check_redundancy_2x_ipv4(self):
        full_resolved_nameserver_dict = {
            'ns1.nic.nast': ['172.31.1.1'],
            'ns2.nic.nast': ['172.31.1.2'],
        }
        new_issue = stage1.check_redundancy(full_resolved_nameserver_dict)
        self.assertEqual(None, new_issue)

    def test_check_redundancy_no_ipv4(self):
        full_resolved_nameserver_dict = {
            'ns1.nic.nast': ['fd00:10:10::1:1'],
            'ns2.nic.nast': ['fd00:10:10::2:2'],
        }
        new_issue = stage1.check_redundancy(full_resolved_nameserver_dict)
        self.assertEqual(NastIssue(IssueCodes.INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IPV4_ADDRESSES),
                         new_issue)
        self.assertEqual({
            'expected': "1",
            'found': "0"
        }, new_issue.get_params())

    def test_check_redundancy_same_ips(self):
        full_resolved_nameserver_dict = {
            'ns1.nic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
            'ns2.nic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
        }
        new_issue = stage1.check_redundancy(full_resolved_nameserver_dict)
        self.assertEqual(NastIssue(IssueCodes.INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IP_ADDRESSES),
                         new_issue)
        self.assertEqual({
            'expected': "1",
            'found': "0"
        }, new_issue.get_params())

    def test_check_redundancy_same_ips_2x_ipv4(self):
        full_resolved_nameserver_dict = {
            'ns1.nic.nast': ['172.31.1.1'],
            'ns2.nic.nast': ['172.31.1.1'],
        }
        new_issue = stage1.check_redundancy(full_resolved_nameserver_dict)
        self.assertEqual(NastIssue(IssueCodes.INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IP_ADDRESSES),
                         new_issue)
        self.assertEqual({
            'expected': "1",
            'found': "0"
        }, new_issue.get_params())

    def test_check_redundancy_1x_ipv4_2x_ipv6(self):
        nameserver = {'ns1.denic.nast': ['fd00:10:10::1:1'],
                      'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2']}
        issue = stage1.check_redundancy(nameserver)
        self.assertEqual(None, issue)

    def test_check_redundancy_2x_same_ipv4_1x_ipv6(self):
        nameserver = {'ns1.denic.nast': ['192.168.0.1'],
                      'ns2.denic.nast': ['192.168.0.1', 'fd00:10:10::2:2']}

        issue = stage1.check_redundancy(nameserver)
        self.assertEqual(NastIssue(IssueCodes.INSUFFICIENT_DIVERSITY_OF_NAMESERVERS_IP_ADDRESSES), issue)
