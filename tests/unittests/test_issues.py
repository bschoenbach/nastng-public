import unittest
from nast.config import Config
from nast.issues import (IssueSeverity,
                         IssueCodes,
                         NastIssue,
                         NAST_ISSUES,
                         contains_errors,
                         aggregate_issues)


class IssuesTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_contains_errors(self):
        issues_list = list()
        self.assertFalse(contains_errors(issues_list))
        issues_list.append(NastIssue(IssueCodes.UNEXPECTED_EXCEPTION,
                                     IssueSeverity.INFO))
        issues_list.append(NastIssue(IssueCodes.UNEXPECTED_EXCEPTION,
                                     IssueSeverity.WARNING, {}))
        issues_list.append(NastIssue(IssueCodes.UNEXPECTED_EXCEPTION,
                                     IssueSeverity.WARNING, {}))

        self.assertFalse(contains_errors(issues_list))
        issues_list.append(NastIssue(IssueCodes.UNEXPECTED_EXCEPTION,
                                     IssueSeverity.ERROR, {}))
        self.assertTrue(contains_errors(issues_list))

    def test_NastIssue(self):
        self.assertRaises(ValueError, NastIssue, IssueCodes.TIMEOUT, 999)
        self.assertRaises(ValueError, NastIssue, 13, IssueSeverity.INFO)
        params = {"key1": "value1", "key2": "values"}
        issue = NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR, params)
        self.assertEqual(901, issue.get_code())
        self.assertEqual(3, issue.get_severity())
        self.assertEqual(params, issue.get_params())
        self.assertTrue(issue.is_error())
        self.assertEqual("Unexpected RCODE", issue.get_name())
        self.assertEqual(
            "NastIssue(code=901, name=Unexpected RCODE, severity=error, params={'key1': 'value1', 'key2': 'values'})", str(issue))

    def test_NastIssue_with_unknown_code(self):
        issue = NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR)
        issue.code = 7777
        self.assertEqual("Unknown Errorcode [7777]", issue.get_name())

    def test_NastIssue_Compare(self):
        issue1 = NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR)
        issue2 = NastIssue(IssueCodes.TIMEOUT, IssueSeverity.ERROR)
        issue3 = NastIssue(IssueCodes.UNEXPECTED_EXCEPTION, IssueSeverity.ERROR)
        issue4 = NastIssue(IssueCodes.TIMEOUT, IssueSeverity.ERROR)

        self.assertEqual(issue2, issue4)
        self.assertNotEqual(issue1, issue2)
        self.assertGreater(issue2, issue1)
        self.assertGreaterEqual(issue2, issue1)
        self.assertGreaterEqual(issue2, issue4)
        self.assertLess(issue1, issue2)
        self.assertLessEqual(issue1, issue2)
        self.assertLessEqual(issue2, issue4)

    def test_get_hash_for_aggregation(self):
        issue = NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR)
        self.assertEqual("007edc6bb8dc1197944661f114c9fed461087504", issue.get_hash_for_aggregation())
        issue = NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR, {'blah': 'blubb'})
        self.assertEqual("dea65b8a58f0ce97b04090c858bbbf61414d5471", issue.get_hash_for_aggregation())

        issue = NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR)
        self.assertEqual("3fb402cc504c9c6ce4fb98c31ab5c4e984bca3e4", issue.get_hash_for_aggregation())

        issue = NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'blah': 'blubb'})
        self.assertEqual("3fb402cc504c9c6ce4fb98c31ab5c4e984bca3e4", issue.get_hash_for_aggregation())

    def test_aggregate_issues_equal(self):
        issue_list = []
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR))
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR))
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR))
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR))

        aggregated_list = aggregate_issues(issue_list)
        self.assertEqual(1, len(aggregated_list))

    def test_aggregate_issues_non_equal(self):
        issue_list = []
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR))
        issue_list.append(NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR))
        issue_list.append(NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_MODULO_INVALID_LENGTH, IssueSeverity.ERROR))
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.WARNING))

        aggregated_list = aggregate_issues(issue_list)
        self.assertEqual(4, len(aggregated_list))

    def test_aggregate_issues_with_parameter(self):
        issue_list = []
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR, {'ip': '1.2.3.4'}))
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR, {'ip': '1.2.3.5'}))
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR, {'ip': '1.2.3.6'}))
        issue_list.append(NastIssue(IssueCodes.UNEXPECTED_RCODE, IssueSeverity.ERROR, {'ip': '1.2.3.7'}))

        aggregated_list = aggregate_issues(issue_list)
        self.assertEqual(4, len(aggregated_list))

    def test_aggregate_special_issues_with_parameter(self):
        issue_list = []
        issue_list.append(NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'ip': '1.2.3.4'}))
        issue_list.append(NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'ip': '1.2.3.5'}))
        issue_list.append(NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'ip': '1.2.3.6'}))
        issue_list.append(NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'ip': '1.2.3.7'}))

        aggregated_list = aggregate_issues(issue_list)
        self.assertEqual(1, len(aggregated_list))

        issue_list = []
        issue_list.append(NastIssue(IssueCodes.REFRESH_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'ip': '1.2.3.4'}))
        issue_list.append(NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'ip': '1.2.3.5'}))
        issue_list.append(NastIssue(IssueCodes.RETRY_VALUE_OUT_OF_RANGE_REFRESH, IssueSeverity.ERROR, {'ip': '1.2.3.6'}))
        issue_list.append(NastIssue(IssueCodes.EXPIRE_VALUE_OUT_OF_RANGE, IssueSeverity.ERROR, {'ip': '1.2.3.7'}))

        aggregated_list = aggregate_issues(issue_list)
        self.assertEqual(4, len(aggregated_list))


# end
