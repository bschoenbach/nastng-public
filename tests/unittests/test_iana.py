import os
import unittest
import json
import ipaddress
#import netaddr
from nast.config import Config
from nast.iana import IanaIPv6Addresses, parse_special_registry, \
    parse_address_assignments, InvalidIanaFileError
from nast.issues import IssueSeverity, IssueCodes, NastIssue


def show_network(network):
    ip_network = ipaddress.ip_network(network)
    print("\n===========================================\n%s: First: %s, Last: %s\n" %
          (str(ip_network), str(ip_network[0]), str(ip_network[-1])))


class IanaTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        os.environ["NAST_TESTMODE"] = "True"
        cls.config = Config()
        cls.iana = IanaIPv6Addresses()
        cls.maxDiff = None

    def test_check_no_issues(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'algorithm': 8,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }

        issue_list = self.iana.check_ipaddresses_from_params(params)
        self.assertEqual([], issue_list)

    def test_check_no_nameserver(self):
        self.assertEqual([], self.iana.check_ipaddresses_from_params({}))

    def test_check_not_assigned(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', '1234:10:10::1:1'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'algorithm': 8,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        result = self.iana.check_ipaddresses_from_params(params)
        self.assertEqual([NastIssue(IssueCodes.IPV6_ADDRESS_NOT_ALLOCATED,
                                    IssueSeverity.ERROR)], result)
        self.assertEqual({"nameserver": "ns1.denic.nast",
                          "ip": "1234:10:10::1:1"}, result[0].get_params())

    def test_check_not_routable_by_special_purpose(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', '2001::2'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'algorithm': 8,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        result = self.iana.check_ipaddresses_from_params(params)
        self.assertEqual([NastIssue(IssueCodes.IPV6_ADDRESS_NOT_ROUTABLE,
                                    IssueSeverity.ERROR)], result)
        self.assertEqual({"nameserver": "ns1.denic.nast",
                          "ip": "2001::2"}, result[0].get_params())

    def test_check_invalid_ipv6_address(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'xyz::abc:123'],
                                 'ns4.denic.net': []},
                  'dnskey': [{'flags': 257, 'algorithm': 8,
                              'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='
                              }]
                  }
        result = self.iana.check_ipaddresses_from_params(params)
        self.assertEqual([NastIssue(IssueCodes.INVALID_IP_ADDRESS,
                                    IssueSeverity.ERROR)], result)
        self.assertEqual({"nameserver": "ns1.denic.nast",
                          "ip": "xyz::abc:123"}, result[0].get_params())

    def test_assigned(self):
        # show_network("2001:5000::/20")
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("2a01:4f8:202:109a::2")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("fd00:10:10::1:1")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("fd00:10:10::0")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("fd00:10:10:ffff:ffff:ffff:ffff:ffff")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("2400::")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("240f:ffff:ffff:ffff:ffff:ffff:ffff:ffff")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("2001:5000::")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("2001:5fff:ffff:ffff:ffff:ffff:ffff:ffff")))

        # RESERVED
        self.assertFalse(self.iana.check_assigned(ipaddress.ip_address("2d00::")))
        self.assertFalse(self.iana.check_assigned(ipaddress.ip_address("2dff:ffff:ffff:ffff:ffff:ffff:ffff:ffff")))

    def test_special_purpose(self):
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2a01:4f8:202:109a::2")))
        # show_network("::ffff:0:0/96")
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("fd00:10:10::1:1")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001::2")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001:1ff:ffff:ffff:ffff:ffff:ffff:ffff")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2001:1::1")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001:2::")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001:2:0:ffff:ffff:ffff:ffff:ffff")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2001:3::")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2001:3:ffff:ffff:ffff:ffff:ffff:ffff")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2001:4:112::")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2001:4:112:ffff:ffff:ffff:ffff:ffff")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001:10::")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001:1f:ffff:ffff:ffff:ffff:ffff:ffff")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001:db8::")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2001:db8:ffff:ffff:ffff:ffff:ffff:ffff")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2002::")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("2002:ffff:ffff:ffff:ffff:ffff:ffff:ffff")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2003::")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2003:ffff:ffff:ffff:ffff:ffff:ffff:ffff")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("64:ff9b::")))
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("64:ff9b::ffff:ffff")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("64:ff9b:1::")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("64:ff9b:1:ffff:ffff:ffff:ffff:ffff")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("::ffff:0:0")))
        self.assertFalse(self.iana.check_special_purpose(ipaddress.ip_address("::ffff:ffff:ffff")))

    def test_old_saft_tests(self):
        self.assertTrue(self.iana.check_special_purpose(ipaddress.ip_address("2a02:568:201:314::1:5")))
        self.assertTrue(self.iana.check_assigned(ipaddress.ip_address("2a02:568:201:314::1:5")))

    def test_no_testmode(self):
        if "NAST_TESTMODE" in os.environ:
            del(os.environ["NAST_TESTMODE"])
        iana = IanaIPv6Addresses()
        os.environ["NAST_TESTMODE"] = "True"
        self.assertFalse(iana.check_assigned(ipaddress.ip_address("fd00:10:10::1:1")))
        self.assertFalse(iana.check_special_purpose(ipaddress.ip_address("fd00:10:10::1:1")))

    def test_invalid_address_assignments(self):
        self.assertRaises(InvalidIanaFileError, parse_address_assignments, "tests/testdata/invalid_ipv6-unicast-address-assignments.xml")
        self.assertRaises(InvalidIanaFileError, parse_address_assignments, "tests/testdata/no_xml.xml")

    def test_invalid_special_registry(self):
        self.assertRaises(InvalidIanaFileError, parse_special_registry, "tests/testdata/invalid_iana-ipv6-special-registry.xml")
        self.assertRaises(InvalidIanaFileError, parse_special_registry, "tests/testdata/no_xml.xml")

    def test_check_ipaddresses_from_aaaa_rr(self):
        result = self.iana.check_ipaddresses_from_aaaa_rr({})
        self.assertEqual(0, len(result))
