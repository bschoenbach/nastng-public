import base64
import copy
from inspect import signature
import unittest

from dns.rdatatype import DNSKEY
from nast.config import Config
from nast import stage3
from nast.issues import NAST_ISSUES, NastIssue, IssueCodes, IssueSeverity
import dns.exception
import dns.rdtypes.ANY.DNSKEY
import dns.rdtypes.ANY.RRSIG
import dns.message


class Stage3Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_check_params_flags(self):
        params = {
            'dnskey': [
                # FLAG valid + warning
                {'flags':'256', 'algorithm':5, 'key':''},
                # FLAGS valid 
                {'flags':257, 'algorithm':5, 'key':''},
                # FLAGS unknown
                {'flags':"xxxx", 'algorithm':5, 'key':''},
                # FLAGS invalid REVOKE set
                {'flags':'385', 'algorithm':5, 'key':''},
                # FLAGS invalid ZONE unset
                {'flags':1, 'algorithm':5, 'key':''}
            ]
        }    

        new_issues = stage3.check_params_flags(params['dnskey'])
        expected_issues = [ NastIssue(IssueCodes.DNSKEY_RR_SEP_FLAG_SHOULD_BE_SET, IssueSeverity.WARNING),
                            NastIssue(IssueCodes.DNSKEY_RR_UNKNOWN_FLAGS, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_REVOKE_FLAG_MUST_NOT_BE_SET, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_ZONE_FLAG_MUST_BE_SET, IssueSeverity.ERROR)
                        ]
        expected_params = list()
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())
            expected_params.append(None)

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_params_protocol(self):
        params = {
            'dnskey': [
                # PROTOCOL valid
                {'flags':256, 'protocol': 3, 'algorithm':5, 'key':''},
                # PROTCOL invalid 
                {'flags':257, 'protocol': 4, 'algorithm':5, 'key':''},
                {'flags':257, 'protocol': 'dfsdfsdf', 'algorithm':5, 'key':''}
            ]
        }

        new_issues = stage3.check_params_protocol(params['dnskey'])
        expected_issues = [ NastIssue(IssueCodes.DNSKEY_RR_PROTOCOL_INVALID, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_PROTOCOL_INVALID, IssueSeverity.ERROR)
                          ]
        
        expected_params = [ {'expected': '3', 'found': '4'},
                            {'expected': '3', 'found': 'dfsdfsdf'}
                        ]

        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)


    def test_check_params_uniqueness(self):
        params = {
            'dnskey': [
                # Key unique
                {'flags':256, 'algorithm':5, 'key':'AQPSKmynfzW4ky Bv015MUG2DeIQ3Cbl+BBZH4b/0PY 1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                # Key unique key
                {'flags':256, 'algorithm':5, 'key':'AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4b=='},
                # Key duplicate to 0
                {'flags':256, 'algorithm':5, 'key':'AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                # key unique flags
                {'flags':257, 'algorithm':5, 'key':'AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                # key unique algorithm
                {'flags':257, 'algorithm':7, 'key':'AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                 # key unique algorithm BUT max number of 5 keys exceeded
                {'flags':257, 'algorithm':8, 'key':'AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='}
            ]
        }

        # check if all keys are unique among themeselves + max number of keys is exceeded
        new_issues = stage3.check_params_uniqueness(params['dnskey'])
        expected_issues = [ NastIssue(IssueCodes.DNSKEY_RR_MAX_NUMBER_EXCEEDED, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_DUPLICATE, IssueSeverity.ERROR)
                        ]
        expected_params = [ None, {'key': '256 5 AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='}
                        ]
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())
        
        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_params_algorithm(self):
        params = {
            'dnskey': [
                # accepted IANA algos
                {'flags':256, 'algorithm':3, 'key':''},
                {'flags':256, 'algorithm':5, 'key':''},
                {'flags':256, 'algorithm':6, 'key':''},
                {'flags':257, 'algorithm':7, 'key':''},
                {'flags':257, 'algorithm':8, 'key':''},
                {'flags':257, 'algorithm':10, 'key':''},
                {'flags':257, 'algorithm':12, 'key':''},
                {'flags':257, 'algorithm':13, 'key':''},
                {'flags':257, 'algorithm':14, 'key':''},
                # SUPPORT OF NEW ED25519 (15) AND ED448 (16) POSTPONED UNTIL RELEASE IN RRI
                #{'flags':257, 'algorithm':15, 'key':''},
                #{'flags':257, 'algorithm':16, 'key':''},
                # invalid algos
                {'flags':257, 'algorithm':1, 'key':''},
                {'flags':257, 'algorithm':2, 'key':''},
                {'flags':257, 'algorithm':17, 'key':''},
                {'flags':257, 'algorithm':-1, 'key':''}
            ]
        }

        # check if all allowed algos are accepted
        new_issues = stage3.check_params_algorithm(params['dnskey'])
        expected_issues = [ NastIssue(IssueCodes.DNSKEY_RR_ALGORITHM_NOT_SUPPORTED, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_ALGORITHM_NOT_SUPPORTED, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_ALGORITHM_NOT_SUPPORTED, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_ALGORITHM_NOT_SUPPORTED, IssueSeverity.ERROR)
                        ]
        # SUPPORT OF NEW ED25519 (15) AND ED448 (16) POSTPONED UNTIL RELEASE IN RRI
        expected_params = [ {'expected': '3,5,6,7,8,10,12,13,14', 'found': '1'},#{'expected': '3,5,6,7,8,10,12,13,14,15,16', 'found': '1'},
                            {'expected': '3,5,6,7,8,10,12,13,14', 'found': '2'},#{'expected': '3,5,6,7,8,10,12,13,14,15,16', 'found': '2'},
                            {'expected': '3,5,6,7,8,10,12,13,14', 'found': '17'},#{'expected': '3,5,6,7,8,10,12,13,14,15,16', 'found': '17'}
                            {'expected': '3,5,6,7,8,10,12,13,14', 'found': '-1'}#{'expected': '3,5,6,7,8,10,12,13,14,15,16', 'found': '-1'}
                        ]
    
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_params_public_key(self):
        params = {
            'dnskey': [
                # INVALID public keys
                # [1] invalid base64 string
                {'flags':256, 'algorithm':5, 'key':'QPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                {'flags':256, 'algorithm':5, 'key':''},
                # [2] RSA invalid modulo length < 512 bit
                { 'flags':256, 'algorithm':8, 'key':'AwEAAcWCzDctAg/MMQERulpLCtiuq1hkHLG/XMJNgcp7xDAnA4ORTIw0'},
                # [3] RSA invalid modulo length > 4096 bit
                {'flags':257, 'algorithm':8, 'key':'AwEAAcWCzDctAg/MMQERulpLCtiuq1hkHLG/XMJNgcp7xDAnA4ORTIw0DCpygQ4x74gt3W+gmQfGWfBS0g/ts8hGwUFZL7YiUp822M3C8fQzxSel6q0EmhKWUP3SGzSa1L6+3YkIxqvrmr/7HDQkEwjBE7rg6oOMViGyKKHnCpygQ4x74gt3W+gmQfGWfBS0g/ts8hGwUFZL7YiUp822M3C8fQzxSel6q0EmhKWUP3SGzSa1L6+3YkIxqvrmr/7HDQkEwjBE7rg6oOMViGyKKHnCpygQ4x74gt3W+gmQfGWfBS0g/ts8hGwUFZL7YiUp822M3C8fQzxSel6q0EmhKWUP3SGzSa1L6+3YkIxqvrmr/7HDQkEwjBE7rg6oOMViGyKKHnCpygQ4x74gt3W+gmQfGWfBS0g/ts8hGwUFZL7YiUp822M3C8fQzxSel6q0EmhKWUP3SGzSa1L6+3YkIxqvrmr/7HDQkEwjBE7rg6oOMViGyKKHnCpygQ4x74gt3W+gmQfGWfBS0g/ts8hGwUFZL7YiUp822M3C8fQzxSel6q0EmhKWUP3SGzSa1L6+3YkIxqvrmr/7HDQkEwjBE7rg6oOMViGyKKHnDCpygQ4x74gt3W+gmQfGWfBS0g/ts8hGwUFZL7YiUp822M3C8fQzxSel6q0EmhKWUP3SGzSa1L6+3YkIxqvrmr/7HDQkEwjBE7rg6oOMViGyKKHnDCpygQ4x74gt3W+gmQfGWfBS0g/ts8hGwUFZL7YiUp822M3C8fQzxSel6q0EmhKWUP3SGzSa1L6+3YkIxqvrmr/7HDQkEwjBE7rg6oOMViGyKKHnPsn2Gbf9'},
                # [4] RSA invalid exp length 136 > 128 bit
                {'flags':257, 'algorithm':5, 'key':'EQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                # [5] RSA invalid exp length of RSA key 136 > 128 bit and modulo length -1040 < 512 bit
                {'flags':257, 'algorithm':5, 'key':'AAEBKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                # [6] DSA invalid length 231 != 309 (expected)
                {'flags':257, 'algorithm':3, 'key':'BOPdJjdc/ZQWCVA/ONz6LjvugMnB2KKL3F1D2i9GdrpircWRKS2DfRn5KiMM2HQXBHv0ZdkFs/tmjg7rYxrN+bzBNrlwfU5RMjioi67PthD07EHbZjwoZ5sKC2BZ/M596hygfx5JAvbIWBQVF+ztiuCnWCkbGvVXwsmE+odINCur+o+EjA9hF06LqTviUJKqTxisQO5OHM/0ufNenzIbijJPTXbUcF3vW+CMlX+AUPLSag7YnhWaEu7BLCKfg3vJVw9mtaN2W3oWPRdebGUf/QfyVKXoWD6zDLByCZh4wKvpcwgAsel4'},
                # [7] DSA invalid length 231 != 309 (expected) + parameter T out of range 9
                {'flags':257, 'algorithm':3, 'key':'CePdJjdc/ZQWCVA/ONz6LjvugMnB2KKL3F1D2i9GdrpircWRKS2DfRn5KiMM2HQXBHv0ZdkFs/tmjg7rYxrN+bzBNrlwfU5RMjioi67PthD07EHbZjwoZ5sKC2BZ/M596hygfx5JAvbIWBQVF+ztiuCnWCkbGvVXwsmE+odINCur+o+EjA9hF06LqTviUJKqTxisQO5OHM/0ufNenzIbijJPTXbUcF3vW+CMlX+AUPLSag7YnhWaEu7BLCKfg3vJVw9mtaN2W3oWPRdebGUf/QfyVKXoWD6zDLByCZh4wKvpcwgAsel4bO5LVe7s8qstSxqrwzmvaZ5XYOMZFbN7CXtutiswAkb0pkehIYime6IRkDwWDG+14H5yriRuCDK3m7GvwxMo+ggV0k3Po9LD5wWSIi1N'},
                # [8] GOST invalid length
                {'flags':257, 'algorithm':12, 'key':'aRS/DcPWGQj2wVJydT8EcAVoC0kXn5pDVm2IMvDDPXeD32dsSKcmq8KNVzigjL4OXZTV+t/6w4X1gpNrZiC0'},
                # [9] ECDSA invalid length 4096 != 512
                {'flags':257, 'algorithm':13, 'key':'vSzSbE2Zj3Sh5Ih8wkslXd3rJKZe6kuUygfwxw1dZx7K/WWYNGt+G9cPPCoT6x2L4K0OaLRyI2VPpWj5AM4='},
                # [10] ECDSA invalid length 760 != 768
                {'flags':257, 'algorithm':14, 'key':'x4J4ZD1HCt1/byrkCrYKuXqUqJt7yLNF0am4INabO7My5mBcRAC+Yl9ioCOhnWhYXQDrOOW5gy/5v/ENDX3EMi6fO5rrsHZ341AtelMW2pTmfXJgwtbSV18gnOFDE4E='},
                # [11] ED invalid length 248 != 256
                {'flags':257, 'algorithm':15, 'key':'M5qY5/ULkHpBtN+x/sW5DCNMDrxRz0LOjRePKwSFiA=='},
                # [12] ED invalid length 448 != 456
                {'flags':257, 'algorithm':16, 'key':'tyNAEl29DQjbV4yRMx9pqmZS0zRydtSOTWZX5ZVw/5o7p4ddpJVLIozg4Zc+AkAEHGJQE+v1XKo='},
                ###################
                # VALID public keys
                # RSA
                {'flags':257, 'algorithm':5, 'key':'AQPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                {'flags': 257, 'algorithm': 8, 'key': 'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='},
                # DSA
                {'flags':257, 'algorithm':3, 'key':'BOPdJjdc/ZQWCVA/ONz6LjvugMnB2KKL3F1D2i9GdrpircWRKS2DfRn5KiMM2HQXBHv0ZdkFs/tmjg7rYxrN+bzBNrlwfU5RMjioi67PthD07EHbZjwoZ5sKC2BZ/M596hygfx5JAvbIWBQVF+ztiuCnWCkbGvVXwsmE+odINCur+o+EjA9hF06LqTviUJKqTxisQO5OHM/0ufNenzIbijJPTXbUcF3vW+CMlX+AUPLSag7YnhWaEu7BLCKfg3vJVw9mtaN2W3oWPRdebGUf/QfyVKXoWD6zDLByCZh4wKvpcwgAsel4bO5LVe7s8qstSxqrwzmvaZ5XYOMZFbN7CXtutiswAkb0pkehIYime6IRkDwWDG+14H5yriRuCDK3m7GvwxMo+ggV0k3Po9LD5wWSIi1N'},
                # GOST
                {'flags':256, 'algorithm':12, 'key':'aRS/DcPWGQj2wVJydT8EcAVoC0kXn5pDVm2IMvDDPXeD32dsSKcmq8KNVzigjL4OXZTV+t/6w4X1gpNrZiC01g=='},
                # ECDSA
                {'flags':256, 'algorithm':13, 'key':'vSzSbE2Zj3Sh5Ih8wkslXd3rJKZe6kuUygfwxw1dZx7K/WWYNGt+G9cPPCoT6x2L4K0OaLRyI2VPpWj5AM7gUA=='},
                {'flags':256, 'algorithm':14, 'key':'x4J4ZD1HCt1/byrkCrYKuXqUqJt7yLNF0am4INabO7My5mBcRAC+Yl9ioCOhnWhYXQDrOOW5gy/5v/ENDX3EMi6fO5rrsHZ341AtelMW2pTmfXJgwtbSV18gnOFDE4Hu'},
                # ED
                {'flags':257, 'algorithm':15, 'key':'M5qY5/ULkHpBtN+x/sW5DCNMDrxRz0LOjRePKwSFiP8='},
                {'flags':257, 'algorithm':16, 'key':'tyNAEl29DQjbV4yRMx9pqmZS0zRydtSOTWZX5ZVw/5o7p4ddpJVLIozg4Zc+AkAEHGJQE+v1XKoA'}
            ]
        }

        # check if all public keys are encoded correctly
        new_issues = stage3.check_params_public_key(params['dnskey'])
        expected_issues = [ #[1]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_FORMAT_INVALID, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_FORMAT_INVALID, IssueSeverity.ERROR),
                            #[2]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_MODULO_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[3]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_MODULO_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[4]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_EXPONENT_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[5]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_MODULO_INVALID_LENGTH, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_RSA_EXPONENT_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[6]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_DSA_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[7]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_DSA_PARAMETER_T_OUT_OF_RANGE, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_DSA_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[8]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_GOST_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[9]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ECDSA_INVALID_LENGTH, IssueSeverity.ERROR),
                            #[10]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ECDSA_INVALID_LENGTH,  IssueSeverity.ERROR),
                            #[11]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ED_INVALID_LENGTH,  IssueSeverity.ERROR),
                            #[12]
                            NastIssue(IssueCodes.DNSKEY_RR_PUBLIC_KEY_ED_INVALID_LENGTH,  IssueSeverity.ERROR)
                        ]
        expected_params = [ #[1]
                            {'expected': 'Base64 String', 'found': 'QPSKmynfzW4kyBv015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8nokfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6RfoRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                            {'expected': 'Base64 String', 'found': ''},
                            #[2]
                            {'expected': 'modulo bit length in range [512,4096]', 'found': 'modulo bit length 304'},
                            #[3]
                            {'expected': 'modulo bit length in range [512,4096]', 'found': 'modulo bit length 5032'},
                            #[4]
                            {'expected': 'exponent bit length in range [8,128]', 'found': 'exponent bit length 136'},
                            #[5]
                            {'expected': 'modulo bit length in range [512,4096]', 'found': 'modulo bit length 0'},
                            {'expected': 'exponent bit length in range [8,128]', 'found': 'exponent bit length 2056'},
                            #[6]
                            {'expected': 'key byte length 309', 'found': 'key byte length 231'},
                            #[7]
                            {'expected': 'T parameter in range [0,8]', 'found': 'T parameter 9'},
                            {'expected': 'key byte length 429', 'found': 'key byte length 309'},
                            #[8]
                            {'expected': 'key bit length 512', 'found': 'key bit length 504'},
                            #[9]
                            {'expected': 'key bit length 512', 'found': 'key bit length 496'},
                            #[10]
                            {'expected': 'key bit length 768', 'found': 'key bit length 760'},
                            #[11]
                            {'expected': 'key bit length 256', 'found': 'key bit length 248'},
                            #[12]
                            {'expected': 'key bit length 456', 'found': 'key bit length 448'}
                        ]
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())
    
        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_rrset_identical_visible(self):
        params_nameservers = {
            'ns1.denic.nast' : ['172.31.1.1','fd00:10:10::1:1'],
            'ns2.denic.nast' : ['172.31.2.2','fd00:10:10::2:2'],
            'ns3.denic.nast' : ['172.31.3.3','fd00:10:10::3:3'],
            'ns4.denic.nast' : ['172.31.4.4']
        }
        params_dnskeys = {
            'dnskey': [
                # RSA
                {'flags':257, 'algorithm':5, 'key':'AQPSKmynfzW4kyBv 015MUG2DeIQ3Cbl+BBZH4b/0PY1kxkmvHjcZc8no kfzj31GajIQKY+5CptLr3buXA10hWqTkF7H6R foRqXQeogmMHfpftf6zMv1LyBUgia7za6ZEzOJBOztyvhjL742iU/TpPSEDhm2SNKLijfUppn1UaNvv4w=='},
                # DSA
                {'flags':257, 'algorithm':3, 'key':'BOPdJjdc/ZQWCVA/ONz6LjvugMnB2KKL3F1D2i9GdrpircWRKS2DfRn5KiMM2HQXBHv0ZdkFs/tmjg7rYxrN+bzBNrlwfU5RMjioi67PthD07EHbZjwoZ5sKC2BZ/M596hygfx5JAvbIWBQVF+ztiuCnWCkbGvVXwsmE+odINCur+o+EjA9hF06LqTviUJKqTxisQO5OHM/0ufNenzIbijJPTXbUcF3vW+CMlX+AUPLSag7YnhWaEu7BLCKfg3vJVw9mtaN2W3oWPRdebGUf/QfyVKXoWD6zDLByCZh4wKvpcwgAsel4bO5LVe7s8qstSxqrwzmvaZ5XYOMZFbN7CXtutiswAkb0pkehIYime6IRkDwWDG+14H5yriRuCDK3m7GvwxMo+ggV0k3Po9LD5wWSIi1N'},
                # GOST
                {'flags':256, 'algorithm':12, 'key':'aRS/DcPWGQj2wVJydT8EcAVoC0kXn5pDVm2IMvDDPXeD32dsSKcmq8KNVzigjL4OXZTV+t/6w4X1gpNrZiC01g=='},
                # ECDSA
                {'flags':256, 'algorithm':13, 'key':'vSzSbE2Zj3Sh5Ih8wkslXd3rJKZe6kuUygfwxw1dZx7K/WWYNGt+G9cPPCoT6x2L4K0OaLRyI2VPpWj5AM7gUA=='},
                # ED
                {'flags':257, 'algorithm':16, 'key':'tyNAEl29DQjbV4yRMx9pqmZS0zRydtSOTWZX5ZVw/5o7p4ddpJVLIozg4Zc+AkAEHGJQE+v1XKoA'}
            ]
        }
        query_dnskeys_identical = {
            'ns1.denic.nast' : {'172.31.1.1': params_dnskeys['dnskey'],
                                'fd00:10:10::1:1': params_dnskeys['dnskey']},
            'ns2.denic.nast' : {'172.31.2.2': params_dnskeys['dnskey'],
                                'fd00:10:10::2:2':params_dnskeys['dnskey']},
            'ns3.denic.nast' : {'172.31.3.3': params_dnskeys['dnskey'],
                                'fd00:10:10::3:3':params_dnskeys['dnskey']},
            'ns4.denic.nast' : {'172.31.4.4':params_dnskeys['dnskey'],
                                'fd00:10:10::4:4':params_dnskeys['dnskey']}
        }
        query_dnskeys_unidentical = {
            'ns1.denic.nast' : {'172.31.1.1': params_dnskeys['dnskey'][0:2],
                                'fd00:10:10::1:1': params_dnskeys['dnskey']},
            'ns2.denic.nast' : {'172.31.2.2': params_dnskeys['dnskey'],
                                'fd00:10:10::2:2': params_dnskeys['dnskey']},
            'ns3.denic.nast' : {'172.31.3.3': params_dnskeys['dnskey'][0:2],
                                'fd00:10:10::3:3':params_dnskeys['dnskey'][0:2]},
            'ns4.denic.nast' : {'172.31.4.4': params_dnskeys['dnskey'],
                                'fd00:10:10::4:4':params_dnskeys['dnskey']}
        }
        # check if dnskey rrsets are identical
        new_issues, visible_dnskeys, rrset_dnskeys = stage3.check_rrset_identical_visible(params_nameservers, params_dnskeys['dnskey'], query_dnskeys_identical)
        self.assertEqual(0, len(new_issues))
       

        # check if dnskey rrsets are unidentical
        new_issues, visible_dnskeys, rrset_dnskeys = stage3.check_rrset_identical_visible(params_nameservers, params_dnskeys['dnskey'], query_dnskeys_unidentical)
        expected_issues = [ NastIssue(IssueCodes.DNSKEY_RRSET_INCONSISTENT, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RRSET_INCONSISTENT, IssueSeverity.ERROR),
                            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
                            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
                            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
                            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
                            NastIssue(IssueCodes.DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.WARNING),
                            NastIssue(IssueCodes.ALL_DNSKEY_RR_FROM_REQUEST_NOT_FOUND_IN_ALL_NAMESERVER_RESPONSES, IssueSeverity.ERROR)
                        ]
        expected_params = [ {'nameserver': 'ns1.denic.nast'},
                            {'nameservers': 'ns1.denic.nast != ns2.denic.nast, ns4.denic.nast != ns3.denic.nast'},
                            {'key': stage3.to_key_wire_text(params_dnskeys['dnskey'][0])},
                            {'key': stage3.to_key_wire_text(params_dnskeys['dnskey'][1])},
                            {'key': stage3.to_key_wire_text(params_dnskeys['dnskey'][2])},
                            {'key': stage3.to_key_wire_text(params_dnskeys['dnskey'][3])},
                            {'key': stage3.to_key_wire_text(params_dnskeys['dnskey'][4])},
                            None,
                        ]

        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_proof_of_possession(self):
        params_domain = "denic.nast"
        params_nameservers = {
            'ns1.denic.nast' : ['172.31.1.1','fd00:10:10::1:1'],
            'ns2.denic.nast' : ['172.31.2.2','fd00:10:10::2:2'],
            'ns3.denic.nast' : ['172.31.3.3','fd00:10:10::3:3'],
            'ns4.denic.nast' : ['172.31.4.4','fd00:10:10::4:4']
        }
        params_visible_dnskeys = {
            'dnskey': [
                # RSA
                {'flags':257, 'algorithm':8, 'key':'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='},
                # RSA
                {'flags':256, 'algorithm':8, 'key':'AwEAAeNhBKM5IpxsuobXYKIBw1BM3OJH+CMuN04DYn39WoVuwopIxG3O Uv0yCjPQCTl2CAzNJDwplioQy/pfnKTyOQkAxRKALDVJpfTqlTGxUKzc P3uGRLkvMaxPgh3iqimDPGJ54g2c5vF4HvbzpQFy76t96glbh+GC7VsV ke+pG0M5'}
            ]
        }
        
        rrsig_rdata =  dns.rdata.from_text(dns.rdataclass.IN, dns.rdatatype.RRSIG , 'DNSKEY 8 2 86400 20410806003331 20210806040931 18556 denic.nast. FAtLnQqOaNGoLjYmzcLaT1tOgfVIqtOdB1uPN3TJQtTtmPPkKuq4el9y viKmtomNt7YHJQ9vP7AudMj3b3s/JMC6M8xl2LaadM3ZnzL8ajaYlyjK 3NGslqzOxSGnsLKK5++jPu1I+7TSbKEPDaxezqPeKTNdaIkxC7RViH7T ku8Gh2mek04PQTagbQzKlbXLoojb9zJZ72mX/jbVKyocPvlY1hJkw7Ze /UR4fDiVT91I0SgcnQ+PAWnxbVNtyFJjdtFVPhcVdxa2eNIXpPjhe71V ykEHecI9s2lOkumZaKmlyxejlMiA1yAwIP2CrZv5jAFhqmwIgLFfYrpN dwtGfQ==')

        dnskey_rdataset_list = list()
        for key in params_visible_dnskeys['dnskey']:
            dnskey_rdataset_list.append(stage3.to_key_rdata(key))
        
        dnskey_rrset = dns.rrset.from_rdata_list(dns.name.from_text(params_domain), 86400, dnskey_rdataset_list)
        rrsig_rrset = dns.rrset.from_rdata_list(dns.name.from_text(params_domain), 86400, [rrsig_rdata])
        
        rdata_dict = {'DNSKEY': {'RRSET': dnskey_rrset, 'RRSIG': rrsig_rrset} }

        query_rdata = {
            'ns1.denic.nast' : {'172.31.1.1': rdata_dict,
                                'fd00:10:10::1:1': rdata_dict},
            'ns2.denic.nast' : {'172.31.2.2': rdata_dict,
                                'fd00:10:10::2:2': rdata_dict},
            'ns3.denic.nast' : {'172.31.3.3': rdata_dict,
                                'fd00:10:10::3:3': rdata_dict},
            'ns4.denic.nast' : {'172.31.4.4': rdata_dict,
                                'fd00:10:10::4:4': rdata_dict}
        }

        # check if DNSKEY rrset is correctly signed - VALID CASE
        new_issues = stage3.check_proof_of_possession(params_nameservers, [params_visible_dnskeys['dnskey'][0]], query_rdata, params_domain)
        self.assertEqual(0, len(new_issues))

        # check if DNSKEY rrset is correctly signed - INVALID CASE
        new_issues = stage3.check_proof_of_possession(params_nameservers, [params_visible_dnskeys['dnskey'][1]], query_rdata, params_domain)
  
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_RRSET_SIGNATURE_VALIDATION_FAILED, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr), 'record': str('DNSKEY')})

        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_chain_of_trust(self):
        params_domain = "denic.nast"
        params_nameservers = {
            'ns1.denic.nast' : ['172.31.1.1','fd00:10:10::1:1'],
            'ns2.denic.nast' : ['172.31.2.2','fd00:10:10::2:2'],
            'ns3.denic.nast' : ['172.31.3.3','fd00:10:10::3:3'],
            'ns4.denic.nast' : ['172.31.4.4','fd00:10:10::4:4']
        }
        params_rrset_dnskeys = {
            'dnskey': [
                # RSA
                {'flags':257, 'algorithm':8, 'key':'AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC70gJX6EUvKHdSfb/ChiM31jo STHccSytFz97cHeftqX/9nCprf4e1dGofktlZ7lAMQ2s6HnGHWxQm+sq 6o0y0b+WJQOVm6nWKV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3CsZALS1Do +cAMsJD99t4H0hEEpQiJz5saVbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qwl2yiqyu/NtuqrqC+YyTXQN66 udy72ew+thytikpYteVHaGzeFXN6WiHkLO/1dgCJUxaww3TWWeHIbYxa aeNLcWnA8pU='},
                # RSA
                {'flags':256, 'algorithm':8, 'key':'AwEAAeNhBKM5IpxsuobXYKIBw1BM3OJH+CMuN04DYn39WoVuwopIxG3O Uv0yCjPQCTl2CAzNJDwplioQy/pfnKTyOQkAxRKALDVJpfTqlTGxUKzc P3uGRLkvMaxPgh3iqimDPGJ54g2c5vF4HvbzpQFy76t96glbh+GC7VsV ke+pG0M5'}
            ]
        }
        rrset_data = dns.rdata.from_text(dns.rdataclass.IN, dns.rdatatype.SOA , 'ns1.denic.nast. its.denic.nast. 2021051001 10800 1800 3600000 1800')    
        rrsig_rdata =  dns.rdata.from_text(dns.rdataclass.IN, dns.rdatatype.RRSIG , 'SOA 8 2 86400 20410806003331 20210806040931 33529 denic.nast. nXhP03ZsMX4c8nooO5rTacctnVJ6U7IQcxn1Af2SKo2y47krowbtYakg jgU2vetOoDqhjkZRhMgTfnOSMr+7mrnF79P9dx0R165KdtV824Y5pPWO lONWlTh1/95Exz4vCzSrH4Y0bBN+wxo08Wq9bkJ9sgnlv/BLr1jQvxqo uEU=')

        soa_rrset = dns.rrset.from_rdata_list(dns.name.from_text(params_domain), 86400, [rrset_data])
        rrsig_rrset = dns.rrset.from_rdata_list(dns.name.from_text(params_domain), 86400, [rrsig_rdata])
        
        rdata_dict = {'SOA': {'RRSET': soa_rrset, 'RRSIG': rrsig_rrset} }

        query_rdata = {
            'ns1.denic.nast' : {'172.31.1.1': rdata_dict,
                                'fd00:10:10::1:1': rdata_dict},
            'ns2.denic.nast' : {'172.31.2.2': rdata_dict,
                                'fd00:10:10::2:2': rdata_dict},
            'ns3.denic.nast' : {'172.31.3.3': rdata_dict,
                                'fd00:10:10::3:3': rdata_dict},
            'ns4.denic.nast' : {'172.31.4.4': rdata_dict,
                                'fd00:10:10::4:4': rdata_dict}
        }

        # check if SOA rrset is correctly signed - VALID CASE indirectly
        new_issues = stage3.check_chain_of_trust(params_nameservers, [params_rrset_dnskeys['dnskey'][1]], query_rdata, params_domain)
        self.assertEqual(0, len(new_issues))

        # check if SOA rrset is correctly signed - INVALID CASE indirectly
        new_issues = stage3.check_chain_of_trust(params_nameservers, [params_rrset_dnskeys['dnskey'][0]], query_rdata, params_domain)
        
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.SOA_RRSET_SIGNATURE_VALIDATION_FAILED, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr), 'record': str('SOA')})
        
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def make_test_query(self):
        rrset_udp = 'id 60921\n' \
            'opcode QUERY\n' \
            'rcode NOERROR\n' \
            'flags QR AA RD\n' \
            'edns 0\n' \
            'eflags DO\n' \
            'payload 1232\n' \
            ';QUESTION\n' \
            'denic.nast. IN DNSKEY \n' \
            ';ANSWER\n' \
            'denic.nast. 86400 IN DNSKEY 257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC 70gJX6EUvKHdSfb/ChiM31joSTHccSyt Fz97cHeftqX/9nCprf4e1dGofktlZ7lA MQ2s6HnGHWxQm+sq6o0y0b+WJQOVm6nW KV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3C sZALS1Do+cAMsJD99t4H0hEEpQiJz5sa VbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qw l2yiqyu/NtuqrqC+YyTXQN66udy72ew+ thytikpYteVHaGzeFXN6WiHkLO/1dgCJ Uxaww3TWWeHIbYxaaeNLcWnA8pU=\n' \
            'denic.nast. 86400 IN DNSKEY 256 3 8 AwEAAeNhBKM5IpxsuobXYKIBw1BM3OJH +CMuN04DYn39WoVuwopIxG3OUv0yCjPQ CTl2CAzNJDwplioQy/pfnKTyOQkAxRKA LDVJpfTqlTGxUKzcP3uGRLkvMaxPgh3i qimDPGJ54g2c5vF4HvbzpQFy76t96glb h+GC7VsVke+pG0M5\n' \
            'denic.nast. 86400 IN RRSIG DNSKEY 8 2 86400 20210912115400 20210813115400 18556 denic.nast. qrc0D/SUKs/fBNUwaUlXTOBuUV0i3wQj DWAM9nW2oK0ZQBdBVnvXDlnum5Fec86f 9nFP60/yok1rBTyiXzG8xeHMVFZDjElV dmPvMu9Abs+6/XzpBh1956Cpouaf63Ow cEO6rUc/Nw8b/UQZdQLosnZT/LLu22jD 6Qvq+pphFyruEeBjoOeAdYokAbgIUYYy rHcVZS+Qya1q2pqI428yCGuVBrRta3OS QOYzX6E+gkHCTHTjU0qX8B8ayfIia1WJ fF8dimwFT/1XHv8Q3TEWy8m6e0aHSrY9 Qe5JuohR2DJcTzEVKAsyJSLsMCebw4rE YL04+h+/8E2lRostRXjJ4w==\n' \
            ';AUTHORITY\n' \
            ';ADDITIONAL\n'
        rrset_tcp = 'id 60921\n' \
            'opcode QUERY\n' \
            'rcode NOERROR\n' \
            'flags QR AA RD\n' \
            ';QUESTION\n' \
            'denic.nast. IN DNSKEY\n' \
            ';ANSWER\n' \
            'denic.nast. 86400 IN DNSKEY 257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC 70gJX6EUvKHdSfb/ChiM31joSTHccSyt Fz97cHeftqX/9nCprf4e1dGofktlZ7lA MQ2s6HnGHWxQm+sq6o0y0b+WJQOVm6nW KV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3C sZALS1Do+cAMsJD99t4H0hEEpQiJz5sa VbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qw l2yiqyu/NtuqrqC+YyTXQN66udy72ew+ thytikpYteVHaGzeFXN6WiHkLO/1dgCJ Uxaww3TWWeHIbYxaaeNLcWnA8pU=\n' \
            'denic.nast. 86400 IN DNSKEY 256 3 8 AwEAAeNhBKM5IpxsuobXYKIBw1BM3OJH +CMuN04DYn39WoVuwopIxG3OUv0yCjPQ CTl2CAzNJDwplioQy/pfnKTyOQkAxRKA LDVJpfTqlTGxUKzcP3uGRLkvMaxPgh3i qimDPGJ54g2c5vF4HvbzpQFy76t96glb h+GC7VsVke+pG0M5\n' \
            ';AUTHORITY\n' \
            ';ADDITIONAL\n'
        rrsig_tcp = 'id 16396\n' \
            'opcode QUERY\n' \
            'rcode NOERROR\n' \
            'flags QR AA RD\n' \
            ';QUESTION\n' \
            'denic.nast. IN RRSIG\n' \
            ';ANSWER\n' \
            'denic.nast. 86400 IN RRSIG SOA 8 2 86400 20210912115400 20210813115400 33529 denic.nast. ZhsVRSC89Z5mFsEmD15jnRE1DA/Mic7s XTKVrChZ4k8hpAamUBQ21I5Ng9fyFduq kVmeEUILTlsEOnk20WIX2atAGSQBH04Y ZqtFBU/5GrU7KILu2I0ay2QhQbM2x/mT IFTkvvdQWf4fgm37vHmZF02wRMmlQDSH hCSPEYZiXtQ=\n' \
            'denic.nast. 86400 IN RRSIG NS 8 2 86400 20210912115400 20210813115400 33529 denic.nast. c9HwqH93b+AsswwkGeRpLM5tR+jZh3sE VxmJF+YaGMr/woBGiANFQwo/AeLu9cMV t40GExCSRgS+7p9oPWAanyhFfCKeiWNj YfryOTyCELq+d1AaUf+vnms9LbuPY4aU AdxGAUzKgGhreLBc3hKloB2vyEZkPo4c KmCYNxq6T7U=\n' \
            'denic.nast. 86400 IN RRSIG DNSKEY 8 2 86400 20210912115400 20210813115400 18556 denic.nast. qrc0D/SUKs/fBNUwaUlXTOBuUV0i3wQj DWAM9nW2oK0ZQBdBVnvXDlnum5Fec86f 9nFP60/yok1rBTyiXzG8xeHMVFZDjElV dmPvMu9Abs+6/XzpBh1956Cpouaf63Ow cEO6rUc/Nw8b/UQZdQLosnZT/LLu22jD 6Qvq+pphFyruEeBjoOeAdYokAbgIUYYy rHcVZS+Qya1q2pqI428yCGuVBrRta3OS QOYzX6E+gkHCTHTjU0qX8B8ayfIia1WJ fF8dimwFT/1XHv8Q3TEWy8m6e0aHSrY9 Qe5JuohR2DJcTzEVKAsyJSLsMCebw4rE YL04+h+/8E2lRostRXjJ4w==\n' \
            'denic.nast. 0 IN RRSIG NSEC3PARAM 8 2 0 20210912115400 20210813115400 33529 denic.nast. 1sJwpsaj8SxAxOmZTDI7BSUyENpb9Ilp UBN9k0zW6IPlQJTkdfT9xgl5KS70cc+4 0KxCyFd8p4C2XZp0WIsdLJL36t7pO7/I BiFn6JbcUqYSYAbLxm/JR2cTIjETQa+d OFS1Cc4O3lV5x5Z0iCV1IMWOII2HUBpi o8AvhdvSgZs=\n' \
            ';AUTHORITY\n' \
            ';ADDITIONAL\n'
        return rrset_udp, rrset_tcp, rrsig_tcp

    def test_check_edns_support(self):
        params_domain = "denic.nast"
        params_nameservers = {
            'ns1.denic.nast' : ['172.31.1.1','fd00:10:10::1:1'],
            'ns2.denic.nast' : ['172.31.2.2','fd00:10:10::2:2'],
            'ns3.denic.nast' : ['172.31.3.3','fd00:10:10::3:3'],
            'ns4.denic.nast' : ['172.31.4.4','fd00:10:10::4:4']
            }
        rrset_udp_str, _, _ = self.make_test_query()
        #rrset_tcp = dns.message.from_text(rrset_tcp_str)
        #rrsig_tcp = dns.message.from_text(rrsig_tcp_str)
        
        # check if EDNS is suppoerted - VALID CASE
        rrset_udp = dns.message.from_text(rrset_udp_str)
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)
        new_issues = stage3.check_edns_support(params_nameservers, params_query_result, params_query_rdata)
        self.assertEqual(0, len(new_issues))

        # check if EDNS is supported - INVALID CASE DO-Bit missing
        missing_do = rrset_udp_str.replace("eflags DO\n", "")
        rrset_udp = dns.message.from_text(missing_do)
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)
        new_issues = stage3.check_edns_support(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_OR_SOA_EDNS0_QUERY_NOT_SUPPORTED, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr), 'record': 'DNSKEY', 'found': str('DO-Bit missing')})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)
 
        # check if EDNS is supported - INVALID CASE RRSIG missing
        missing_rrsig = rrset_udp_str.replace("denic.nast. 86400 IN RRSIG DNSKEY 8 2 86400 20210912115400 20210813115400 18556 denic.nast. qrc0D/SUKs/fBNUwaUlXTOBuUV0i3wQj DWAM9nW2oK0ZQBdBVnvXDlnum5Fec86f 9nFP60/yok1rBTyiXzG8xeHMVFZDjElV dmPvMu9Abs+6/XzpBh1956Cpouaf63Ow cEO6rUc/Nw8b/UQZdQLosnZT/LLu22jD 6Qvq+pphFyruEeBjoOeAdYokAbgIUYYy rHcVZS+Qya1q2pqI428yCGuVBrRta3OS QOYzX6E+gkHCTHTjU0qX8B8ayfIia1WJ fF8dimwFT/1XHv8Q3TEWy8m6e0aHSrY9 Qe5JuohR2DJcTzEVKAsyJSLsMCebw4rE YL04+h+/8E2lRostRXjJ4w==\n", "")
        rrset_udp = dns.message.from_text(missing_rrsig)
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)
       
        new_issues = stage3.check_edns_support(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_OR_SOA_EDNS0_QUERY_NOT_SUPPORTED, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr), 'record': 'DNSKEY', 'found': str('RRSIG missing')})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())
        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

        # check if EDNS is supported - INVALID CASE OPT header invalid
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.message.BadEDNS()}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)
       
        new_issues = stage3.check_edns_support(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_OR_SOA_EDNS0_QUERY_NOT_SUPPORTED, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr), 'record': 'DNSKEY', 'found': str('OPT invalid')})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_udp_edns_protocol(self):
        params_nameservers = {
            'ns1.denic.nast' : ['172.31.1.1','fd00:10:10::1:1'],
            'ns2.denic.nast' : ['172.31.2.2','fd00:10:10::2:2'],
            'ns3.denic.nast' : ['172.31.3.3','fd00:10:10::3:3'],
            'ns4.denic.nast' : ['172.31.4.4','fd00:10:10::4:4']
            }
        rrset_udp_str, _, _ = self.make_test_query()
        rrset_udp = dns.message.from_text(rrset_udp_str)
        
        # check if udp is ok - VALID CASE
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        new_issues = stage3.check_udp_edns_protocol(params_nameservers, params_query_result)
        self.assertEqual(0, len(new_issues))

        # check if udp is truncated - INVALID CASE
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.message.Truncated()}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        new_issues = stage3.check_udp_edns_protocol(params_nameservers, params_query_result)
        expected_issues = list()
        expected_params = list()
        expected_issues.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED, IssueSeverity.WARNING))
        expected_params.append({'nameservers': 'ns1.denic.nast, ns2.denic.nast, ns3.denic.nast, ns4.denic.nast',
                                 'error': 'truncation'
                               })
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

        # check if udp is timeout - INVALID CASE
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
                if nameserver == 'ns1.denic.nast' or nameserver == 'ns2.denic.nast':
                    params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.exception.Timeout()}
                else:
                    params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
    
        new_issues = stage3.check_udp_edns_protocol(params_nameservers, params_query_result)
        expected_issues = list()
        expected_params = list()
        expected_issues.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED, IssueSeverity.WARNING))
        expected_params.append({'nameservers': 'ns1.denic.nast, ns2.denic.nast',
                                'error': 'timeout'
                               })
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

        # check if udp is connection refused - INVALID CASE
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
                if nameserver == 'ns1.denic.nast' or nameserver == 'ns2.denic.nast':
                    params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': ConnectionRefusedError()}
                else:
                    params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
    
        new_issues = stage3.check_udp_edns_protocol(params_nameservers, params_query_result)
        expected_issues = list()
        expected_params = list()
        expected_issues.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED, IssueSeverity.WARNING))
        expected_params.append({'nameservers': 'ns1.denic.nast, ns2.denic.nast',
                                'error': 'connection refused'
                               })
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)


        
        # check if udp is failed unknown - INVALID CASE
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
                if nameserver == 'ns1.denic.nast' or nameserver == 'ns2.denic.nast':
                    params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.exception.DNSException()}
                else:
                    params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
    
        new_issues = stage3.check_udp_edns_protocol(params_nameservers, params_query_result)
        expected_issues = list()
        expected_params = list()
        expected_issues.append(NastIssue(IssueCodes.SOME_DNSKEY_OR_SOA_RRSET_EDNS0_QUERY_FAILED, IssueSeverity.WARNING))
        expected_params.append({'nameservers': 'ns1.denic.nast, ns2.denic.nast',
                                'error': 'unknown'
                               })
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

    def test_check_dnskey_rrset(self):
        params_domain = "denic.nast"
        params_nameservers = {
            'ns1.denic.nast' : ['172.31.1.1','fd00:10:10::1:1'],
            'ns2.denic.nast' : ['172.31.2.2','fd00:10:10::2:2'],
            'ns3.denic.nast' : ['172.31.3.3','fd00:10:10::3:3'],
            'ns4.denic.nast' : ['172.31.4.4','fd00:10:10::4:4']
            }
        rrset_udp_str, _, _ = self.make_test_query()
        rrset_udp = dns.message.from_text(rrset_udp_str)

         # check if rrset is available- VALID
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        self.assertEqual(0, len(new_issues))

        # check if rrset is available- VALID
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.message.Truncated(), 'tcp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        self.assertEqual(0, len(new_issues))
        
        # check if rrset is available - INVALID CASE - Timeout after UDP to TCP switch - due to UDP truncation 
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.message.Truncated(), 'tcp': dns.exception.Timeout()}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.TIMEOUT, IssueSeverity.ERROR))
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_RRSET_NOT_AVAILABLE, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr),
                                        'protocol': 'tcp', 'record': 'DNSKEY'})
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr)})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

        # check if rrset is available - INVALID CASE - Timeout after UDP to TCP switch - due to UDP timeout 
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.exception.Timeout(), 'tcp': dns.exception.Timeout()}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.TIMEOUT, IssueSeverity.ERROR))
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_RRSET_NOT_AVAILABLE, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr),
                                        'protocol': 'tcp', 'record': 'DNSKEY'
                                       })
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr)})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)

        # check if rrset is available - INVALID CASE - Timeout after UDP to TCP switch - due to UDP unknown DNSException 
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.exception.DNSException(), 'tcp': dns.exception.Timeout()}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.TIMEOUT, IssueSeverity.ERROR))
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_RRSET_NOT_AVAILABLE, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr),
                                        'protocol': 'tcp', 'record': 'DNSKEY'
                                       })
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr)})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)
    
        # check if rrset is available - INVALID CASE - TCP Unknown DNSException 
        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.exception.DNSException(), 'tcp': dns.exception.DNSException()}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_RRSET_NOT_AVAILABLE, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr)})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)
        self.assertListEqual(expected_params, new_issue_params)
    
        # check if rrset is available - INVALID CASE - RRSET is missing in UDP answer
        missing_rrset = rrset_udp_str.replace("denic.nast. 86400 IN DNSKEY 257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC 70gJX6EUvKHdSfb/ChiM31joSTHccSyt Fz97cHeftqX/9nCprf4e1dGofktlZ7lA MQ2s6HnGHWxQm+sq6o0y0b+WJQOVm6nW KV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3C sZALS1Do+cAMsJD99t4H0hEEpQiJz5sa VbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qw l2yiqyu/NtuqrqC+YyTXQN66udy72ew+ thytikpYteVHaGzeFXN6WiHkLO/1dgCJ Uxaww3TWWeHIbYxaaeNLcWnA8pU=\n", "")
        missing_rrset = missing_rrset.replace("denic.nast. 86400 IN DNSKEY 256 3 8 AwEAAeNhBKM5IpxsuobXYKIBw1BM3OJH +CMuN04DYn39WoVuwopIxG3OUv0yCjPQ CTl2CAzNJDwplioQy/pfnKTyOQkAxRKA LDVJpfTqlTGxUKzcP3uGRLkvMaxPgh3i qimDPGJ54g2c5vF4HvbzpQFy76t96glb h+GC7VsVke+pG0M5\n", "")
        rrset_udp = dns.message.from_text(missing_rrset)

        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_RRSET_NOT_AVAILABLE, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr)})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)

        # check if rrset is available - INVALID CASE - RRSET is missing in TCP answer
        missing_rrset = rrset_udp_str.replace("denic.nast. 86400 IN DNSKEY 257 3 8 AwEAAeMkpA0W8ZNvZuT+AHWazdYfChdC 70gJX6EUvKHdSfb/ChiM31joSTHccSyt Fz97cHeftqX/9nCprf4e1dGofktlZ7lA MQ2s6HnGHWxQm+sq6o0y0b+WJQOVm6nW KV9CGFVgA+wvvcD2KEdjVvIgvfxFNe3C sZALS1Do+cAMsJD99t4H0hEEpQiJz5sa VbVtEnwN82nPt4/ram+DLcHmJJTbzP98 q2xUfLl+d0FOLcfyYY2STjWS7wYEQ5Qw l2yiqyu/NtuqrqC+YyTXQN66udy72ew+ thytikpYteVHaGzeFXN6WiHkLO/1dgCJ Uxaww3TWWeHIbYxaaeNLcWnA8pU=\n", "")
        missing_rrset = missing_rrset.replace("denic.nast. 86400 IN DNSKEY 256 3 8 AwEAAeNhBKM5IpxsuobXYKIBw1BM3OJH +CMuN04DYn39WoVuwopIxG3OUv0yCjPQ CTl2CAzNJDwplioQy/pfnKTyOQkAxRKA LDVJpfTqlTGxUKzcP3uGRLkvMaxPgh3i qimDPGJ54g2c5vF4HvbzpQFy76t96glb h+GC7VsVke+pG0M5\n", "")
        rrset_udp = dns.message.from_text(missing_rrset)

        params_query_result = dict()
        for nameserver, ip_list in params_nameservers.items():
            params_query_result[nameserver] = dict()
            for ip_addr in ip_list:
                params_query_result[nameserver][ip_addr] = dict()
                params_query_result[nameserver][ip_addr]['DNSKEY'] = {'udp': dns.exception.DNSException(), 'tcp': rrset_udp}
                params_query_result[nameserver][ip_addr]['SOA+DNSSEC'] = {'udp': None}
        _, params_query_rdata = stage3.create_dnssec_dict(params_query_result, params_nameservers, params_domain)

        new_issues = stage3.check_dnskey_rrset(params_nameservers, params_query_result, params_query_rdata)
        expected_issues = list()
        expected_params = list()
        for nameserver, ip_list in params_nameservers.items():
            for ip_addr in ip_list:
                expected_issues.append(NastIssue(IssueCodes.DNSKEY_RRSET_NOT_AVAILABLE, IssueSeverity.ERROR))
                expected_params.append({'nameserver': str(nameserver), 'ip': str(ip_addr)})
            
        new_issue_params = list()
        for issue in new_issues:
            new_issue_params.append(issue.get_params())

        self.assertEqual(len(expected_issues), len(new_issues))
        self.assertListEqual(expected_issues, new_issues)