import unittest
from nast.config import Config
from nast import stage0
from nast.issues import NastIssue, IssueCodes, IssueSeverity


class Stage0Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.config = Config()
        cls.maxDiff = None

    def test_remove_closing_dotts_from_nameserver(self):
        params = {
            'domain': 'reachability-ipv4.nast.',
            'nameserver': {
                'ns1.nic.nast.': [],
                'ns6.nic.nast.': []
            },
            'dnskey': []
        }

        issue_list = stage0.parameter_check(self.config, params)
        self.assertEqual(0, len(issue_list))
        self.assertFalse(issue_list)
        self.assertEqual({'ns1.nic.nast': [],
                          'ns6.nic.nast': []}, params['nameserver'])

    def test_convert_idn_to_ace(self):
        params = {
            "domain": "sTraße.de.",
            "nameserver": {
                "ns1.xn--strae-oqa.net.": [],
                "ns2.straße.net": []
            },
            'dnskey': []
        }

        issue_list = stage0.parameter_check(self.config, params)
        self.assertEqual(0, len(issue_list))

        self.assertEqual({
            "domain": "xn--strae-oqa.de",
            "nameserver": {
                "ns1.xn--strae-oqa.net": [],
                "ns2.xn--strae-oqa.net": []
            },
            'dnskey': []
        }, params)

    def test_check_ip_addresses_of_glue_rr(self):
        params = {
            'domain': 'reachability-ipv4.nast.',
            'nameserver': {
                'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns2.nic.nast.': ['172.345.1.1', 'abcd::1224:xyzv'],
                'ns6.nic.nast.': ['172.6', 'fd00:10:10::6:6']
            },
            'dnskey': []
        }

        issue_list = stage0.check_ip_addresses_of_glue_rr(params)
        issue = NastIssue(IssueCodes.INVALID_IP_ADDRESS, IssueSeverity.ERROR)
        self.assertEqual([issue, issue, issue], issue_list)
        self.assertEqual({'nameserver': 'ns2.nic.nast.', 'ip': '172.345.1.1'}, issue_list[0].get_params())

    def test_check_glue_records(self):
        params = {
            'domain': 'nic.nast.',
            'nameserver': {
                'ns1.nic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns2.nic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                'ns2.nic.de.': [],
            },
            'dnskey': []
        }
        new_issues = stage0.check_glue_records(params)
        self.assertEqual([], new_issues)

    def test_check_glue_records_glue_record_not_in_zone(self):
        params = {
            'domain': 'nic.nast.',
            'nameserver': {
                'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns2.nic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
                'ns2.nic.de.': ['172.31.2.2'],
            },
            'dnskey': []
        }
        new_issues = stage0.check_glue_records(params)
        self.assertEqual(
            [NastIssue(IssueCodes.PROVIDED_GLUE_RECORDS_NOT_APPLICABLE, IssueSeverity.WARNING)],
            new_issues)
        self.assertEqual({
            'ns': 'ns2.nic.de.'
        }, new_issues[0].get_params())
        self.assertEqual([], params['nameserver']['ns2.nic.de.'])

    def test_check_glue_records_glue_record_is_subdomain_in_zone(self):
        params = {
            'domain': 'nic.nast.',
            'nameserver': {
                'ns1.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
                'ns2.test.nic.nast.': ['172.31.2.2', 'fd00:10:10::2:2'],
            },
            'dnskey': []
        }
        new_issues = stage0.check_glue_records(params)
        self.assertEqual([], new_issues)

    def test_check_glue_records_glue_records_not_given(self):
        params = {
            'domain': 'nic.nast.',
            'nameserver': {
                'ns1.nic.nast.': [],
                'nic.nast.': [],
                'nast.': [],
                'ns2.nic.nast.': ['172.31.1.1', 'fd00:10:10::1:1'],
            },
            'dnskey': []
        }
        new_issues = stage0.check_glue_records(params)
        self.assertEqual(
            [NastIssue(IssueCodes.MISSING_GLUE_RECORDS, IssueSeverity.ERROR, {'ns': 'ns1.nic.nast.'}),
             NastIssue(IssueCodes.MISSING_GLUE_RECORDS, IssueSeverity.ERROR, {'ns': 'nic.nast.'})],
            new_issues)
        self.assertEqual({
            'ns': 'ns1.nic.nast.'
        }, new_issues[0].get_params())
        self.assertEqual({
            'ns': 'nic.nast.'
        }, new_issues[1].get_params())

    def test_check_glue_records_its_not_inside_zone(self):
        params = {
            'domain': 'mydomain.nast',
            'nameserver': {
                'mydomain.nast.nic.nast.': [],
                'ns2.nic.nast.': [],
                'mydomain.nast.nast.': [],
                'ns1.mydomain.nast.nast.': [],
                'ns1.testnast.nast.': [],
            },
            'dnskey': []
        }
        new_issues = stage0.check_glue_records(params)
        self.assertEqual([], new_issues)

    def test_less_than_2_nameservers(self):
        params = {
            'domain': 'test.de.',
            'nameserver': {
                'ns1.nic.nast.': [],
            },
            'dnskey': []
        }
        new_issues = stage0.parameter_check(self.config, params)
        self.assertEqual([NastIssue(IssueCodes.INSUFFICIENT_NUMBER_OF_NAMESERVER_REACHABLE)],
                         new_issues)

        self.assertEqual({
            'expected': "2",
            'found': "1"
        }, new_issues[0].get_params())

    def test_check_referral_response_size(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'invalid ip address string'],
                                 'ns4.denic.net': []
                                 },
                   'dnskey': []
                  }
        new_issue = stage0.check_referral_response_size(params)
        self.assertEqual([], new_issue)

    def test_check_referral_response_size_error(self):
        params = {'domain': 'denic.nast',
                  'nameserver': {'ns1.denic.nast': ['172.31.1.1', 'fd00:10:10::1:1'],
                                 'ns2.denic.nast': ['172.31.2.2', 'fd00:10:10::2:2'],
                                 'ns3.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns4.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns5.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns6.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns7.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns8.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns9.denic.nast': ['172.31.3.3', 'fd00:10:10::3:3'],
                                 'ns10.denic.net': [],
                                 },
                  'dnskey': []
                  }
        new_issue = stage0.check_referral_response_size(params)
        self.assertEqual([NastIssue(IssueCodes.CALCULATED_REFERRAL_RESPONSE_LARGER_THAN_ALLOWED)],
                         new_issue)

    def test_check_referral_response_512(self):
        # with dots
        params = {'domain': 'nast-742-340.de.',
                  'nameserver': {'dns1.hdnvzrtlavdfrt64jghvdfertnvgthaefgqrufgqfehfvbfgfweruqfge.frhuifgfzegfgferzgferzifg.ferzhigferzigferzifgerzifggfgeugfergf.nast-742-340.de.': ['10.113.46.5'],
                                 'dns2.hdnvzrtlavdfrt64jghvdfertnvgthaefbezivgbefzigvehvigv.cvefbhgzivgfbvzefzvegwevigzr.vbefhgfehifbezifgwerzigferzigf.nast-742-340.de.': ['10.113.46.6']},
                   'dnskey': []
                  }
        new_issue = stage0.check_referral_response_size(params)
        self.assertEqual([], new_issue)
        # no dots
        params = {'domain': 'nast-742-340.de',
                  'nameserver': {'dns1.hdnvzrtlavdfrt64jghvdfertnvgthaefgqrufgqfehfvbfgfweruqfge.frhuifgfzegfgferzgferzifg.ferzhigferzigferzifgerzifggfgeugfergf.nast-742-340.de': ['10.113.46.5'],
                                 'dns2.hdnvzrtlavdfrt64jghvdfertnvgthaefbezivgbefzigvehvigv.cvefbhgzivgfbvzefzvegwevigzr.vbefhgfehifbezifgwerzigferzigf.nast-742-340.de': ['10.113.46.6']},
                  'dnskey': []
                  }
        new_issue = stage0.check_referral_response_size(params)
        self.assertEqual([], new_issue)

    def test_check_referral_response_513(self):
        # with dots
        params = {'domain': 'nast-742-340.de.',
                  'nameserver': {'dns1.hdnvzrtlavdfrt64jghvdfertnvgthaefgqrufgqfehfvbfgfweruqfge.frhuifgfzegfgferzgferzifg.ferzhigferzigferzifgerzifggfgeugfergf.nast-742-340.de.': ['10.113.46.5'],
                                 'dns2.hdnvzrtlavdfrt6a4jghvdfertnvgthaefbezivgbefzigvehvigv.cvefbhgzivgfbvzefzvegwevigzr.vbefhgfehifbezifgwerzigferzigf.nast-742-340.de.': ['10.113.46.6']},
                   'dnskey': []
                  }
        new_issue = stage0.check_referral_response_size(params)
        self.assertEqual([NastIssue(IssueCodes.CALCULATED_REFERRAL_RESPONSE_LARGER_THAN_ALLOWED, IssueSeverity.ERROR)], new_issue)
        # no dots
        params = {'domain': 'nast-742-340.de',
                  'nameserver': {'dns1.hdnvzrtlavdfrt64jghvdfertnvgthaefgqrufgqfehfvbfgfweruqfge.frhuifgfzegfgferzgferzifg.ferzhigferzigferzifgerzifggfgeugfergf.nast-742-340.de': ['10.113.46.5'],
                                 'dns2.hdnvzrtlavdfrt6a4jghvdfertnvgthaefbezivgbefzigvehvigv.cvefbhgzivgfbvzefzvegwevigzr.vbefhgfehifbezifgwerzigferzigf.nast-742-340.de': ['10.113.46.6']},
                   'dnskey': []
                  }
        new_issue = stage0.check_referral_response_size(params)
        self.assertEqual([NastIssue(IssueCodes.CALCULATED_REFERRAL_RESPONSE_LARGER_THAN_ALLOWED, IssueSeverity.ERROR)], new_issue)
        self.assertEqual({'length in octets': "513",
                          'length allowed': "512"}, new_issue[0].get_params())
